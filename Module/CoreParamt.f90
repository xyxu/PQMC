!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: A module used for defining the related quantities for PQMC calculations. All the quantities include
!              the following different parts:
!               (1) The Model Parameters for extended Hubbard Model;
!               (2) Some quantities used in Monte Carlo simulations;
!               (3) Some quantities used in Hubbard-Stratonovich Transformation;
!               (4) Ising fields for the four terms in Hubbard-Stratonovich Transformation;
!               (5) The projector matrix and Exponential Kinetic matrix;
!               (6) QMC updating and equal-time Green Function related matrices;
! COMMENT: For 2D Honeycomb lattice Hubbard Model.
! AUTHOR:  Yuan-Yao He, Rong-Qiang He
! DATE:    2014-10-23
! PURPOSE: Different subroutines are introduced as following:
!             
!   CoreParamt --> Define the core variables for the calculation; 
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!________________________________________ Begin Module ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	module CoreParamt
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________      
		use RealPrecsn
		implicit none
!**************************************************************************************************************  
!______________________________ 1. Define the finite lattice size _____________________________________________
!**************************************************************************************************************
!________________________________________________________________________________________________	  
!_________________ (0). Lattice size of the system ______________________________________________
!________________________________________________________________________________________________		
		integer NOrbt       ! Number of orbitals in a single unit cell of a layer  --> NOrbt = 2 for honeycomb lattice
		integer NumL1       ! Number of unit cells in a1-direction                 --> a1 = (sqrt(3),0) 
		integer NumL2       ! Number of unit cells in a2-direction                 --> a2 = (sqrt(3)/2, 3/2)
		integer NumNC       ! Total number of unit cells for this lattice          --> NumNC = NumL1 * NumL2
		integer NumNS       ! Total number of lattice sites for double layer model --> For honeycomb: NumNS
!________________________________________________________________________________________________	  
!_________________ (1). The basic lattice vectors in real and reciprocal ________________________
!________________________________________________________________________________________________	
		real(rp) A1Vec(2)   ! The a1 lattice vector in real space                 --> A1Vec = (sqrt(3), 0)
		real(rp) A2Vec(2)   ! The a2 lattice vector in real space                 --> A2Vec = (sqrt(3)/2, 3/2)
		real(rp) L1Vec(2)   ! Total length of a1 direction for the lattice system --> L1Vec = NumL1 * A1Vec
		real(rp) L2Vec(2)   ! Total length of a2 direction for the lattice system --> L2Vec = NumL2 * A2Vec
		
		real(rp) B1Vec(2)   ! The b1 basic vector in reciprocal space             --> B1Vec = 2*pi/sqrt(3)*(1, -1/sqrt(3))
		real(rp) B2Vec(2)   ! The b2 basic vector in reciprocal space             --> B2Vec = 4*pi/3*(0, 1)
		real(rp) X1Vec(2)   ! Vector used to generate the discrete k points in BZ --> X1Vec = B1Vec / NumL1
		real(rp) X2Vec(2)   ! Vector used to generate the discrete k points in BZ --> X2Vec = B2Vec / NumL2
!________________________________________________________________________________________________	  
!_________________ (2). Nearest-Neighbor unit cells of all unit cells ___________________________
!________________________________________________________________________________________________
		integer, allocatable ::       UCList(:, :) ! UCList(NumNC, 2)                     --> The two integer indexes for a unit cell          
		integer, allocatable ::    InvUCList(:, :) ! InvUCList(-NumL1:NumL1,-NumL2:NumL2) --> From two integer indexes to get Unit cell number
		integer, allocatable ::  NNUCList(:, :, :) ! NNUCList(NumNC, -1:1, -1:1)          --> Record the six NN Unit cells for all unit cells
		integer, allocatable ::       KpList(:, :) ! KpList(NumNC, NumNC)                 --> The two integer indexes for a single k point
		integer, allocatable ::    InvKpList(:, :) ! InvKpList(-NumL1:NumL1,-NumL2:NumL2) --> From two integer indexes to get k point number
!________________________________________________________________________________________________	  
!_________________ (3). NN, 2NN and 3NN lattice sites information _______________________________
!________________________________________________________________________________________________
		integer, allocatable ::    StList(:, :)    ! StList(NumNS, 2)        --> Return the unit cell integer index for site and 1 or 2 for sublattice
		integer, allocatable :: InvStList(:, :)    ! InvStList(NumNC, NOrbt) --> From UC index and Orbit to get the lattice site index
		integer, allocatable ::    NNBond(:, :)    ! NNBond(NumNC, 0:3)      --> The NN lattice sites for A sublattice sites
		integer, allocatable ::   TNNBond(:, :)    ! TNNBond(NumNC, 0:3)     --> The third-NN lattice sites for A sublattice sites
!________________________________________________________________________________________________	  
!_________________ (4). Distance indexes for two unit cells in the finite lattice _______________
!________________________________________________________________________________________________
		integer, allocatable :: IminusJ(:, :)      ! The integer index for (i-j) and i,j are unit cell indexes
!________________________________________________________________________________________________	  
!_________________ (5). Open or periodic boundary conditions in a1 and a2 directions ____________
!________________________________________________________________________________________________
		integer IfPr1                               ! Whether to apply the periodic boundary condition in a1 direction
		integer IfPr2                               ! Whether to apply the periodic boundary condition in a2 direction
		integer, allocatable ::    NNStBnd(:, :)   ! Indicating whether we have NN lattice sites for some A site
		integer, allocatable :: NNUCBnd(:, :, :)   ! Indicating whether we have NN unit cell for this unit cell
		integer, allocatable ::    TNNsBnd(:, :)   ! Indicating whether we have 3NN lattice sites
!**************************************************************************************************************	  
!______________________________ 2. Model Parameters for extended Hubbard Model ________________________________
!**************************************************************************************************************
		integer Magnt     ! The strength of external magnetic field in unit of Magnetic flux Quanta
		real(rp) TwstX    ! The twisted phase used to reduce the degeneracy, very small
		
		real(rp) Hopt1    ! The Nearest-Neighbor hopping parameter
		real(rp) Dimer    ! The Bond dimerization
		real(rp) HopSC    ! The Intrinsic SOC parameter
		real(rp) Hopt3    ! The third-Nearest-Neighbor hopping parameter
		real(rp) HubbU    ! The on-site Hubbard Repulsion interaction strength
		
		integer NumEl     ! Number of electrons in both spin-up and spin-down sectors, canonical ensemble calculations    
!**************************************************************************************************************	  
!______________________________ 3. Some quantities used in Monte Carlo simulations ____________________________
!**************************************************************************************************************
		real(rp) BetaT    ! The projection parameter in PQMC, effective inverse temperature
		real(rp) Dltau    ! The imaginary time slice, Dltau = BetaT / LTrot
		
		integer LTrot     ! Number of time slices. LTrot = BetaT / Dltau
		integer NWrap     ! Two useness: (1) Inteval number of time slices for using SVD; (2) Green Function wrapping.
		integer DmUST     ! Dimension of the USTMt matrix; DmUST = NTrot / NWrap
		integer NmBin     ! Number of measurements independently
		integer NSwep     ! Number of sweep in a single Bin
!**************************************************************************************************************	  
!______________________________ 4. Hubbard-Stratonovich Transformation interaction terms ______________________
!**************************************************************************************************************
!_______________________________________________________________________________________________	  
!________ (1). The values of 4-component Ising fields flipping values __________________________
!_______________________________________________________________________________________________	
		integer FlipL(-2:2, 3)        ! The flipping values for the 4-component Ising fields
!_______________________________________________________________________________________________	  
!________ (2). The factors of 4-component Ising fields and the ratios __________________________
!_______________________________________________________________________________________________		
		real(rp) GamL(-2:2)           ! The gamma constant, GamL(0) is not used
		real(rp) EtaL(-2:2)           ! The eta constant, EtaL(0) is not used
		real(rp) DGamL(-2:2, 3)       ! The ratio of GamL(i) and GamL(j)
!_______________________________________________________________________________________________	  
!________ (3). The factors for U interaction term used in updating process _____________________
!_______________________________________________________________________________________________		
		complex(rp) LmdaU             ! The constant in HS transformation for U interaction term
		complex(rp) ExpoU(-2:2)       ! The exponential of exp(LmdaU * EtaL(-2:2))
		complex(rp) DltU1(-2:2, 3)    ! Constant used for updating of equal-time Green Function
		complex(rp) DltU2(-2:2, 3)    ! The division of the constants in the possibility distribution function
!_______________________________________________________________________________________________  
!________ (4). Ising fields for the four terms in Hubbard-Stratonovich Transformation __________
!_______________________________________________________________________________________________      
		integer, allocatable :: IsngbU(:, :)         ! Ising fields for the intra-layer U interaction term
!_______________________________________________________________________________________________	  
!________ (5). The projector matrix and Exponential Kinetic matrix _____________________________
!_______________________________________________________________________________________________        
		complex(rp), allocatable :: ProjM(:, :)      ! The projector matrix, P(NumNS, NumNS) --> But we use part
		complex(rp), allocatable :: ExpK0(:, :)      ! The exponential of Kinetic matrix, exp(-\Delta\tau * K)
		complex(rp), allocatable :: ExpKI(:, :)      ! The inverse of exponential of Kinetic matrix, [exp(-\Delta\tau * K)]^{-1}
!**************************************************************************************************************	  
!______________________________ 5. QMC updating and equal-time Green Function related matrices ________________
!**************************************************************************************************************
		integer IfUDV                                 ! If to output the test details in UDV decomposition
		
		real(rp) PhaseDif                             ! The difference between two phases (real number)
		real(rp) PhaseMax                             ! The maximum phase difference (real number)
		complex(rp) Phase                             ! The phase of determinant of spin-\sigma sector
		
		complex(rp), allocatable ::    ULeft(:, :)   ! The left side matrix: P^{\dagger} * B_i
		complex(rp), allocatable ::    URght(:, :)   ! The right side matrix: B_i * P 
		complex(rp), allocatable ::    ULtRt(:, :)   ! The left-right matrix: P^{\dagger} * {B_i} * P
		complex(rp), allocatable :: ULtRtInv(:, :)   ! The inverse of left-right matrix: (P^{\dagger} * {B_i} * P)^{-1}
!**************************************************************************************************************	  
!______________________________ 6. Time-displayed Green Function related matrices _____________________________
!**************************************************************************************************************
		complex(rp), allocatable ::    ULeft_1(:, :) ! The temporary Left side matrix 
		complex(rp), allocatable ::    URght_1(:, :) ! The temporary right side matrix 
		complex(rp), allocatable :: ULtRtInv_1(:, :) ! The temporary inverse matrix of Left-right matrix
		complex(rp), allocatable ::   USTMt(:, :, :) ! Record the ULeft or URght matrix where the UDV decomposition happens
!**************************************************************************************************************	  
!______________________________ 7. Metropolis Accepting ratio for U interactions ______________________________
!**************************************************************************************************************
		integer ISeed        ! The input ISeed for random number generating process
		real(rp) AcptU      ! The global accepting ratio for intra-layer U updating Ising fields
!**************************************************************************************************************	  
!______________________________ 8. The file handler for all the output files __________________________________
!**************************************************************************************************************
		integer OutPF(100)    ! The integer handler for the output file --> Main Output files
		integer ExtPF(50)     ! The integer handler for the output file --> Some additional output files
	  
	end module
!__________________________________________________________________________________________________________________________________________  
!________________________________________ End Module ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$