!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: A module and a few subroutines used for count the time consumed during the PQMC simulations for every BIN calculations. The
!             consumed time mainly includes the following parts: 
!               (1) U interaction updating     --> TimsUIntr, TimeUIntr
!               (2) V interaction updating     --> TimsVIntr, TimeVIntr
!               (3) J interaction updating     --> TimsJIntr, TimeJIntr
!               (4) Wrapping for Hopping term  --> TimsKWrap, TimeKWrap
!               (5) Wrapping for U interaction --> TimsUWrap, TimeUWrap
!               (6) Wrapping for V interaction --> TimsVWrp0, TimeVWrp0
!               (7) Wrapping for J interaction --> TimsJWrp0, TimeJWrp0
!               (8) UDV decomposition          --> TimsUDVOt, TimeUDVOt
!               (9) Observables Measurement    --> TimsStaMe, TimeStaMe
!              (10) Data process               --> TimsDatPr, TimeDatPr
!              (11) Total time For a BIN       --> TimsSgBIN
!
! COMMENT: This file is used to check Calculation part for every part in PQMC code.
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-30
! PURPOSE: Different subroutines are introduced as following:
!             
!     QMCTimeRec     --> Module to define the time recording quantities used in PQMC simulations;
!     QMCTimeRecInit --> Subroutine to perform the initialization for QMCTimeRec module.
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!________________________________________ Begin Module ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	module QMCTimeRec
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________ Integer calculation times in QMC --> Count times ___________________________________
!______________________________________________________________________________________________________________
		integer TimsUIntr       ! Number of calling intra-layer U  interaction updating subroutine in a single BIN simulation
		integer TimsJIntr       ! Number of calling intra-layer J  interaction updating subroutine in a single BIN simulation
		integer TimsKWrap       ! Number of calling Hopping term wrapping subroutine in a single BIN simulation
		integer TimsUWrap       ! Number of calling intra-layer U  interaction term wrapping subroutine in a single BIN simulation
		integer TimsJWrp0       ! Number of calling intra-layer J  interaction term wrapping subroutine in a single BIN simulation
		integer TimsUDVOt       ! Number of UDV decompositions in the PQMC simulations in a single BIN simulation
		integer TimsMtInv       ! Number of matrix inverse in the PQMC simulations in a single BIN simulation
		integer TimsStaMe       ! Number of measurements in a single BIN simulation in a single BIN simulation for static measurements
		integer TimsDynMe       ! Number of measurements in a single BIN simulation in a single BIN simulation for dynamic measurements
		integer TimsDatPr       ! Number of data post process in a single BIN simulation
!______________________________________________________________________________________________________________	  
!_________________________ Real calculation time in QMC --> Count time ________________________________________
!______________________________________________________________________________________________________________
		real(8) TimeUIntr       ! Time consumed for intra-layer U  interaction updating in a single BIN simulation
		real(8) TimeJIntr       ! Time consumed for intra-layer J  interaction updating in a single BIN simulation
		real(8) TimeKWrap       ! Time consumed for K interaction wrapping in a single BIN simulation
		real(8) TimeUWrap       ! Time consumed for intra-layer U  interaction wrapping in a single BIN simulation
		real(8) TimeJWrp0       ! Time consumed for intra-layer J  interaction wrapping in a single BIN simulation
		real(8) TimeUDVOt       ! Time consumed for UDV decomposition in a single BIN simulation
		real(8) TimeMtInv       ! Time consumed for Matrix inverse in a single BIN simulation
		real(8) TimeStaMe       ! Time consumed for Observables measurements for static measurements
		real(8) TimeDynMe       ! Time consumed for Observables measurements for Dynamic measurements
		real(8) TimeDatPr       ! Time consumed for data processing in a single BIN simulation
		real(8) TimeTotal       ! Time consumed for all the above calculations (not including matrix multiplication and inverse in UDV)
		real(8) TimeSgBIN       ! Time consumed for a single BIN simulation
		
	end module
!__________________________________________________________________________________________________________________________________________  
!________________________________________ End Module ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$




!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine QMCTimeRecInit()  
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  QMCTimeRecInit()  
! TYPE:     subroutine
! PURPOSE:  This Subroutine initialize the quantities defined in QMCTimeRec module.
! KEYWORDS: Initialization of time recording quantities.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-30
! DESCRIPTION:
!
!     We give the values to the quanties defined in the module QMCTimeRec. 
!
!     Input:  (none)   Output: (none)
!
! MODULES USED: QMCTimeRec
! SUBROUTINES CALLED:  (none)
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________
		use QMCTimeRec 
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________ Integer calculation times in QMC --> Count times ___________________________________
!______________________________________________________________________________________________________________
		TimsUIntr = 0       ! Number of calling intra-layer U interaction updating subroutine in a single BIN simulation
		TimsJIntr = 0       ! Number of calling intra-layer J interaction updating subroutine in a single BIN simulation
		TimsKWrap = 0       ! Number of calling Hopping term wrapping subroutine in a single BIN simulation
		TimsUWrap = 0       ! Number of calling intra-layer U interaction term wrapping subroutine in a single BIN simulation
		TimsJWrp0 = 0       ! Number of calling intra-layer J interaction term wrapping subroutine in a single BIN simulation
		TimsUDVOt = 0       ! Number of UDV decompositions in the PQMC simulations in a single BIN simulation
		TimsMtInv = 0       ! Number of matrix inverse in the PQMC simulations in a single BIN simulation
		TimsStaMe = 0       ! Number of measurements in a single BIN simulation in a single BIN simulation for static measurements
		TimsDynMe = 0       ! Number of measurements in a single BIN simulation in a single BIN simulation for dynamic measurements
		TimsDatPr = 0       ! Number of data post process in a single BIN simulation
!______________________________________________________________________________________________________________	  
!_________________________ Real calculation time in QMC --> Count time ________________________________________
!______________________________________________________________________________________________________________
		TimeUIntr = 0.0_8       ! Time consumed for intra-layer U interaction updating in a single BIN simulation
		TimeJIntr = 0.0_8       ! Time consumed for intra-layer J interaction updating in a single BIN simulation
		TimeKWrap = 0.0_8       ! Time consumed for K interaction wrapping in a single BIN simulation
		TimeUWrap = 0.0_8       ! Time consumed for intra-layer U interaction wrapping in a single BIN simulation
		TimeJWrp0 = 0.0_8       ! Time consumed for intra-layer J interaction wrapping in a single BIN simulation
		TimeUDVOt = 0.0_8       ! Time consumed for UDV decomposition in a single BIN simulation
		TimeMtInv = 0.0_8       ! Time consumed for matrix inverse in a single BIN simulation
		TimeStaMe = 0.0_8       ! Time consumed for Observables measurements for static measurements
		TimeDynMe = 0.0_8       ! Time consumed for Observables measurements for Dynamic measurements
		TimeDatPr = 0.0_8       ! Time consumed for data processing in a single BIN simulation
		TimeTotal = 0.0_8       ! Time consumed for all the above calculations (not including matrix multiplication in UDV)
		TimeSgBIN = 0.0_8       ! Time consumed for a single BIN simulation
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$