!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: A module and a few subroutines used for defining the observables which needs to be calculated in the PQMC Calculations.
!                  These observables includes the equal-time quantities (static) and time-displayed quantities (dynamic).
!                  Including: 
!             (0) Integer quantities used in equal-time measurement;
!             (1) Integer quantities used in time-displayed measurement;
!             (2) Expectation energies for every term in model Hamiltonian and double occupancy;
!             (2) Equal-time Real space correlation functions for some chosen lattice sites pairs in every BIN simulation;
!             (3) Equal-time Real space correlation functions for all lattice sites in every BIN simulation;
!             (4) Time-displayed Real space correlation functions for all lattice sites in every BIN simulation;
!             (5) Average Expectation energies of all terms for all BIN simulations;
!             (6) Average Equal-time Real space correlation functions for some chosen lattice sites pairs for all BIN simulations;
!             (7) Average Equal-time reciprocal space correlation functions for all lattice sites for all BIN simulations;
! COMMENT: Physical Observables.  
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-30
! PURPOSE: Different subroutines are introduced as following:
!             
!    Observable     --> Subroutine to define the related quantities for calculations;
!    ObservableInit --> Subroutine to perform the initializations of the quantities defined in this module.
!    ObservableFinl --> Subroutine to perform finalization of Observable module.
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!________________________________________ Begin Module ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	module Observable
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________    
		use RealPrecsn
		implicit none
!______________________________________________________________________________________________________________	  
!_______________________ 0. Integer parameters used in equal-time measurements ________________________________
!______________________________________________________________________________________________________________
		integer NObs        ! Count the number of measurements for equal-time physical quantities
		integer MeaTt       ! The number of time slices used for measurement
		integer MeaSt       ! The starting time slice used for measurement
		integer MeaEn       ! The ending time slice used for measurement
!______________________________________________________________________________________________________________	  
!_______________________ 1. Integer parameters used in time-displayed measurements ____________________________
!______________________________________________________________________________________________________________
		integer IfTAU       ! Whether to calculate the time-displayed quantities
		integer NObsT       ! Count the number of measurements for time displayed physical quantities
		integer SigNt       ! Sign integer for the time-displayed quantities
		integer NmTDM       ! Total length of the time-displaced measurements (+1)
		integer NTAUIN      ! The beginning imaginary time for time-displaced measurements
!**************************************************************************************************************	  
!_____________________ Whether to output all the dynamic Single-particle Green Function _______________________
!____________________________ to calculate spin Chern number and Z2 invariant _________________________________
!**************************************************************************************************************
		integer IfOtptGF                    ! 
!______________________________________________________________________________________________________________	  
!_______________________ 2. Observables in real space calculated from PQMC for every BIN ______________________
!______________________________________________________________________________________________________________
		integer, parameter :: NumCor = 30  ! Largest Number of lattice site pairs for calculating the correlation functions
		integer NumCal                      ! Number of lattice site pairs for calculating the correlation functions we actually do
		complex(rp) Obser(30)               ! Some observables including energies and double occupancy for every NmBIN simulations
		complex(rp) ExtOb(50)               ! The additional equal-time quantities
		complex(rp) CorrFTmp(NumCor, 4)     ! Tmporary matrix used to deducting the background term in Correlation functions
		complex(rp) CorrFunc(NumCor, 5)     ! The three Correlation functions on 3*2 lattices plus the total occupation
!______________________________________________________________________________________________________________	  
!_______________________ 3. Equal-time Correlation functions for all lattice sites ____________________________
!____________________________ in real space for every BIN, used for constructing ______________________________
!________________________________ Structure factors and momentum distribution _________________________________
!______________________________________________________________________________________________________________
		complex(rp), allocatable :: DenOcc(:, :, :)   ! The density occupation on all lattice sites
		complex(rp), allocatable :: SpinZc(:, :, :)   ! The spin-z component on all lattice sites
		complex(rp), allocatable :: SpinZZ(:, :, :)   ! The <Sz*Sz> correlation function
		complex(rp), allocatable :: SpinPM(:, :, :)   ! The <S+S- + S-S+> correlation function 
		complex(rp), allocatable :: GreenT(:, :, :)   ! Total Gup+Gdw for calculation of momentum distribution
		complex(rp), allocatable :: DenDen(:, :, :)   ! <n_i*n_j> density-density correlation function
!______________________________________________________________________________________________________________	  
!_______________________ 4. Time-dispalyed Correlation functions for all lattice sites ________________________
!____________________________ in real space for every BIN, used for constructing ______________________________
!______________________________________ reciprocal space Green Function _______________________________________
!______________________________________________________________________________________________________________
		integer IfKPt          ! Whether to store the K point data for gap --> For L=3x
		integer IfMPt          ! Whether to store the K point data for gap --> For L=2x  --> If Both --> L=6x
		complex(rp), allocatable :: GREENT_Tau(:, :, :, :)      ! Gup+Gdw for calculation of momentum distribution
		complex(rp), allocatable :: SPINZZ_Tau(:, :, :, :)      ! The <Sz*Sz> correlation function
		complex(rp), allocatable :: SPINPM_Tau(:, :, :, :)      ! The <S+S->+<S-S+> correlation function
		complex(rp), allocatable :: DenDen_Tau(:, :, :, :)      ! <n_i*n_j> density-density correlation function
		complex(rp), allocatable :: DenOcc_Tau(:, :, :   )      ! The <A_i> * <A_j> term in the density-density correlation function
		
		complex(rp), allocatable :: GreenG_Tau(:)               ! The global single-particle gap   
!______________________________________________________________________________________________________________	  
!_______________________ 5. Final expectation values for energies for all BIN's _______________________________
!______________________________________________________________________________________________________________
		real(rp), allocatable :: REHopt1(:)      ! Expectation energy for all the NN hopping terms
		real(rp), allocatable :: REHoptd(:)      ! Expectation energy for the dimerized NN hopping terms
		real(rp), allocatable :: REHopSC(:)      ! Expectation energy for the Intrinsic SOC terms
		real(rp), allocatable :: REHopt3(:)      ! Expectation energy for the 3NN hopping terms
		real(rp), allocatable :: REUIntr(:)      ! Expectation energy for intra-layer U  interaction terms
		real(rp), allocatable :: RETotal(:)      ! Expectation energy for Total model Hamiltonian
		real(rp), allocatable :: RDouOcc(:)      ! Expectation value for double occupancy
!______________________________________________________________________________________________________________	  
!_______________________ 6. Final expectation values for CorrFunc for all BIN's in real space _________________
!______________________________________________________________________________________________________________
		real(rp), allocatable :: RSpZSpZ(:, :)   ! Expectation value for <Sz*Sz> correlation function          --> (NmBIN, NumCal)
		real(rp), allocatable :: RSpmSpm(:, :)   ! Expectation value for <S+S->+<S-S+> correlation function    --> (NmBIN, NumCal) 
		real(rp), allocatable :: RSpnSpn(:, :)   ! Expectation value for <S_i*S_j> correlation function        --> (NmBIN, NumCal) 
		real(rp), allocatable :: RDenDen(:, :)   ! Expectation value for <n_i*n_j> density-density correlation --> (NmBIN, NumCal)
		real(rp), allocatable :: RGreenF(:, :)   ! Expectation value for Total Green Function                  --> (NmBIN, NumCal)
!______________________________________________________________________________________________________________	  
!_______________________ 7. Final expectation values for equal-time reciporcal space __________________________
!_____________________________ Structures and momentum distribution for all BIN's _____________________________
!______________________________________________________________________________________________________________
		complex(rp), allocatable :: KGreenT(:, :, :)  ! Momentum distribution for the system
		complex(rp), allocatable :: KSpinZZ(:, :, :)  ! Z-direction Spin structrue factor in reciprocal space 
		complex(rp), allocatable :: KSpinPM(:, :, :)  ! xy-plane Spin structrue factor in reciprocal space 
		complex(rp), allocatable :: KSpnSpn(:, :, :)  ! Total Spin structrue factor in reciprocal space 
		complex(rp), allocatable :: KDenDen(:, :, :)  ! Density-Density correlation function in reciprocal space
!______________________________________________________________________________________________________________	  
!_______________________ 8. Final expectation values for equal-time reciporcal space __________________________
!___________________________ Structures factors for AFM, Density, SOC for all BIN's ___________________________
!______________________________________________________________________________________________________________
		real(rp), allocatable :: KOrderP(:, :)           ! All the structure factors
!______________________________________________________________________________________________________________	  
!_______________________ 9. Final expectation values for time-displayed reciporcal space ______________________
!_____________________________ Green Function and structure factors for all BIN's _____________________________
!______________________________________________________________________________________________________________
		real(rp), allocatable :: KGreenT_Tau(:, :, :)    ! Gup+Gdw for calculation of momentum distribution time-displaced
		real(rp), allocatable :: KSpinZZ_Tau(:, :, :)    ! The <Sz*Sz> correlation function time-displaced
		real(rp), allocatable :: KSpinPM_Tau(:, :, :)    ! The <S+S->+<S-S+> correlation function time-displaced
		real(rp), allocatable :: KSpnSpn_Tau(:, :, :)    ! The <S+S->+<S-S+> correlation function time-displaced
		real(rp), allocatable :: KDenDen_Tau(:, :, :)    ! <n_i*n_j> density-density correlation function time-displaced
		real(rp), allocatable :: RGreenG_Tau(:, :, :)    ! Used to determine the global single-particle gap of the system
		
		real(rp), allocatable :: AllKGreenP_Tau(:, :, :) ! Physical Green Function -> cc^+
!**************************************************************************************************************	  
!______________________________ For calculating the Spin Chern Number _________________________________________
!**************************************************************************************************************
		complex(rp), allocatable :: GIw0k(:, :, :, :)    ! The zero-frequency Green Function at all k points

	end module
!__________________________________________________________________________________________________________________________________________  
!________________________________________ End Module ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine InitMCObserv()
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  InitMCObserv()
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to perform the initialization for observables allocated in subroutine InitAllocObs, just give zero
!                  values for the matrices.
! KEYWORDS: Initialization of matrices in Observable module.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-30
! DESCRIPTION:
!
!     Initialization of constants in PQMC, including:
!             (0) Initialization of some measurement parameters;
!             (1) Allocate the observable matrices and initializations.
!
!     Input:  (none)   Output: (none)
!
! MODULES USED: RealPrecsn, CoreParamt, Observable
! SUBROUTINES CALLED:  (none)
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use Observable
		implicit none
!______________________________________________________________________________________________________________	  
!____________________________ Main calculations of Observables Initialization _________________________________
!______________________________________________________________________________________________________________
!_______________________________________________________________________________________	  
!__________________ (0) Static observables _____________________________________________
!_______________________________________________________________________________________
		DenOcc = cmplx(0.0_rp, 0.0_rp, rp)     ! Initialization
		SpinZc = cmplx(0.0_rp, 0.0_rp, rp)     ! Initialization
		SPINZZ = cmplx(0.0_rp, 0.0_rp, rp)     ! Initialization
		SPINPM = cmplx(0.0_rp, 0.0_rp, rp)     ! Initialization
		GREENT = cmplx(0.0_rp, 0.0_rp, rp)     ! Initialization
		DenDen = cmplx(0.0_rp, 0.0_rp, rp)     ! Initialization
		
		NObs     = 0
		Obser    = cmplx(0.0_rp, 0.0_rp, rp)
		ExtOb    = cmplx(0.0_rp, 0.0_rp, rp)
		CorrFTmp = cmplx(0.0_rp, 0.0_rp, rp)
		CorrFunc = cmplx(0.0_rp, 0.0_rp, rp)
!_______________________________________________________________________________________	  
!__________________ (1) Dynamic observables ____________________________________________
!_______________________________________________________________________________________
		if(IfTAU == 1) then
			NObsT = 0
			SigNt = 0.0_rp
			
			GREENT_Tau = cmplx(0.0_rp, 0.0_rp, rp)               ! Initialization
			SPINZZ_Tau = cmplx(0.0_rp, 0.0_rp, rp)               ! Initialization
			SPINPM_Tau = cmplx(0.0_rp, 0.0_rp, rp)               ! Initialization
			DenDen_Tau = cmplx(0.0_rp, 0.0_rp, rp)               ! Initialization
			DenOcc_Tau = cmplx(0.0_rp, 0.0_rp, rp)               ! Initialization
			GreenG_Tau = cmplx(0.0_rp, 0.0_rp, rp)               ! Initialization
!**************************************************************************************************************	  
!______________________________ For calculating the Spin Chern Number _________________________________________
!**************************************************************************************************************
			GIw0k = cmplx(0.0_rp, 0.0_rp, rp)
		end if
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$