!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: A few subroutines for performing the data processing after the whole PQMC simulations, including:
!                  (0) Calculate the avgerage and errorbar for expectation energies of all the energy terms;
!                  (1) Calculate the avgerage for accepting ratio of three different interaction terms;
!                  (2) Calculate the avgerage and errorbar for real space correlation functions in only a few lattice site pairs;
!                  (3) Calculate the avgerage and errorbar for reciprocal space correlation functions.
! COMMENT: PQMC Finalization process.
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-30
! PURPOSE: Different subroutines are introduced as following:
!             
!   PQMCDataPr --> Subroutine to call subroutines to perform data processing and output the processed data;
!   EngDocMean --> Subroutine to calculate the avgerage and errorbar for expectation energies of all the energy terms;
!   RCorrFMean --> Subroutine to calculate the avgerage and errorbar for real space correlation functions;
!   KCorrFMean --> Subroutine to calculate the avgerage and errorbar for reciprocal space correlation functions;
!   MeanErrBar --> Subroutine to calculate the average and errorbar for a vector data.
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!________________________________________ Begin Module ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine PQMCDataPr()
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  PQMCDataPr()
! TYPE:     subroutine
! PURPOSE:  This Subroutine performs the data process for part of the results from PQMC.
! KEYWORDS: Data process.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-30
! DESCRIPTION:
!
!     V interaction updating.
!
!     Input:  (none).   Output: (none).
!
! MODULES USED: RealPrecsn, CoreParamt, Observable
! SUBROUTINES CALLED: EngDocMean, RCorrFMean, KCorrFMean
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________
		use RealPrecsn
		use CoreParamt      
		use Observable
		implicit none
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer Nk       ! Loop integer for k points
		integer I1       ! Loop for 1,2,3,4 matrix elements
		integer I2       ! Loop for 1,2,3,4 matrix elements
		integer I0
		integer NT
		
		integer Nx
		integer Ny
		
		integer Itp1     !
		integer Itp2     !
		
		real(rp) Avgr
		real(rp) ErrB
		
		real(rp) VecK(2) ! The k point coordinates

		real(rp) EngAvg(7, 2)       ! Mean values and error bar for the energies and Double Occupancy
		real(rp) CrFAvg(5, NumCal)   ! Mean values for the three correlation functions
		real(rp) CrFErr(5, NumCal)   ! Error bar values for the three correlation functions
		
		real(rp) KCrFAvg(2*NOrbt*NOrbt)          ! Average values used in momentum space correlation function
		real(rp) KCrFErr(2*NOrbt*NOrbt)          ! Errorbar used in momentum space correlation function
		
		character(100) FileName
!______________________________________________________________________________________________________________	  
!_________________________________ Main calculations for Data process _________________________________________
!______________________________________________________________________________________________________________
!**************************************************************************************************	  
!__________  0. Energies and the Double Occupancy, and output data ________________________________
!**************************************************************************************************
			call EngDocMean(7, 2, EngAvg)
			write(OutPF(15), "(es25.16, A)", advance = "no") HubbU, char(9)
			do I1 = 1, 7
				write(OutPF(15), "(A, es25.16, A, es25.16)", advance = "no") char(9), EngAvg(I1, 1), char(9), EngAvg(I1, 2)
			enddo
			write(OutPF(15), "()")
!**************************************************************************************************	  
!__________ 1. Global Accepting Ratio for U, V, J interactions, and output data ___________________
!**************************************************************************************************
			write(OutPF(16), "(es25.16, A, A, es25.16)") HubbU, char(9), char(9), AcptU/NmBin
!**************************************************************************************************	  
!__________ 2. Calculate RSpZSpZ, RSpmSpm, RSpnSpn, RDenDen, RGreenF, and output data _____________
!**************************************************************************************************
			call RCorrFMean(CrFAvg, CrFErr)
			do I1 = 17, 21
				write(OutPF(I1), "(es25.16, A)", advance = "no") HubbU, char(9)
			enddo
			do I1 = 1, NumCal
				write(OutPF(17), "(A, es25.16)", advance = "no") char(9), CrFAvg(1, I1)
				write(OutPF(18), "(A, es25.16)", advance = "no") char(9), CrFAvg(2, I1)
				write(OutPF(19), "(A, es25.16)", advance = "no") char(9), CrFAvg(3, I1)
				write(OutPF(20), "(A, es25.16)", advance = "no") char(9), CrFAvg(4, I1)
				write(OutPF(21), "(A, es25.16)", advance = "no") char(9), CrFAvg(5, I1)
			enddo
			do I1 = 17, 21
				write(OutPF(I1), "(A)", advance = "no") char(9)
			enddo
			do I1 = 1, NumCal
				write(OutPF(17), "(A, es25.16)", advance = "no") char(9), CrFErr(1, I1)
				write(OutPF(18), "(A, es25.16)", advance = "no") char(9), CrFErr(2, I1)
				write(OutPF(19), "(A, es25.16)", advance = "no") char(9), CrFErr(3, I1)
				write(OutPF(20), "(A, es25.16)", advance = "no") char(9), CrFErr(4, I1)
				write(OutPF(21), "(A, es25.16)", advance = "no") char(9), CrFErr(5, I1)
			enddo
			do I1 = 17, 21
				write(OutPF(I1), "()")
			enddo
!**************************************************************************************************	  
!__________ 3. Calculate KSpinZZ, KSpinPM, KGreenT, KDenDen, and output data ______________________
!**************************************************************************************************
			if(IfPr1 == 1 .and. IfPr2 ==1) then
				do Nk = 1, NumNC+NumL1+NumL2+1
					VecK = KpList(Nk, 1) * X1Vec + KpList(Nk, 2) * X2Vec
					Itp1 = NOrbt * NOrbt
					
					call KCorrFMean(NmBIN, Itp1, KGreenT(1:NmBIN, Nk, 1:Itp1), KCrFAvg, KCrFErr)
					write(OutPF(22), "(es25.16, A, es25.16, A)", advance = "no") VecK(1), char(9), VecK(2), char(9)
					write(OutPF(23), "(es25.16, A, es25.16, A)", advance = "no") VecK(1), char(9), VecK(2), char(9)
					do I1 = 1, Itp1
						write(OutPF(22), "(A, es25.16, A, es25.16)", advance = "no") char(9), KCrFAvg(2*I1-1), char(9), KCrFAvg(2*I1)
						write(OutPF(23), "(A, es25.16, A, es25.16)", advance = "no") char(9), KCrFErr(2*I1-1), char(9), KCrFErr(2*I1)
						if( (mod(I1, NOrbt) == 0) .and. (I1 .ne. Itp1) ) then
							write(OutPF(22), "(A)", advance = "no") char(9)
							write(OutPF(23), "(A)", advance = "no") char(9)
						end if
					enddo
					write(OutPF(22), "()")
					write(OutPF(23), "()")
					
					call KCorrFMean(NmBIN, Itp1, KSpinZZ(1:NmBIN, Nk, 1:Itp1), KCrFAvg, KCrFErr)
					write(OutPF(24), "(es25.16, A, es25.16, A)", advance = "no") VecK(1), char(9), VecK(2), char(9)
					write(OutPF(25), "(es25.16, A, es25.16, A)", advance = "no") VecK(1), char(9), VecK(2), char(9)
					do I1 = 1, Itp1
						write(OutPF(24), "(A, es25.16, A, es25.16)", advance = "no") char(9), KCrFAvg(2*I1-1), char(9), KCrFAvg(2*I1)
						write(OutPF(25), "(A, es25.16, A, es25.16)", advance = "no") char(9), KCrFErr(2*I1-1), char(9), KCrFErr(2*I1)
						if( (mod(I1, NOrbt) == 0) .and. (I1 .ne. Itp1) ) then
							write(OutPF(24), "(A)", advance = "no") char(9)
							write(OutPF(25), "(A)", advance = "no") char(9)
						end if
					enddo
					write(OutPF(24), "()")
					write(OutPF(25), "()")
			
					call KCorrFMean(NmBIN, Itp1, KSpinPM(1:NmBIN, Nk, 1:Itp1), KCrFAvg, KCrFErr)
					write(OutPF(26), "(es25.16, A, es25.16, A)", advance = "no") VecK(1), char(9), VecK(2), char(9)
					write(OutPF(27), "(es25.16, A, es25.16, A)", advance = "no") VecK(1), char(9), VecK(2), char(9)
					do I1 = 1, Itp1
						write(OutPF(26), "(A, es25.16, A, es25.16)", advance = "no") char(9), KCrFAvg(2*I1-1), char(9), KCrFAvg(2*I1)
						write(OutPF(27), "(A, es25.16, A, es25.16)", advance = "no") char(9), KCrFErr(2*I1-1), char(9), KCrFErr(2*I1)
						if( (mod(I1, NOrbt) == 0) .and. (I1 .ne. Itp1) ) then
							write(OutPF(26), "(A)", advance = "no") char(9)
							write(OutPF(27), "(A)", advance = "no") char(9)
						end if
					enddo
					write(OutPF(26), "()")
					write(OutPF(27), "()")
					
					call KCorrFMean(NmBIN, Itp1, KSpnSpn(1:NmBIN, Nk, 1:Itp1), KCrFAvg, KCrFErr)
					write(OutPF(28), "(es25.16, A, es25.16, A)", advance = "no") VecK(1), char(9), VecK(2), char(9)
					write(OutPF(29), "(es25.16, A, es25.16, A)", advance = "no") VecK(1), char(9), VecK(2), char(9)
					do I1 = 1, Itp1
						write(OutPF(28), "(A, es25.16, A, es25.16)", advance = "no") char(9), KCrFAvg(2*I1-1), char(9), KCrFAvg(2*I1)
						write(OutPF(29), "(A, es25.16, A, es25.16)", advance = "no") char(9), KCrFErr(2*I1-1), char(9), KCrFErr(2*I1)
						if( (mod(I1, NOrbt) == 0) .and. (I1 .ne. Itp1) ) then
							write(OutPF(28), "(A)", advance = "no") char(9)
							write(OutPF(29), "(A)", advance = "no") char(9)
						end if
					enddo
					write(OutPF(28), "()")
					write(OutPF(29), "()")
			
					call KCorrFMean(NmBIN, Itp1, KDenDen(1:NmBIN, Nk, 1:Itp1), KCrFAvg, KCrFErr)
					write(OutPF(30), "(es25.16, A, es25.16, A)", advance = "no") VecK(1), char(9), VecK(2), char(9)
					write(OutPF(31), "(es25.16, A, es25.16, A)", advance = "no") VecK(1), char(9), VecK(2), char(9)
					do I1 = 1, Itp1
						write(OutPF(30), "(A, es25.16, A, es25.16)", advance = "no") char(9), KCrFAvg(2*I1-1), char(9), KCrFAvg(2*I1)
						write(OutPF(31), "(A, es25.16, A, es25.16)", advance = "no") char(9), KCrFErr(2*I1-1), char(9), KCrFErr(2*I1)
						if( (mod(I1, NOrbt) == 0) .and. (I1 .ne. Itp1) ) then
							write(OutPF(30), "(A)", advance = "no") char(9)
							write(OutPF(31), "(A)", advance = "no") char(9)
						end if
					enddo
					write(OutPF(30), "()")
					write(OutPF(31), "()")

				enddo
!________________________________________________________________________________________	  
!_______________________________ All the Structure factors ______________________________
!________________________________________________________________________________________
				write(OutPF(44), "(es25.16, A)", advance = "no") HubbU, char(9)
				do I1 = 1, 8
					call MeanErrBar(2, NmBIN, KOrderP(2:NmBIN, I1), Avgr, ErrB)
					write(OutPF(44), "(A, es25.16, A, es25.16)", advance = "no") char(9), Avgr, char(9), ErrB
					if( (mod(I1, 2) == 0) .and. (I1 .ne. 8) ) then
						write(OutPF(44), "(A)", advance = "no") char(9)
					end if
				enddo
				
			end if
!**************************************************************************************************	  
!__________ 4. The time-displaced quantities: Mean and errorbar ___________________________________
!**************************************************************************************************
			if(IfTAU == 1) then
				do NT = 0, NmTDM
!__________________________________________________________________________________	  
!_______ KGreenT_TAU, KSpinZZ_TAU, KSpinPM_TAU, KSpnSpn_TAU, KDenDen_TAU __________
!__________________________________________________________________________________						
					do I0 = 38, 42
						write(OutPF(I0), "(es25.16, A)", advance = "no") NT * Dltau, char(9)
					enddo
					
					do I1 = 1, 6
!_____________________________________________________________________________  
!____________________________ KGreenT_TAU ____________________________________
!_____________________________________________________________________________
						call MeanErrBar(2, NmBIN, KGreenT_TAU(NT, I1, 2:NmBIN), Avgr, ErrB)
						write(OutPF(38), "(A, es25.16, A, es25.16)", advance = "no") char(9), Avgr, char(9), ErrB
!_____________________________________________________________________________  
!____________________________ KSpinZZ_TAU ____________________________________
!_____________________________________________________________________________						
						call MeanErrBar(2, NmBIN, KSpinZZ_TAU(NT, I1, 2:NmBIN), Avgr, ErrB)
						write(OutPF(39), "(A, es25.16, A, es25.16)", advance = "no") char(9), Avgr, char(9), ErrB
!_____________________________________________________________________________  
!____________________________ KSpinPM_TAU ____________________________________
!_____________________________________________________________________________							
						call MeanErrBar(2, NmBIN, KSpinPM_TAU(NT, I1, 2:NmBIN), Avgr, ErrB)
						write(OutPF(40), "(A, es25.16, A, es25.16)", advance = "no") char(9), Avgr, char(9), ErrB
!_____________________________________________________________________________  
!____________________________ KSpnSpn_TAU ____________________________________
!_____________________________________________________________________________							
						call MeanErrBar(2, NmBIN, KSpnSpn_TAU(NT, I1, 2:NmBIN), Avgr, ErrB)
						write(OutPF(41), "(A, es25.16, A, es25.16)", advance = "no") char(9), Avgr, char(9), ErrB
!_____________________________________________________________________________  
!____________________________ KDenDen_TAU ____________________________________
!_____________________________________________________________________________						
						call MeanErrBar(2, NmBIN, KDenDen_TAU(NT, I1, 2:NmBIN), Avgr, ErrB)
						write(OutPF(42), "(A, es25.16, A, es25.16)", advance = "no") char(9), Avgr, char(9), ErrB
						
						if(I1 == 2 .or. I1 == 4) then
							do I0 = 38, 42
								write(OutPF(I0), "(A)", advance = "no") char(9)
							enddo
						end if
			
					enddo
					
					do I0 = 38, 42
						write(OutPF(I0), "()")
					enddo
!__________________________________________________________________________________	  
!_______________________________ RGreenG_Tau ______________________________________
!__________________________________________________________________________________					
					write(OutPF(43), "(es25.16, A)", advance = "no") NT * Dltau, char(9)
					do I1 = 1, 2
						call MeanErrBar(2, NmBIN, RGreenG_Tau(NT, I1, 2:NmBIN), Avgr, ErrB)
						write(OutPF(43), "(A, es25.16, A, es25.16)", advance = "no") char(9), Avgr, char(9), ErrB
					enddo
					write(OutPF(43), "()")
					
				enddo
!**************************************************************************************************	  
!__________ 5. Output all the single-particle GF data _____________________________________________
!**************************************************************************************************
				if(IfOtptGF == 1) then
					Nk = 0
					do Nx = 0, NumL1-1
						do Ny = 0, NumL2-1
							Nk = Nk + 1
							write(FileName, "('Output/99_', I2.2, '_', I2.2, '_GreenFunc.txt')") Nx, Ny
							open(ExtPF(47), file = trim(FileName))
							do NT = 0, NmTDM
								write(ExtPF(47), "(es25.16, A)", advance = "no") NT * Dltau, char(9)
								do I1 = 1, 2*NOrbt*NOrbt
									write(ExtPF(47), "(A, es25.16)", advance = "no") char(9), AllKGreenP_Tau(Nk, NT, I1)/(NmBIN-1)
									if(I1 == NOrbt*NOrbt) then
										write(ExtPF(47), "(A)", advance = "no") char(9)
									end if
								enddo
								write(ExtPF(47), "()")
							enddo
							close(ExtPF(47))
						enddo
					enddo
				end if
				
			end if

	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!________________________________________ Begin Module ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine EngDocMean(NDim, MDim, EngAvg)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  EngDocMean(NDim, MDim, EngAvg)
! TYPE:     subroutine
! PURPOSE:  This Subroutine calculates the mean values and error bar for the energies and double occupancy.
! KEYWORDS: Data process.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-30
! DESCRIPTION:
!
!     V interaction updating.
!
!     Input:  (none).   Output: (none).
!
! MODULES USED: RealPrecsn, CoreParamt, Observable
! SUBROUTINES CALLED: MeanErrBar
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________
		use RealPrecsn
		use CoreParamt
		use Observable
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		integer NDIm
		integer MDim
		real(rp) EngAvg(NDim, MDim)
!______________________________________________________________________________________________________________	  
!_________________________________ Main calculations for Data process _________________________________________
!______________________________________________________________________________________________________________
		call MeanErrBar(2, NmBIN, REHopt1(2:NmBIN), EngAvg(1, 1), EngAvg(1, 2))    ! For the full NN hopping term
		call MeanErrBar(2, NmBIN, REHoptd(2:NmBIN), EngAvg(2, 1), EngAvg(2, 2))    ! For the dimerized NN hopping term 
		call MeanErrBar(2, NmBIN, REHopSC(2:NmBIN), EngAvg(3, 1), EngAvg(3, 2))    ! For the Intrinsic SOC hopping term
		call MeanErrBar(2, NmBIN, REHopt3(2:NmBIN), EngAvg(4, 1), EngAvg(4, 2))    ! For the 3NN hopping term
		call MeanErrBar(2, NmBIN, REUIntr(2:NmBIN), EngAvg(5, 1), EngAvg(5, 2))    ! For the intra-layer HubbU interaction term
		call MeanErrBar(2, NmBIN, RETotal(2:NmBIN), EngAvg(6, 1), EngAvg(6, 2))    ! For the whole model Hamiltonian
		call MeanErrBar(2, NmBIN, RDouOcc(2:NmBIN), EngAvg(7, 1), EngAvg(7, 2))    ! For the double occupancy

	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!________________________________________ Begin Module ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine RCorrFMean(CrFAvg, CrFErr)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  RCorrFMean(CrFAvg, CrFErr)
! TYPE:     subroutine
! PURPOSE:  This Subroutine calculates the mean values and error bar for the three correlation functions in real space including:
!                 (1) The <Sz*Sz> correlation function;
!                 (2) The <S+*S- + S-*S+> correlation function;
!                 (3) The <n_i * n_j> correlation function;
! KEYWORDS: Data process.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-30
! DESCRIPTION:
!
!     V interaction updating.
!
!     Input:  (none).   Output: (none).
!
! MODULES USED: RealPrecsn, CoreParamt, Observable
! SUBROUTINES CALLED: MeanErrBar
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________
		use RealPrecsn
		use CoreParamt
		use Observable
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		real(rp) CrFAvg(5, NumCal)   ! Mean values for the three correlation functions
		real(rp) CrFErr(5, NumCal)   ! Error bar values for the three correlation functions
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer Id
!______________________________________________________________________________________________________________	  
!_________________________________ Main calculations for Data process _________________________________________
!______________________________________________________________________________________________________________
		do Id = 1, NumCal
			call MeanErrBar(2, NmBIN, RSpZSpZ(2:NmBIN, Id), CrFAvg(1, Id), CrFErr(1, Id))   ! <Sz * Sz>         --> Without -<A_i>*<A_j>
			call MeanErrBar(2, NmBIN, RSpmSpm(2:NmBIN, Id), CrFAvg(2, Id), CrFErr(2, Id))   ! <S+*S- + S-*S+>
			call MeanErrBar(2, NmBIN, RSpnSpn(2:NmBIN, Id), CrFAvg(3, Id), CrFErr(3, Id))   ! <n_i * n_j>       --> With -<A_i>*<A_j> 
			call MeanErrBar(2, NmBIN, RDenDen(2:NmBIN, Id), CrFAvg(4, Id), CrFErr(4, Id))   ! <n_i * n_j>       --> With -<A_i>*<A_j> 
			call MeanErrBar(2, NmBIN, RGreenF(2:NmBIN, Id), CrFAvg(5, Id), CrFErr(5, Id))   ! <c_i^+ * c_j>    
		enddo

	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!________________________________________ Begin Module ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine KCorrFMean(NDim1, NDim2, CMatrix, KCrFAvg, KCrFErr)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  KCorrFMean(NDim1, NDim2, CMatrix, KCrFAvg, KCrFErr)
! TYPE:     subroutine
! PURPOSE:  This Subroutine calculates the mean values and error bar for the correlation functions in reciprocal space including:
!                 (1) The <Sz*Sz> correlation function;
!                 (2) The <S+*S- + S-*S+> correlation function;
!                 (3) The <n_i * n_j> correlation function;
!                 (4) Momentum distribution.
! KEYWORDS: Data process.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-30
! DESCRIPTION:
!
!     V interaction updating.
!
!     Input:  NDim    --> Input integer as dimension of CMatrix(N, 4) matrix;
!             CMatrix --> Input complex matrix for calculating average and errorbar;
!
!     Output: KCrFAvg --> The output average values for real and complex parts of CMatrix;
!             KCrFErr --> The output errorbar values for real and complex parts of CMatrix;
!
! MODULES USED: RealPrecsn
! SUBROUTINES CALLED: MeanErrBar
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________
		use RealPrecsn
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		integer NDim1
		integer NDim2
		complex(rp) CMatrix(NDim1, NDim2)
		real(rp) KCrFAvg(2*NDim2)
		real(rp) KCrFErr(2*NDim2)
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer Id
!______________________________________________________________________________________________________________	  
!_________________________________ Main calculations for Data process _________________________________________
!______________________________________________________________________________________________________________
		do Id = 1, NDim2
			call MeanErrBar(2, NDim1, real(CMatrix(2:NDim1, Id)), KCrFAvg(2*Id-1), KCrFErr(2*Id-1))
			call MeanErrBar(2, NDim1, imag(CMatrix(2:NDim1, Id)), KCrFAvg(2*Id), KCrFErr(2*Id))
		enddo

	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!________________________________________ Begin Module ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine MeanErrBar(M, N, VecA, Avgr, ErrB)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  MeanErrBar(M, N, VecA, Avgr, ErrB)
! TYPE:     subroutine
! PURPOSE:  This Subroutine calculates the average and errorbar value for an array of data VecA(M:N).
! KEYWORDS: Mean and ErrorBar.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-30
! DESCRIPTION:
!
!     V interaction updating.
!
!     Input:  (none).   Output: (none).
!
! MODULES USED: RealPrecsn
! SUBROUTINES CALLED:  
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________
		use RealPrecsn
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		integer M            ! Starting integer index for data processing
		integer N            ! Ending integer index for data processing
		real(rp) VecA(M:N)   ! Data vector
		real(rp) Avgr        ! Average value
		real(rp) ErrB        ! Error Bar
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer I1
!______________________________________________________________________________________________________________	  
!_____________________________ Main calculations Mean values and error bar ____________________________________
!______________________________________________________________________________________________________________
		Avgr = sum(VecA(M:N)) / ((N-M+1)*1.0_rp)
		ErrB = 0.0_rp
		do I1 = M, N
			ErrB = ErrB + (VecA(I1) - Avgr)**2
		enddo
		ErrB = ErrB / ((N-M+1)*1.0_rp)
		ErrB = sqrt(ErrB)
		ErrB = ErrB / sqrt( (N-M)*1.0_rp )

	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$