!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: A single subroutine used to read in the calculation parameters used in PQMC simulations, including:
!                  (0) Size of the finite lattice                               --> NumL1, NumL2
!                  (1) Six model parameters of t-SOC-t3-U-V-J model             --> Hopt1, HopSC, Hopt3, HubbU, HubbV, ExchJ
!                  (2) Dimer paremeter on first NN bond of A site               --> Dimer
!                  (3) External magnetic field and twisted boundary             --> Magnt, TwstX
!                  (4) Projection parameter, Number of time slices and Wrapping --> BetaT, LTrot, NWrap
!                  (5) BIN number, Sweep number and measurement number          --> NmBin, NSwep, MeaTt
!                  (6) Time-displayed quantities calculations                   --> IfTAU, NmTDM
! COMMENT: Read in calculation parameters.
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-30
! PURPOSE: Different subroutines are introduced as following:
!             
!   PQMCReadPm --> Subroutine to read in the calculation parameters for PQMC simulation from input file;
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
   


!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
   subroutine PQMCReadPm() 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  PQMCReadPm()
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to read the input calculation parameters for PQMC calculations.
! KEYWORDS: Read Input model parameters.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-01
! DESCRIPTION:
!
!     Read the lattice size and model parameters from the input file. If the input file doesn't exit, we ouput the error massage.
!
!     Input: (none);  Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt, StdInOutSt, Observable
! SUBROUTINES CALLED:  (none)
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
      use RealPrecsn
      use CoreParamt
      use Observable
      use StdInOutSt
      implicit none
!______________________________________________________________________________________________________________	  
!_____________________________ Reading process in this subroutine _____________________________________________
!______________________________________________________________________________________________________________
!_________________________________________________________________________________________________	  
!____________________ (0) The realistic reading process in this subroutine _______________________
!_________________________________________________________________________________________________
         open(57, err = 58, file = "Input/ModelParam.txt")
         read(57, *) NumL1, NumL2                               ! L1, L2
         read(57, *) IfPr1, IfPr2                               ! OBC or PBC in the simulation
         read(57, *) Hopt1, HopSC, Hopt3, HubbU                 ! t1, SOC, t3, U
         read(57, *) Dimer                                      ! Dimer
         read(57, *) Magnt, TwstX                               ! External magnetic field, Twisted Boundary Phase in x-drection
         read(57, *) BetaT, LTrot, NWrap                        ! Beta, LTrot, NWrap
         read(57, *) NmBin, NSwep, MeaTt                        ! NBIN, NSweep, MeaTt
         read(57, *) IfTAU, NmTDM                               ! IFTAU, NmTDM
         read(57, *) IfOtptGF                                   ! IfOtptGF
         close(57)
         
         if( (mod(NumL1, 2) == 0) .and. (mod(NumL1, 6) .ne. 0) ) then
            IfKPt = 0
            IfMPt = 1
         end if
         if( (mod(NumL1, 3) == 0) .and. (mod(NumL1, 6) .ne. 0) ) then
            IfKPt = 1
            IfMPt = 0
         end if
         if( mod(NumL1, 6) == 0 ) then
            IfKPt = 1
            IfMPt = 1
         end if
         
         write(        *, "('PQMCReadPm: ', 6I6)") NumL1, NumL2, NumL1*NumL2, 2*NumL1*NumL2
         write(        *, "()")
         write(MonitorOt, "('PQMCReadPm: ', 6I6)") NumL1, NumL2, NumL1*NumL2, 2*NumL1*NumL2
         write(MonitorOt, "()")
         go to 59
!_________________________________________________________________________________________________	  
!____________________ (1) If the input file not exit, initialize _________________________________
!_________________________________________________________________________________________________	
58       write(        *, "('PQMCReadPm: The input file ModelParam.txt does not exist! Use default initialization!')")
         write(        *, "()")
         write(MonitorOt, "('PQMCReadPm: The input file ModelParam.txt does not exist! Use default initialization!')")
         write(MonitorOt, "()")
         NumL1 = 3; NumL2 = 3
         IfPr1 = 1; IfPr2 = 1
         Hopt1 = - 1.0_rp
         HopSC = + 0.0_rp
         Hopt3 = - 0.0_rp
         HubbU = + 4.0_rp
         Magnt = 0; TwstX = 0.0001_rp
         BetaT = 40; LTrot = 1000; NWrap = 10
         NmBin = 21; NSwep =  200; MeaTt = 500
         IfTAU =  0; NmTDM = 0
!_________________________________________________________________________________________________	  
!____________________ (2) If the reading process succeed, return _________________________________
!_________________________________________________________________________________________________		
59       continue
      
   end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$