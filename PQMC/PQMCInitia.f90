!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: A few subroutines used for performing the PQMC initialization before the whole simulation, including:
!                  (0) Setting some basic integer parameters;
!                  (1) Make lattice and calculate the unit cell and lattice site lists;
!                  (2) Allocate the observables matrices and initialization.
!                  (3) Calculate the Hubbard-Stratonovich Transformation related quantities;
!                  (4) List all the calculation parameters in PQMC simulation to a output file. 
!                  (5) Output file handles for the PQMC calculated data;
!                  (6) Calculate the projector slater determinant and exponential hopping matrices;
!                  (7) Initialize the Four Ising fields for U, V, J interaction terms.
!                  (8) Allocate vectors and matrices for the PQMC simulation and initialization;	 
! COMMENT: PQMC initialization process.
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-30
! PURPOSE: Different subroutines are introduced as following:
!             
!   PQMCInitia   --> Subroutine to call other subroutines to perform the total PQMC initializations;
!   InitConstant --> Subroutine to perform initialization of some Constants in global PQMC calculations;
!   InitLattStru --> Subroutine to perform initialization of the finite lattice applied in PQMC calculations;
!   InitAllocObs --> Subroutine to perform initialization of both Static and Dynamic observables in PQMC calculations;
!   InitHSTransf --> Subroutine to perform initialization of HS Tranformation related quantities for U, V, J interactions;
!   InitListCons --> Subroutine to list all constants defined in PQMC Simulations;
!   InitMCOutput --> Subroutine to set all the output files during PQMC simulations;
!   InitProjExph --> Subroutine to perform initialization of Slater Determinant and exponential hopping matrices;
!   InitIsingFld --> Subroutine to perform initialization of all Ising fields for U, V, J interactions;
!   InitMCVecMat --> Subroutine to perform initialization of Key vectors and matrices in PQMC calculations;
!
!   GenerateIfIt --> Subroutine to calculate the integers for controling calculations for some term in model Hamiltonian;
!   GenerateList --> Subroutine to be called by InitLattStru to calculate the Lists for the finite lattice.
!   ListItem     --> Subroutine to output a single constant by some format.
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine PQMCInitia()
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  PQMCInitia()
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to perform the PQMC initialization before the whole PQMC simulation.
! KEYWORDS: PQMC initialization.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-30
! DESCRIPTION:
!
!     PQMC Initialization including the following processes:
!                  (0) Initialize the MPI setting before the calculations;
!                  (0) Setting some basic integer parameters;
!                  (1) Make lattice and calculate the unit cell and lattice site lists;
!                  (2) Allocate the observables matrices and initialization.
!                  (3) Calculate the Hubbard-Stratonovich Transformation related quantities;
!                  (4) Output file handles for the PQMC calculated data;
!                  (5) Calculate the projector slater determinant and exponential hopping matrices;
!                  (6) Initialize the Four Ising fields for U, V, J interaction terms.
!                  (7) Allocate vectors and matrices for the PQMC simulation and initialization;
!                  (8) List all the calculation parameters in PQMC simulation to a output file.          
!
!
!     Input:  (none)   Output: (none)
!
! MODULES USED: (none)
! SUBROUTINES CALLED:  InitConstant; 
!                      InitLattStru; 
!                      InitAllocObs;
!                      InitHSTransf;
!                      InitListCons;
!                      InitMCOutput; 
!                      InitProjExph; 
!                      InitIsingFld;
!                      InitMCVecMat.
!                      
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		implicit none
!______________________________________________________________________________________________________________	  
!_______________________________ Main calculations of PQMC Initialization _____________________________________
!______________________________________________________________________________________________________________
!**************************************************************************************************	  
!___________ 0. Initialization of some Constants in program _______________________________________
!**************************************************************************************************
		call InitConstant()
!**************************************************************************************************	  
!___________ 1. Initialization of the finite lattice of the system ________________________________
!**************************************************************************************************
		call InitLattStru()
!**************************************************************************************************	  
!___________ 2. Allocation of Observables vectors and matrices ____________________________________
!**************************************************************************************************
		call InitAllocObs()
!**************************************************************************************************	  
!___________ 3. Initialization of HS Tranformation related quantities _____________________________
!**************************************************************************************************
		call InitHSTransf()
!**************************************************************************************************	  
!___________ 4. List all the calcaulation constants _______________________________________________
!**************************************************************************************************		
		call InitListCons()
!**************************************************************************************************	  
!___________ 5. Initialization of output files handles for the PQMC calculated data _______________
!**************************************************************************************************
		call InitMCOutput()
!**************************************************************************************************	  
!___________ 6. Initialization of Slater Determinant and exponential hopping matrices _____________
!**************************************************************************************************
		call InitProjExph()
!**************************************************************************************************	  
!___________ 7. Initialization of Four Ising fields for U, V, J interactions ______________________
!**************************************************************************************************
		call InitIsingFld()
!**************************************************************************************************	  
!___________ 8. Initialization of Core PQMC vectors and matrices __________________________________
!**************************************************************************************************
		call InitMCVecMat()
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$