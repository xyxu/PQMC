!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: A few subroutines for performing the PQMC finialization after the whole PQMC simulation, including:
!                  (0) Close the opened output txt files in the whole PQMC simulation program;
!                  (1) Finalization of vectors and matrices in CoreParamt module;
!                  (2) Finalization of vectors and matrices in Observable module.
! COMMENT: PQMC Finalization process.
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-30
! PURPOSE: Different subroutines are introduced as following:
!             
!   PQMCFinalz     --> Subroutine to call subroutines to perform the total PQMC finalization;
!   CloseOutptFile --> Subroutine to close all the opened output txt files;
!   CoreParamtFinl --> Subroutine to perform yje finalization for CoreParamt module;
!   ObservableFinl --> Subroutine to perform yje finalization for Observable module.
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine PQMCFinalz()
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  PQMCFinalz()
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to perform the PQMC Finalization after the whole PQMC simulation.
! KEYWORDS: PQMC initialization.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-30
! DESCRIPTION:
!
!     PQMC Initialization including the following processes:
!                  (0) Close all the output files opened in the program;
!                  (1) Finalization of CoreParamt module;
!                  (2) Finalization of Observable module.
!
!     Input:  (none)   Output: (none)
!
! MODULES USED: (none)
! SUBROUTINES CALLED: CloseOutptFile, CoreParamtFinl, ObservableFinl
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________
		implicit none
!______________________________________________________________________________________________________________	  
!_______________________________ Main calculations of PQMC Finalization _______________________________________
!______________________________________________________________________________________________________________
!__________________________________________________________________________________________________	  
!______________________ 0. Close all the output files opened in the program _______________________
!__________________________________________________________________________________________________
		call CloseOutptFile()
!__________________________________________________________________________________________________	  
!______________________ 1. Finalization of CoreParamt module ______________________________________
!__________________________________________________________________________________________________
		call CoreParamtFinl()  
!__________________________________________________________________________________________________	  
!______________________ 2. Finalization of Observable module ______________________________________
!__________________________________________________________________________________________________
		call ObservableFinl()	
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine CloseOutptFile()  
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  CloseOutptFile()  
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to close all the output files we opened during the PQMC simulation.
! KEYWORDS: Close all the opened output files.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-30
! DESCRIPTION:
!
!     Close all the opened output files. 
!
!     Input:  (none)   Output: (none)
!
! MODULES USED: CoreParamt
! SUBROUTINES CALLED: (none)
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use CoreParamt
		use StdInOutSt
		use Observable
		implicit none
!______________________________________________________________________________________________________________	  
!______________________________ Close all the opened output files _____________________________________________
!______________________________________________________________________________________________________________
!________________________________________________________________________________________	  
!_______ (0) The monitor outputing process ______________________________________________
!________________________________________________________________________________________			
			close(MonitorOt)
!________________________________________________________________________________________	  
!_______ (0) Calculation information and noninteracting single-particle energy level ____
!________________________________________________________________________________________
			close(OutPF(1))     ! For outputting the PQMC simulations information
			close(OutPF(2))     ! For Outputting the Single-Particle energy levels of the noninteracting Hamiltonian matrix
!________________________________________________________________________________________	  
!_______ (1) Expectation energies and accepting ratio for all BINs simulations __________
!________________________________________________________________________________________			
			close(OutPF(3))     ! For outputting the PQMC calculated Expectation energies for all the terms for all the NmBIN simulations
			close(OutPF(4))     ! For outputting the accepting ratio for U, V, J interactions in every BIN calculation
!________________________________________________________________________________________	  
!_______ (2) Real space correlation functions for all BINs simulations __________________
!________________________________________________________________________________________
			close(OutPF(5))     ! For Outputting the Sz-Sz correlation function in real space for all the NmBIN simulations --> Without -<A_i>*<A_j>
			close(OutPF(6))     ! For Outputting the S+*S- correlation function in real space for all the NmBIN simulations
			close(OutPF(7))     ! For Outputting the S_i*S_j correlation function in real space for all the NmBIN simulations
			close(OutPF(8))     ! For Outputting the ni-nj correlation function in real space for all the NmBIN simulations --> With -<A_i>*<A_j>
			close(OutPF(9))     ! For Outputting the <c_{i\u}^+ c_{j\u}> + <c_{i\d}^+ c_{j\d}>
!________________________________________________________________________________________	  
!_______ (3) Reciprocal space correlation functions at (0, 0) for all BINs simulations __
!________________________________________________________________________________________
			close(OutPF(10))    ! Momentum distribution correlation at (0, 0) point
			close(OutPF(11))    ! SpZ-SpZ correlation at (0, 0) point               --> Without -<A_i>*<A_j>
			close(OutPF(12))    ! Sp+-Sp- correlation at (0, 0) point    
			close(OutPF(13))    ! For Outputting total spin-spin correlation function in reciprocal space
			close(OutPF(14))    ! Density-Density correlation at (0, 0) point       --> With -<A_i>*<A_j> 	
!________________________________________________________________________________________	  
!_______ (4) Final average and errorbar for expectation energies and accepting ratio ____
!________________________________________________________________________________________	
			close(OutPF(15))    ! For average expectation energy values for all terms after all NBIN calculations
			close(OutPF(16))    ! For Outputting the accepting ratio for U, V, J interactions
!________________________________________________________________________________________	  
!_______ (5) Final average and errorbar for Real space correlation functions ____________
!________________________________________________________________________________________		
			close(OutPF(17))    ! Output the mean and error bar of <Sz * Sz> correlation function (real space) --> Without -<A_i>*<A_j>
			close(OutPF(18))    ! Output the mean and error bar of (<S+*S->+<S-*S+>)(real space)
			close(OutPF(19))    ! Output the mean and error bar of <S_i * S_j> correlation function (real space)
			close(OutPF(20))    ! Output the mean and error bar of <n_i*n_j> correlation function (real space) --> With -<A_i>*<A_j>
			close(OutPF(21))    ! Output the mean and error bar of (n_{ij\u}+n_{ij\d}) occupancy (real space)
!________________________________________________________________________________________	  
!_______ (6) Final average and errorbar for reciprocal space correlation functions ______
!________________________________________________________________________________________
			if(IfPr1 == 1 .and. IfPr2 ==1) then
				close(OutPF(22))     ! Output the momentum distribution after all the NmBIN simulations    
				close(OutPF(23))     ! Errorbar of momentum distribution after all the NmBIN simulations 
				
				close(OutPF(24))     ! Output the z-direction spin structure factor after all the NmBIN simulations  --> Without -<A_i>*<A_j>
				close(OutPF(25))     ! Errorbar of z-direction spin structure factor after all the NmBIN simulations --> Without -<A_i>*<A_j>
				
				close(OutPF(26))     ! Output the    xy-plane spin structure factor after all the NmBIN simulations
				close(OutPF(27))     ! Errorbar of    xy-plane spin structure factor after all the NmBIN simulations
				
				close(OutPF(28))     ! Output the       Total spin structure factor after all the NmBIN simulations    
				close(OutPF(29))     ! Errorbar of   the Total spin structure factor after all the NmBIN simulations
				
				close(OutPF(30))     ! Output the density-density after all the NmBIN simulations                    --> Without -<A_i>*<A_j>
				close(OutPF(31))     ! Errorbar of density-density after all the NmBIN simulations                   --> Without -<A_i>*<A_j>    
			end if
!________________________________________________________________________________________	  
!_______ (7) Time-displaced quantities in every BIN at some k points ____________________
!________________________________________________________________________________________
			if(IfTAU == 1) then
				close(OutPF(32)) ! Output the average of GreenT_tau(k) after all BINs
				close(OutPF(33)) ! Output the average of <Sz(\tau) * Sz>(k) after all BINs
				close(OutPF(34)) ! Output the average of <Spm(\tau) * Spm>(k) after all BINs
				close(OutPF(35)) ! Output the average of <Spn(\tau) * Spn>(k) after all BINs
				close(OutPF(36)) ! Output the average of <n_i(\tau) * n_j>(k) after all BINs
				close(OutPF(37)) ! Output the average of global single-particle Green Function after all BINs
	
				close(OutPF(38)) ! Output the final mean value of GreenT_tau(k) after all BINs
				close(OutPF(39)) ! Output the final mean value of <Sz(\tau) * Sz>(k) after all BINs
				close(OutPF(40)) ! Output the final mean value of <Spm(\tau) * Spm>(k) after all BINs
				close(OutPF(41)) ! Output the final mean value of <Spn(\tau) * Spn>(k) after all BINs
				close(OutPF(42)) ! Output the final mean value of <n_i(\tau) * n_j>(k) after all BINs
				close(OutPF(43)) ! Output the final mean value of global single-particle Green Function after all BINs
			end if
!________________________________________________________________________________________	  
!_______ (8) Final average and errorbar for all structure factors _______________________
!________________________________________________________________________________________
			if(IfPr1 == 1 .and. IfPr2 ==1) then
				close(44)
			end if
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$




!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine CoreParamtFinl()  
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  CoreParamtFinl()  
! TYPE:     subroutine
! PURPOSE:  This Subroutine performs the finalization for the CoreParamt module, mainly deallocate arrays.
! KEYWORDS: Finalization of module.
! AUTHOR:   Yuan-Yao He, Rong-Qiang He
! TIME:     2014-12-30
! DESCRIPTION:
!
!     Finalization for module CoreParamt. 
!
!     Input:  (none)   Output: (none)
!
! MODULES USED: CoreParamt
! SUBROUTINES CALLED: (none)
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use CoreParamt
		use Observable
		implicit none
!______________________________________________________________________________________________________________	  
!______________________________ Deallocate the allocated Arrays _______________________________________________
!______________________________________________________________________________________________________________
!________________________________________________________________________________________________	  
!_________________________________ Lattice lists ________________________________________________
!________________________________________________________________________________________________ 
		deallocate(UCList   )   ! The two integer indexes for a unit cell          
		deallocate(InvUCList)   ! From two integer indexes to get Unit cell number
		deallocate(NNUCList )   ! Record the six NN Unit cells for all unit cells
		deallocate(KpList   )   ! The two integer indexes for a single k point
		deallocate(InvKpList)   ! From two integer indexes to get k point number
		deallocate(StList   )   ! Return the unit cell integer index for site and 1 or 2 for sublattice
		deallocate(InvStList)   ! From UC index and Orbit to get the lattice site index
		deallocate(NNBond   )   ! The NN lattice sites for A sublattice sites
		deallocate(TNNBond  )   ! The third-NN lattice sites for A sublattice sites
		deallocate(NNStBnd  )   ! Matrix elements 0, 1 indicating whether has NN hopping for (I1, I2) sites
		deallocate(NNUCBnd  )   ! Matrix elements 0, 1 indicating whether has NN unit cells for some unit cell
		deallocate(TNNsBnd  )   ! Matrix elements 0, 1 indicating whether has 3NN hopping for (I1, I2) sites
!________________________________________________________________________________________________	  
!____________ Ising fields for the four terms in Hubbard-Stratonovich Transformation ____________
!________________________________________________________________________________________________      
		if( abs(HubbU) > 1.0E-10_rp ) deallocate(IsngbU)        ! Ising fields for the intra-layer U  interaction term
!________________________________________________________________________________________________	  
!__________________ The projector matrix and Exponential Kinetic matrix _________________________
!________________________________________________________________________________________________        
		deallocate(ProjM)           ! The projector matrix, P(NumNS, NumNS) --> But we use part
		deallocate(ExpK0)           ! The exponential of Kinetic matrix, exp(-\Delta\tau * K)
		deallocate(ExpKI)           ! The inverse of exponential of Kinetic matrix, [exp(-\Delta\tau * K)]^{-1}
!________________________________________________________________________________________________	  
!______________ QMC updating and equal-time Green Function related matrices _____________________
!________________________________________________________________________________________________
		deallocate(ULeft)           ! The left side matrix: P^{\dagger} * B_i
		deallocate(URght)           ! The right side matrix: B_i * P 
		deallocate(ULtRt)           ! The left-right matrix: P^{\dagger} * {B_i} * P
		deallocate(ULtRtInv)        ! The inverse of left-right matrix: (P^{\dagger} * {B_i} * P)^{-1}
		deallocate(USTMt)             ! Record the ULeft or URght matrix where the UDV decomposition happens
		if(IfTAU == 1) then
			deallocate(ULeft_1)         ! The temporary Left side matrix 
			deallocate(URght_1)         ! The temporary right side matrix 
			deallocate(ULtRtInv_1)      ! The temporary inverse matrix of Left-right matrix
		end if
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine ObservableFinl()  
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  ObservableFinl()  
! TYPE:     subroutine
! PURPOSE:  This Subroutine performs the finalization for the Observable module, mainly deallocate arrays.
! KEYWORDS: Finalization of Observable module.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-30
! DESCRIPTION:
!
!     Finalization for module Observable. 
!
!     Input:  (none)   Output: (none)
!
! MODULES USED: Observable, CoreParamt
! SUBROUTINES CALLED:  (none)
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use Observable
		use CoreParamt
		implicit none
!______________________________________________________________________________________________________________	  
!____________________________ Deallocate allocated arrays in the simulations __________________________________
!______________________________________________________________________________________________________________
!****************************************************************************************************	  
!_____________________ 0. Equal-time Correlation functions for all lattice sites ____________________
!__________________________ in real space for every BIN, used for constructing ______________________
!______________________________ Structure factors and momentum distribution _________________________
!****************************************************************************************************
		deallocate(DenOcc)            ! The density occupation on all lattice sites
		deallocate(SpinZc)            ! The spin-z component on all lattice sites
		deallocate(SPINZZ)            ! The <Sz*Sz> correlation function
		deallocate(SPINPM)            ! The <S+S->+<S-S+> correlation function 
		deallocate(GREENT)            ! Total Gup+Gdw for calculation of momentum distribution
		deallocate(DenDen)            ! <n_i*n_j> density-density correlation function
!**************************************************************************************************** 
!_____________________ 1. Time-dispalyed Correlation functions for all lattice sites ________________
!____________________________ in real space for every BIN, used for constructing ____________________
!____________________________________ reciprocal space Green Function _______________________________
!****************************************************************************************************
		if(IfTAU == 1) then
			deallocate(SPINZZ_Tau)    ! The <Sz*Sz> correlation function
			deallocate(SPINPM_Tau)    ! The <S+S->+<S-S+> correlation function 
			deallocate(GREENT_Tau)    ! Total Gup+Gdw for calculation of momentum distribution
			deallocate(DenDen_Tau)    ! <n_i*n_j> density-density correlation function
			deallocate(DenOcc_Tau)    ! <n_i*n_j> density-density correlation function
			deallocate(GreenG_Tau)    ! The <S+S->+<S-S+> correlation function 
		end if
!****************************************************************************************************  
!_____________________ 5. Final expectation values for energies for all BIN's _______________________
!****************************************************************************************************
		deallocate(REHopt1)    ! Expectation energy for the full NN hopping term
		deallocate(REHoptd)    ! Expectation energy for the dimerized NN hopping term
		deallocate(REHopSC)    ! Expectation energy for the Intrinsic SOC term
		deallocate(REHopt3)    ! Expectation energy for the 3NN term
		deallocate(REUIntr)    ! Expectation energy for the On-site repulsion HubbU term --> With chemical potential
		deallocate(RETotal)    ! Expectation energy for the whole model Hamiltonian      --> With chemical potential
		deallocate(RDouOcc)    ! Expectation value for the double occupancy --> The average value
!****************************************************************************************************	  
!_____________________ 6. Final expectation values for CorrFunc for all BIN's in real space _________
!****************************************************************************************************      
		deallocate(RSpZSpZ)     ! SpinZ-SpinZ correlation functions in real space with NumCal lattice site pairs
		deallocate(RSpmSpm)     ! SpnPM-SpnPM correlation functions in real space with NumCal lattice site pairs
		deallocate(RSpnSpn)     ! <S_i*S_j> correlation functions in real space with NumCal lattice site pairs
		deallocate(RDenDen)     ! Density-Density correlation functions in real space with NumCal lattice site pairs
		deallocate(RGreenF)     ! Occupation in real space with NumCal lattice site pairs
!****************************************************************************************************	  
!_____________________ 7. Final expectation values for equal-time reciporcal space __________________
!___________________________ Structures and momentum distribution for all BIN's _____________________
!****************************************************************************************************	
		if(IfPr1 == 1 .and. IfPr2 ==1) then
			deallocate(KSpinZZ)     ! Z-direction Spin structrue factor in reciprocal space
			deallocate(KSpinPM)     ! xy-plane Spin structrue factor in reciprocal space 
			deallocate(KSpnSpn)     ! Spin-Spin correlation function in reciprocal space
			deallocate(KGreenT)     ! Momentum distribution for the system
			deallocate(KDenDen)     ! Density-Density correlation function in reciprocal space
			
			deallocate(KOrderP)     ! All the structure factors
		end if
!****************************************************************************************************		  
!_____________________ 8. Final expectation values for time-displayed reciporcal space ______________
!_____________________________ Green Function and structure factors for all BIN's ___________________
!****************************************************************************************************
		if(IfTAU == 1) then
			deallocate(KGreenT_Tau)    ! Gup+Gdw for calculation of momentum distribution time-displaced
			deallocate(KSpinZZ_Tau)    ! The <Sz*Sz> correlation function time-displaced
			deallocate(KSpinPM_Tau)    ! The <S+S->+<S-S+> correlation function time-displaced
			deallocate(KSpnSpn_Tau)    ! The <S+S->+<S-S+> correlation function time-displaced
			deallocate(KDenDen_Tau)    ! <n_i*n_j> density-density correlation function time-displaced
			deallocate(RGreenG_Tau)    ! Determine the global single-particle gap
			
			if(IfOtptGF == 1) then
				deallocate(AllKGreenP_Tau) ! The <S+S->+<S-S+> correlation function time-displaced
			end if
		end if
!**************************************************************************************************************	  
!______________________________ For calculating the Spin Chern Number _________________________________________
!**************************************************************************************************************
		if(IfTAU == 1) then
			deallocate(GIw0k)         ! The zero-frequency Green Function at all k points 
		end if
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$