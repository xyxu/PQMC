!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: This program performs the PQMC calculations of SU(N) extended Hubbard Model on 2D honeycomb lattice including: 
!               (1) Nearest-Neighbor hopping terms (can include the effect of magnetic field)  --> Hopt1 = -1.0_rp, Magnt = 0.0_rp
!               (2) The Intrinsic SOC term in Kane-Mele Model                                  --> HopSC = 
!               (3) The third-nearest-neighbor hopping terms                                   --> Hopt3 = 
!               (4) The on-site Hubbard Repulsion term                                         --> HubbU = 
!               (5) The nearest-neighbor exchange interaction                                  --> ExchJ = 
!               (6) The nearest-neighbor repulsion interaction                                 --> HubbV = 
!          Notice: (i) In the calculations, we introduce the on-site Ising fields for the U interaction term, while we introduce the 
!                        on-bond Ising fields for both the J interaction and V interaction term.
!                  (ii) For the J interaction term, we divide it into three different terms, the first term is the Kinetic energy term,
!                         the second term is the current term and this NN spin exchange can generate a V interaction term. So for the
!                         J interaction term, there is actually three different Ising fields. But in the calculations, we can merge
!                         the V interaction from the J interaction and the original V interaction in extended Hubbard Model. The two 
!                         Ising fields for J interaction term has 3*NumNC fields for every time slice.
!                           exp(-Dtau*H_J) = exp(-J3)*exp(-J2)*exp(-J1)*exp(-K3)*exp(-K2)*exp(-K1)
!                   (iii) The Ising fields for the V interaction term. 
!      All these calculations are performed for half-filling case, which preserves the particle-hole symmetry. So there is no minus
!         sign problem in numerical calculations.
! COMMENT: Used for total PQMC calculations.   
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-15   
! TARGET: The following physical properties of 2D extended Hubbard model on Honeycomb are calculated:
!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$___________________________________ New Changes for this program ___________________________________________$$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!$ 0. This program includes simulations with both the open and periodic boundary conditions;                  $$
!$ 1. This program incorporates bo the single-layer and bilayer cases;                                        $$
!$ 2. We set the monitor outputing file in this program, instead of outputing in the *.o* file.               $$
!$                                                                                                            $$ 
!$                                                                                                            $$
!$                                                                                                            $$
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
!
!
!
! FILES :          
! (1) F90_Files: 
!         
!      General Folder: 
!         HermDiag.f90 --> Subroutines for diagonalizing the real symmetric matrix;
!	       ReSyDiag.f90 --> Subroutines for diagonalizing the complex Hermitian matrix;
!	       MyRandom.f90 --> Subroutines for generating random number in [0, 1];
!         InvMatrx.f90 --> Subroutines for calculating inverse matrix of real or complex matrices;
!         MatrCmpr.f90 --> Subroutines for calculating the differences between two real and complex matrices;
!         MatrMult.f90 --> Subroutines for calculating real and complex matrices multiplications;
!         UDVdcmps.f90 --> Subroutines for performing UDV decomposition for real and complex matrices;
!
!      Main Folder:  
!         exe.f90          --> Main program for this PQMC numerical calculations
!         PQMC.f90         --> Total PQMC Calculations
!         PQMCReadPara.f90 --> Read the model parameters from input file
!         PQMCInitiate.f90 --> Make some important initialization of lattice, PQMC simulation parameters
!         PQMCSimulate.f90 --> Perform the total PQMC simulations
!         PQMCDataProc.f90 --> Perform the data process for PQMC calculated observables
!         PQMCFinalize.f90 --> Perform the finalization of modules and close output files
!                                                                                                                                          
!      Module Folder:       
!         TimeRecord.f90 --> Module for recording the total calculation time of this CDMFT program
!         StdInOutSt.f90 --> Module for setting the standard input and ouput files
!         RealPrecsn.f90 --> Module for real precision control calculations
!         CoreParamt.f90 --> Module for core parameters, quantities for CDMFT calculations (all Green Function matrices)
!         Observable.f90 --> Module for defining the physical quantities needs to be calculated in PQMC                                                     
!       
!      MathLib Folder
!         Diagonaliz.f90 --> Perform the diagonalization of real symmetric and complex Hermitian matrices
!         IniDiagMat.f90 --> Create real or complex diagonal matrices
!         MatCompare.f90 --> Compare two real and complex matrices
!         MatrixMult.f90 --> Calculate the Matrices multiplication for real and complex version
!         InverseMat.f90 --> Calculate the inverse matrix for real and complex square matrix
!         UDVDecomps.f90 --> Calculate the UDV decomposition for real and complex rectangle matrices
!
!      PQMC Folder:                                                                                                                                 
!         PQMCReadPm.f90 --> Subroutine to read calculation parameters from the input file
!         PQMCInitia.f90 --> Subroutine to perform the total initialization of PQMC simulations
!         PQMCSimult.f90 --> Subroutine to perform the core PQMC simulation process
!         PQMCDataPr.f90 --> Subroutine to perform the data process after the whole PQMC simulation
!         PQMCFinalz.f90 --> Subroutine to perform the total finalization of PQMC simulations
!
!      Special Folder:                                                                                                                                 
!         InitCalGrF.f90 --> Subroutine to calculate the UL, UR, ULRInv corresponding to G(M)
!         SweepMeasu.f90 --> Subroutine to perform the Sweep and measurement during PQMC simulation
!         SweepM2One.f90 --> Subroutine to perform the Sweep from M time slice to 1 time slice
!         SweepIntVl.f90 --> Subroutine to perform some interval calculation between the SweepM2One and SweepOne2M subroutines
!         SweepOne2M.f90 --> Subroutine to perform the Sweep from one time slice to M time slice
!         MagFdPhase.f90 --> Function used to evaluate phase factor from magnetic field and twisted boundary
!         MatMultKnt.f90 --> Subroutines used to perform multiplication of matrix with exponential hopping matrix
!         MatMultUVJ.f90 --> Subroutines used to perform multiplication of matrix with exponential U, V, J matrix
!         ObsStatics.f90 --> Subroutines to perform the measurement during the PQMC simulation
!         PostStatic.f90 --> Subroutines to perform the post process of calculated data at last in every BIN
!         SettgPhase.f90 --> Subroutine to calculate the phase factor during the whole PQMC simulation
!         SubroutnTm.f90 --> Subroutines to record the calculation time for important subroutines
!         UDVReOrtho.f90 --> Subroutine to perform the UDV decomposition for URght or ULeft matrix
!         UpdateFdJ0.f90 --> Subroutine to perform the Ising fields updating in J interaction
!         UpdateFldU.f90 --> Subroutine to perform the Ising fields updating in U interaction
!         UpdateFdV0.f90 --> Subroutine to perform the Ising fields updating in V interaction
!  
! (2) Some Important Details
!
!
!
! (3) Physical Properties Calculations
!
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin Main Program __________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________


!______________________________________________________________________________________________________________	      
!___________________ First Include all the f90 files, only used for the calculation on the server______________
!______________________________________________________________________________________________________________
   include "Main/FileInclude.f90" 

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin Main Program __________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
   program Main
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  Main
! TYPE:     Main Program
! PURPOSE:  This Subroutine performs the total Calculation.
! KEYWORDS: Total PQMC calculations.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-05-25          -->  110
! DESCRIPTION:
!
!     By iteration, the Ground states energy can be quickly found since the convergence is rapid. 
!
!     Input: (none);
!
!     Outpt: (none). 
!
! MODULES USED: (none)
! SUBROUTINES CALLED:  (none)
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this Module ________________________________________
!______________________________________________________________________________________________________________
		implicit none	  
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer(8) time1 
		integer(8) time2
!______________________________________________________________________________________________________________	      
!______________ 0. The initialization process for TimeRecord, StdInOutSt, RealPrecsn modules __________________
!______________________________________________________________________________________________________________
		call TimeRecordInit()
		call StdInOutStInit()
		call RealPrecsnInit()
!______________________________________________________________________________________________________________	  
!_____________________ Get the Time for the strating point of Whole PQMC pragram ______________________________
!______________________________________________________________________________________________________________	  
		call SubroutineBgn("PQMC", 1, time1)
!______________________________________________________________________________________________________________	  
!_____________________ Call subroutine to perform the full PQMC numerical calculations ________________________
!______________________________________________________________________________________________________________	  
		call PQMC()
!______________________________________________________________________________________________________________	  
!_____________________ Get the Time for the Ending point of Whole PQMC pragram ________________________________
!______________________________________________________________________________________________________________	  
		call SubroutineEnd("PQMC", 1, time1, time2)
		
	end program 
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End Main Program ____________________________________________________________________________________ 
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$    
