!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: To include all the calculation files in this total program. 
! AUTHOR:  Yuan-Yao He
! DATE:    2015-01-30
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    
!______________________________________________________________________________________________________________	  
!__________________________________ (0) Math library in the program ___________________________________________
!______________________________________________________________________________________________________________    
    !include "MathLib/daxpy.f"
    !include "MathLib/dcabs1.f"
    !include "MathLib/dgedi.f"
    !include "MathLib/dgefa.f"
    !include "MathLib/dgemm.f"
    !include "MathLib/dgemv.f"
    !include "MathLib/dger.f"
    !include "MathLib/dscal.f"
    !include "MathLib/dswap.f"
    !include "MathLib/F01QCF.f"
    !include "MathLib/F01QEF.f"
    !include "MathLib/F01RCF.f"
    !include "MathLib/F01REF.f"
    !include "MathLib/F06FBF.f"
    !include "MathLib/F06FJF.f"
    !include "MathLib/F06FRF.f"
    !include "MathLib/F06HBF.f"
    !include "MathLib/F06HRF.f"
    !include "MathLib/F06KJF.f"
    !include "MathLib/F06QHF.f"
    !include "MathLib/F06THF.f"
    !include "MathLib/HermDiag.f"
    !include "MathLib/idamax.f"
    !include "MathLib/InvMatrx.f"
    !include "MathLib/izamax.f"
    !include "MathLib/lsame.f"
    !include "MathLib/MatrCmpr.f"
    !include "MathLib/MatrMult.f"
    !include "MathLib/MyRandom.f"
    !include "MathLib/P01ABF.f"
    !include "MathLib/P01ABW.f"
    !include "MathLib/P01ABY.f"
    !include "MathLib/P01ABZ.f"
    !include "MathLib/ReSyDiag.f"
    !include "MathLib/s_matrix.f"
    !include "MathLib/UDVdcmps.f"
    !include "MathLib/UDVReOrtho.f"
    !include "MathLib/X02AJF.f"
    !include "MathLib/X04AAF.f"
    !include "MathLib/X04BAF.f"
    !include "MathLib/xerbla.f"
    !include "MathLib/zaxpy.f"
    !include "MathLib/zdscal.f"
    !include "MathLib/zgedi.f"
    !include "MathLib/zgefa.f"
    !include "MathLib/zgemm.f"
    !include "MathLib/zgemv.f"
    !include "MathLib/zgerc.f"
    !include "MathLib/zscal.f"
    !include "MathLib/zswap.f"
!______________________________________________________________________________________________________________	  
!__________________________________ (1) Files in the Module File folder _______________________________________
!______________________________________________________________________________________________________________
    include "Module/TimeRecord.f90"       ! Module for recording the total calculation time of this CDMFT program
    include "Module/StdInOutSt.f90"       ! Module for setting the standard input and ouput files
    include "Module/RealPrecsn.f90"       ! Module for real precision control calculations
    include "Module/CoreParamt.f90"       ! Module for core parameters, quantities for CDMFT calculations (all Green Function matrices)
    include "Module/Observable.f90"       ! Module for defining the physical quantities needs to be calculated in PQMC
    include "Module/QMCTimeRec.f90"       ! Module for record the calculation time consumed by every parts of a single BIN simulation
!______________________________________________________________________________________________________________	  
!__________________________________ (2) Files in the General File folder ______________________________________
!______________________________________________________________________________________________________________
    include "General/HermDiag.f90"        ! Subroutines for diagonalizing the complex Hermitian matrix
    include "General/InvMatrx.f90"        ! Subroutines for calculating inverse matrix of real or complex matrices
    include "General/MatrCmpr.f90"        ! Subroutines for calculating the differences between two real and complex matrices
    include "General/MatrMult.f90"        ! Subroutines for calculating real and complex matrices multiplications
    include "General/MyRandom.f90"        ! Subroutines for generating random real number in [0, 1]
    include "General/ReSyDiag.f90"        ! Subroutines for diagonalizing the real symmetric matrix
    include "General/UDVdcmps.f90"        ! Subroutines for performing UDV decomposition for real and complex matrices
!______________________________________________________________________________________________________________	  
!__________________________________ (3) Files in the Main File folder _________________________________________
!______________________________________________________________________________________________________________
    include "Main/PQMC.f90"               ! Total PQMC Calculations
    include "Main/PQMCReadPara.f90"       ! Read the model parameters from input file
    include "Main/PQMCInitiate.f90"       ! Make some important initialization of lattice, PQMC simulation parameters
    include "Main/PQMCSimulate.f90"       ! Perform the total PQMC simulations
    include "Main/PQMCDataProc.f90"       ! Perform the data process for PQMC calculated observables
    include "Main/PQMCFinalize.f90"       ! Perform the finalization of modules and close output files
!______________________________________________________________________________________________________________	  
!__________________________________ (4) Files in the PQMC File folder _________________________________________
!______________________________________________________________________________________________________________
    include "PQMC/PQMCReadPm.f90"         ! Subroutine to read calculation parameters from the input file          --> PQMCReadPara.f90
    include "PQMC/PQMCInitia.f90"         ! Subroutine to perform the total initialization of PQMC simulations     --> PQMCInitiate.f90
    include "PQMC/PQMCSimult.f90"         ! Subroutine to perform the core PQMC simulation process                 --> PQMCSimulate.f90
    include "PQMC/PQMCDataPr.f90"         ! Subroutine to perform the data process after the whole PQMC simulation --> PQMCDataProc.f90
    include "PQMC/PQMCFinalz.f90"         ! Subroutine to perform the total finalization of PQMC simulations       --> PQMCFinalize.f90
!______________________________________________________________________________________________________________	  
!__________________________________ (5) Files in the Special File folder ______________________________________
!______________________________________________________________________________________________________________
    include "Special/A1Constant.f90"      ! Subroutine to perform some constants initialization before PQMC simulation
    include "Special/A2LattStru.f90"      ! Subroutine to perform finite lattice initialization before PQMC simulation
    include "Special/A3AllocObs.f90"      ! Subroutine to perform allocatation of observables before PQMC simulation
    include "Special/A4HSTransf.f90"      ! Subroutine to calculate HS-transformation related quantities before PQMC simulation
    include "Special/A5ListCons.f90"      ! Subroutine to list all the calculation parameters applied before PQMC simulation
    include "Special/A6MCOutput.f90"      ! Subroutine to perform output files initialization before PQMC simulation
    include "Special/A7ProjExph.f90"      ! Subroutine to calculate projector slater determinant before PQMC simulation
    include "Special/A8IsingFld.f90"      ! Subroutine to perform Ising fields initialization before PQMC simulation
    include "Special/A9MCVecMat.f90"      ! Subroutine to perform key quantities initializations before PQMC simulation
    include "Special/SweepMeasu.f90"      ! Subroutine to perform the Sweep and measurement during PQMC simulation
    include "Special/SweepM2One.f90"      ! Subroutine to perform the Sweep from M time slice to 1 time slice
    include "Special/SweepIntVl.f90"      ! Subroutine to perform some interval calculation between the SweepM2One and SweepOne2M subroutines
    include "Special/SweepOne2M.f90"      ! Subroutine to perform the Sweep from one time slice to M time slice
    include "Special/MatMultKnt.f90"      ! Subroutines used to perform multiplication of matrix with exponential hopping matrix
    include "Special/MatMultUVJ.f90"      ! Subroutines used to perform multiplication of matrix with exponential U, V, J matrix
    include "Special/ObsStatics.f90"      ! Subroutines to perform the equal-time measurements during the PQMC simulation
    include "Special/ObDynMeasu.f90"      ! Subroutines to perform the time-displayed measurements during the PQMC simulation
    include "Special/ObDynPropr.f90"      ! Subroutines to perform the time-displayed measurements during the PQMC simulation
    include "Special/ObsDynamic.f90"      ! Subroutines to perform the time-displayed measurements during the PQMC simulation
    include "Special/ObStaGrFun.f90"      ! Subroutines to calculate the equal-time Green Function matrix during equal-T measurements
    include "Special/ObStaEnrgy.f90"      ! Subroutines to calculate expectation energies during equal-T measurements
    include "Special/ObStaCorFR.f90"      ! Subroutines to calculate real space correlation functions during equal-T measurements
    include "Special/ObStaCorFK.f90"      ! Subroutines to calculate reciprocal space correlation functions during equal-T measurements
    include "Special/PostStatic.f90"      ! Subroutines to perform the post process of calculated equal-time data at last in every BIN
    include "Special/PostDynamc.f90"      ! Subroutines to perform the post process of calculated time-displayed data at last in every BIN
    include "Special/SettgPhase.f90"      ! Subroutine to calculate the phase factor during the whole PQMC simulation
    include "Special/SubroutnTm.f90"      ! Subroutines to record the calculation time for important subroutines
    include "Special/UDVReOrtho.f90"      ! Subroutine to perform the UDV decomposition for URght or ULeft matrix
    include "Special/ULRInverse.f90"      ! Subroutine to calculate the inverse matrix of ULtRt matrix
    include "Special/UpdateFldU.f90"      ! Subroutine to perform the Ising fields updating in intra-layer U interaction