!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: A single subroutine to perform data processing after all the PQMC simulations, and count the time consumed by this process.
! COMMENT: PQMC data processing.
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-30
! PURPOSE: Different subroutines are introduced as following:
!             
!   PQMCDataProc --> Subroutine to perform data processing after all the PQMC simulations and count the time consumed by this process.
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	
	
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine PQMCDataProc()
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  PQMCDataProc()
! TYPE:     subroutine
! PURPOSE:  This Subroutine performs the whole data process of PQMC calculation after the simulation, and count the consumed time.
! KEYWORDS: PQMC data processing.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-30
! DESCRIPTION:
!
!     After the PQMC simulations, we need to perform the data processing for the calculation results.
!
!     Input: (none);  Output: (none).
!
! MODULES USED: (none)
! SUBROUTINES CALLED: (1) SubroutineBgn;
!                     (2) PQMCDataPr;
!                     (3) SubroutineEnd.
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this Module ________________________________________
!______________________________________________________________________________________________________________
		implicit none	  
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer(8) time1 
		integer(8) time2	
!______________________________________________________________________________________________________________	  
!_____________________ Get the Time for the strating point of data processing _________________________________
!______________________________________________________________________________________________________________	  
		call SubroutineBgn("PQMCDataProc", 8, time1)
!______________________________________________________________________________________________________________	  
!_____________________ Call subroutine to perform the data processing _________________________________________
!______________________________________________________________________________________________________________	  
		call PQMCDataPr()
!______________________________________________________________________________________________________________	  
!_____________________ Get the Time for the Ending point of data processing ___________________________________
!______________________________________________________________________________________________________________	  
		call SubroutineEnd("PQMCDataProc", 8, time1, time2)
		
	end subroutine	 
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________ 
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 