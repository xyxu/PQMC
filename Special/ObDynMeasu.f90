!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: A single subroutine used for performing the time-displaced measurements in PQMC simulations, including:
!                  (0) Time-displaced       single-particle Green Function GREENT_Tau(NumNC, NOrbt, NOrbt, 0:NmTDM);
!                  (1) Time-displaced         Spz-Spz correlation function SPINZZ_Tau(NumNC, NOrbt, NOrbt, 0:NmTDM);
!                  (2) Time-displaced         Spm-Spm correlation function SPINPM_Tau(NumNC, NOrbt, NOrbt, 0:NmTDM);
!                  (3) Time-displaced density-density correlation function DenDen_Tau(NumNC, NOrbt, NOrbt, 0:NmTDM);
!          These real space correlation functions are preparing for the calculation of reciprocal space correlations.
!
!          Defnitions of these time-displaced Green Functions:
!                 GT0_{i,j} = + <c_i(\tau)c_j^+>
!                 G0T_{i,j} = + <c_j^+(\tau)c_i>
!                 G00_{i,j} = + <c_ic_j^+>
!                 GTT_{i,j} = + <c_i(\tau)c_j^+(\tau)>
!
! COMMENT: PQMC Observation process time-displaced.
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-11
! PURPOSE: Different subroutines are introduced as following:
!             
!   ObDynMeasu --> Subroutine to call other subroutines to perform the equal-time measurements in PQMC simulations;
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine ObDynMeasu(NT, GT0Up, G0TUp, GTTUp, G00Up) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  ObDynMeasu(NT, GT0Up, G0TUp, GTTUp, G00Up) 
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to measure the time-displaced physical quantities during the sweep of PQMC in the calculations.
! KEYWORDS: Calculate the static physical quantities.
! AUTHOR:   Yuan-Yao He
! TIME:     2015-01-30
! DESCRIPTION:
!
!     Measure the time-displaced physical quantities during the sweep of PQMC.
!
!                 GT0_{i,j} = + <c_i(\tau)c_j^+>
!                 G0T_{i,j} = + <c_j^+(\tau)c_i>
!                 G00_{i,j} = + <c_ic_j^+>
!                 GTT_{i,j} = + <c_i(\tau)c_j^+(\tau)>
!
!     Input: NT    --> Imaginary-time \tau in analytic expression;
!            GT0Up --> <c_i(\tau)c_j^+>
!            G0TUp --> <c_j^+(\tau)c_i>
!            GTTUp --> <c_ic_j^+>
!            G00Up --> <c_i(\tau)c_j^+(\tau)>
!
!     Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt
! SUBROUTINES CALLED: GrFStatics, ExpectEngy, CorreFunct
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use Observable
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		integer NT
		complex(rp) GT0Up(NumNS, NumNS)   ! <c_i(\tau)c_j^+>
		complex(rp) G0TUp(NumNS, NumNS)   ! <c_j^+(\tau)c_i>
		complex(rp) GTTUp(NumNS, NumNS)   ! <c_ic_j^+>
		complex(rp) G00Up(NumNS, NumNS)   ! <c_i(\tau)c_j^+(\tau)>
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer Id            ! Iteration integer for lattice sites
		integer Jd            ! Iteration integer for lattice sites

		integer NL1           ! Integer for layers
		integer NL2           ! Integer for layers
		
		integer Nu_1          ! The unit cell index integer for the I1-th lattice site
		integer No_1          ! The orbital index integer for the I1-th lattice site
		integer Nu_2          ! The unit cell index integer for the I2-the lattice site 
		integer No_2          ! The orbital index integer for the I2-th lattice site
		
		integer imj           ! Integer index for distance of (Nu_1, Nu_2) unit cells
		
		integer Itp1          ! Temporary integer 
		integer Itp2          ! Temporary integer
		
		real(rp) Rtp1         ! Temporary real number 
		real(rp) Rtp2         ! Temporary real number 
		
		complex(rp) Ztp1      ! Temporary complex number 
		complex(rp) Ztp2      ! Temporary complex number 
		complex(rp) Ztp3      ! Temporary complex number 

		complex(rp), allocatable :: G00Dw(:, :)   ! The Spin-down <c_i(\tau)c_j^+>
		complex(rp), allocatable :: GTTDw(:, :)   ! The Spin-down <c_j^+(\tau)c_i>
		complex(rp), allocatable :: G0TDw(:, :)   ! The Spin-down <c_ic_j^+>
		complex(rp), allocatable :: GT0Dw(:, :)   ! The Spin-down <c_i(\tau)c_j^+(\tau)>
!______________________________________________________________________________________________________________	  
!__________________________________ Allocate Array and Initializations ________________________________________
!______________________________________________________________________________________________________________
		allocate(G00Dw(NumNS, NumNS))
		allocate(GTTDw(NumNS, NumNS))
		allocate(G0TDw(NumNS, NumNS))
		allocate(GT0Dw(NumNS, NumNS))

		G00Dw = cmplx(0.0_rp, 0.0_rp, rp)
		GTTDw = cmplx(0.0_rp, 0.0_rp, rp)
		G0TDw = cmplx(0.0_rp, 0.0_rp, rp)
		GT0Dw = cmplx(0.0_rp, 0.0_rp, rp)
!______________________________________________________________________________________________________________	  
!_____________________________ Main calculations for all the static quantities ________________________________
!______________________________________________________________________________________________________________
!**************************************************************************************************
!__________ 0. Calculate the spin-down GFs from Spin-up part by canonical transformation __________
!**************************************************************************************************
		do Id = 1, NumNS
			Rtp1 = + 1.0_rp
			if(StList(Id, 2) == 1) Rtp1 = - 1.0_rp
			do Jd = 1, NumNS
				Rtp2 = + 1.0_rp
				if(StList(Jd, 2) == 1) Rtp2 = - 1.0_rp
				Ztp1 = cmplx(0.0_rp, 0.0_rp, rp)
				if(Id == Jd) Ztp1 = cmplx(1.0_rp, 0.0_rp, rp)
				GT0Dw(Id, Jd) = cmplx(Rtp1*Rtp2, 0.0_rp, rp) * conjg(G0TUp(Jd, Id))
				G0TDw(Id, Jd) = cmplx(Rtp1*Rtp2, 0.0_rp, rp) * conjg(GT0Up(Jd, Id))
				G00Dw(Id, Jd) = cmplx(Rtp1*Rtp2, 0.0_rp, rp) * ( Ztp1 - conjg(G00Up(Jd, Id)) )
				GTTDw(Id, Jd) = cmplx(Rtp1*Rtp2, 0.0_rp, rp) * ( Ztp1 - conjg(GTTUp(Jd, Id)) )
			enddo
		enddo
!**************************************************************************************************
!__________ 1. Get the phase factor from the input phase __________________________________________
!**************************************************************************************************
		Rtp1 = 1.0_rp
		Rtp2 = dble(Phase)
		if(Rtp2 < 0.0_rp) Rtp1 = - 1.0_rp
		Ztp1 = Phase / cmplx(dble(Phase), 0.0_rp, rp)
!**************************************************************************************************
!__________ 2. Calculate the GreenT_Tau, SpinZZ_Tau, SpinPM_Tau, DenDen_Tau Matrices ______________
!**************************************************************************************************
		do Id = 1, NumNS
			Nu_1 = StList(Id, 1)
			No_1 = StList(Id, 2)
			
			Ztp3 = cmplx(2.0_rp, 0.0_rp, rp) - GTTUp(Id, Id) - GTTDw(Id, Id)
			DenOcc_Tau(Nu_1, No_1, NT) = DenOcc_Tau(Nu_1, No_1, NT) + Ztp3 * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)
			
			do Jd = 1, NumNS
				Nu_2 = StList(Jd, 1)
				No_2 = StList(Jd, 2)
				imj = IminusJ(Nu_1, Nu_2)
!_______________________________________________________________________________________	  
!_________________ The time-displaced single-particle Green Function ___________________
!_______________________________________________________________________________________				
!				Ztp2 = G0TUp(Jd, Id) + G0TDw(Jd, Id)
!           Ztp2 = GT0Up(Jd, Id) + GT0Dw(Jd, Id)
				Ztp2 = GT0Up(Jd, Id)
				GreenT_Tau(imj, No_1, No_2, NT) = GreenT_Tau(imj, No_1, No_2, NT) + Ztp2 * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)
!_______________________________________________________________________________________	  
!________________________ The Spz-Spz correlation Function _____________________________
!_______________________________________________________________________________________				
				Ztp2 = G0TUp(Jd, Id) * GT0Up(Id, Jd) + G0TDw(Jd, Id) * GT0Dw(Id, Jd) + &
							& ( GTTUp(Id, Id) - GTTDw(Id, Id) ) * ( G00Up(Jd, Jd) - G00Dw(Jd, Jd) ) 
				Ztp2 = Ztp2 / cmplx(4.0_rp, 0.0_rp, rp)
				SpinZZ_Tau(imj, No_1, No_2, NT) = SpinZZ_Tau(imj, No_1, No_2, NT) + Ztp2 * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)
!_______________________________________________________________________________________	  
!________________________ The Spm-Spm correlation Function _____________________________
!_______________________________________________________________________________________				
				Ztp2 = ( G0TUp(Jd, Id) * GT0Dw(Id, Jd) + G0TDw(Jd, Id) * GT0Up(Id, Jd) ) / 2.0_rp
				SpinPM_Tau(imj, No_1, No_2, NT) = SpinPM_Tau(imj, No_1, No_2, NT) + Ztp2 * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)
!_______________________________________________________________________________________	  
!________________________ The Density-Density correlation Function _____________________
!_______________________________________________________________________________________				
				Ztp2 = G0TUp(Jd, Id) * GT0Up(Id, Jd) + G0TDw(Jd, Id) * GT0Dw(Id, Jd) + &
							& ( cmplx(2.0_rp, 0.0_rp, rp) - GTTUp(Id, Id) - GTTDw(Id, Id) ) * &
							& ( cmplx(2.0_rp, 0.0_rp, rp) - G00Up(Jd, Jd) - G00Dw(Jd, Jd) ) 
				DenDen_Tau(imj, No_1, No_2, NT) = DenDen_Tau(imj, No_1, No_2, NT) + Ztp2 * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)		
			enddo
		enddo
!**************************************************************************************************
!__________ 3. Calculate the GF used to determine the global single-particle gap __________________
!**************************************************************************************************
		do Id = 1, NumNS
			Ztp2 = GT0Up(Id, Id) + GT0Dw(Id, Id)
			GreenG_Tau(NT) = GreenG_Tau(NT) + Ztp2 * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)
		enddo
!______________________________________________________________________________________________________________	  
!___________________________________________ Deallocate the arrays ____________________________________________
!______________________________________________________________________________________________________________
		deallocate(G00Dw)
		deallocate(GTTDw)
		deallocate(G0TDw)
		deallocate(GT0Dw)
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$