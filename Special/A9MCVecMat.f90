!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: Several subroutines used to perform Initialization of key matrices used in PQMC before the whole PQMC simulations.
! COMMENT: PQMC Initialization process.
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-27
! PURPOSE: Different subroutines are introduced as following:
!             
!   InitMCVecMat --> Subroutine to perform Initialization of key matrices used in PQMC in PQMC Simulations;
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine InitMCVecMat()
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  InitMCVecMat()
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to perform the initialization for Core PQMC vectors and matrices.
! KEYWORDS: Initialization of key vectors and matrices in PQMC.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-10
! DESCRIPTION:
!
!     Initialization of constants in PQMC, including:
!             (0) Initialization of some global numbers in PQMC simulations;
!             (1) Allocate global matrices in PQMC simulations and initializations.
!
!     Input:  (none)   Output: (none)
!
! MODULES USED: RealPrecsn, CoreParamt
! SUBROUTINES CALLED:  (none)
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use Observable
		implicit none
!______________________________________________________________________________________________________________	  
!_______________________________ Main calculations of PQMC constants __________________________________________
!______________________________________________________________________________________________________________
!**************************************************************************************************  
!___________________ 0. Initialization of some global numbers in PQMC _____________________________
!**************************************************************************************************
		PhaseDif = 0.0_rp                        ! The difference between two phases (real number)
		PhaseMax = 0.0_rp                        ! The maximum phase difference (real number)
		Phase    = cmplx(0.0_rp, 0.0_rp, rp)     ! The phase of determinant of spin-\sigma sector
!**************************************************************************************************	  
!___________________ 1. Allocate global matrices in PQMC simulations and initializations __________
!**************************************************************************************************
!_______________________________________________________________________________________	  
!________________________ (0) Globally used matrices ___________________________________
!_______________________________________________________________________________________
		allocate(   ULeft(NumEl, NumNS       ))    ! The left side matrix: P^{\dagger} * B_i
		allocate(   URght(NumNS, NumEl       ))    ! The right side matrix: B_i * P 
		allocate(   ULtRt(NumEl, NumEl       ))    ! The left-right matrix: P^{\dagger} * {B_i} * P
		allocate(ULtRtInv(NumEl, NumEl       ))    ! The inverse of left-right matrix: (P^{\dagger} * {B_i} * P)^{-1}
		allocate(   USTMt(NumNS, NumEl, DmUST))    ! Record the ULeft or URght matrix where the UDV decomposition happens 
		ULeft    = cmplx(0.0_rp, 0.0_rp, rp)        ! Initialization
		URght    = cmplx(0.0_rp, 0.0_rp, rp)        ! Initialization
		ULtRt    = cmplx(0.0_rp, 0.0_rp, rp)        ! Initialization
		ULtRtInv = cmplx(0.0_rp, 0.0_rp, rp)        ! Initialization
		USTMt    = cmplx(0.0_rp, 0.0_rp, rp)        ! Initialization
!_______________________________________________________________________________________	  
!________________________ (1) For Dynamic observables __________________________________
!_______________________________________________________________________________________
		if(IfTAU == 1) then
			allocate(ULeft_1(NumEl, NumNS))         ! The temporary Left side matrix 
			allocate(URght_1(NumNS, NumEl))         ! The temporary right side matrix 
			allocate(ULtRtInv_1(NumEl, NumEl))      ! The temporary inverse matrix of Left-right matrix
			ULeft_1    = cmplx(0.0_rp, 0.0_rp, rp)
			URght_1    = cmplx(0.0_rp, 0.0_rp, rp)
			ULtRtInv_1 = cmplx(0.0_rp, 0.0_rp, rp)
		end if      

	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$