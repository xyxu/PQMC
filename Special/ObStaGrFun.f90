!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: A subroutine used to calculate the Equal-time Green Function by the simple formula G = I - URght * ULtRtInv * ULeft and
!              G_{ij}=<c_i*c_j^+> and calculation GC_{ij}=<c_i^+*c_j>. Thes two calculations are for the spin-up part of the system.
!              Then we calculate the spin-down part of Green Function by the canonical transformation (or a special "particle-hole"
!                transformation).
!              Generally, in the above HS transformation, we cal observe that the spin-up Green Function matrix is actually exactly
!                the same as spin-down Green Function matrix. But the one calculated from PQMC is actually an approximate one with 
!                some error. So by this canonical transformation, we can calculate the number of electrons and occupation very 
!                accurately, which is the reason that we never adopt the Gup=Gdw expression directly. 
! COMMENT: Calculate the equal-time Green Function.
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-27
! PURPOSE: Different subroutines are introduced as following:
!             
!   ObStaGrFun --> Subroutine to calculate equal-time Green Function matrices for both spin-up and spin-down cases;
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	
	
	
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine ObStaGrFun(GrFUp, GrFUpC, GrFDw, GrFDwC) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  ObStaGrFun(GrFUp, GrFUpC, GrFDw, GrFDwC)
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to calculate the static Green Function for every measurement of static quantities.
!                 GrFUp  = <c_i c_j^+>             --> GrFUp = I - URght * ULtRtInv * ULeft --> Spin-up
!                 GrFUpC = <c_i^+ c_j> = I - GrF^T --> GrFUpC_{ij} = -GrFUp_{ji}            --> Spin-up
!                 GrFDw  = <c_i c_j^+>             --> GrFDw = I - URght * ULtRtInv * ULeft --> Spin-down
!                 GrFDwC = <c_i^+ c_j> = I - GrF^T --> GrFDwC_{ij} = -GrFDw_{ji}            --> Spin-down
! KEYWORDS: Calculate the equal-time Green Function.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-27
! DESCRIPTION:
!
!     Calculate the static Green Function from URght, ULeft and ULtRtInv.
!             GrF  = I - URght * ULtRtInv * ULeft
!
!     Input: GrFUp  --> The spin-up Green Function calculated from PQMC;   --> <c_i c_j^+>
!            GrFUpC --> The spin-up Green Function calculated from PQMC;   --> <c_i^+ c_j>
!            GrFDw  --> The spin-down Green Function calculated from PQMC; --> <c_i c_j^+>
!            GrFDwC --> The spin-down Green Function calculated from PQMC; --> <c_i^+ c_j>
!
!Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt
! SUBROUTINES CALLED:  (none)
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		complex(rp) GrFUp (NumNS, NumNS) 
		complex(rp) GrFUpC(NumNS, NumNS)
		complex(rp) GrFDw (NumNS, NumNS) 
		complex(rp) GrFDwC(NumNS, NumNS)
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer I1          ! Loop integer
		integer I2          ! Loop integer
		integer I3          ! Loop integer
		integer I4          ! Loop integer
		
		integer Id          ! Loop integer
		integer Jd          ! Loop integer
		
		real(rp) Rtp1       ! Temporary real number
		real(rp) Rtp2       ! Temporary real number
!______________________________________________________________________________________________________________	  
!__________________________________ Allocate Array and Initializations ________________________________________
!______________________________________________________________________________________________________________
		GrFUp  = cmplx(0.0_rp, 0.0_rp, rp)
		GrFUpC = cmplx(0.0_rp, 0.0_rp, rp)
		GrFDw  = cmplx(0.0_rp, 0.0_rp, rp)
		GrFDwC = cmplx(0.0_rp, 0.0_rp, rp)
!______________________________________________________________________________________________________________	  
!________________________________________ Main calculations ___________________________________________________
!______________________________________________________________________________________________________________
!__________________________________________________________________________________________	  
!_____________________ 0. Calculate GrFUp = I - URght * ULtRtInv * ULeft __________________
!__________________________________________________________________________________________
		do I1 = 1, NumNS
			do I2 = 1, NumEl
				do I3 = 1, NumEl
					GrFUp(I1, I2) = GrFUp(I1, I2) + URght(I1, I3) * ULtRtInv(I3, I2)
				enddo
			enddo
		enddo
		
		do I1 = 1, NumNS
			do I2 = 1, NumNS
				do I3 = 1, NumEl
					GrFUpC(I1, I2) = GrFUpC(I1, I2) + GrFUp(I1, I3) * ULeft(I3, I2)
				enddo
			enddo
		enddo

		GrFUp = - GrFUpC
		do I1 = 1, NumNS
			GrFUp(I1, I1) = cmplx(1.0_rp, 0.0_rp, rp) + GrFUp(I1, I1)
		enddo
!__________________________________________________________________________________________	  
!_____________________ 1. Calculate GrFUpC = I - GrFUp^T __________________________________
!__________________________________________________________________________________________
		GrFUpC = cmplx(0.0_rp, 0.0_rp, rp)
		do I1 = 1, NumNS
			do I2 = 1, NumNS
				GrFUpC(I1, I2) = - GrFUp(I2, I1)
			enddo
		enddo
		do I1 = 1, NumNS
			GrFUpC(I1, I1) = cmplx(1.0_rp, 0.0_rp, rp) + GrFUpC(I1, I1)
		enddo
!__________________________________________________________________________________________	  
!_____________________ 2. Calculate GrFDw and GrFDwC by Canonical Transformation __________
!__________________________________________________________________________________________
		do Id = 1, NumNS
			Rtp1 = + 1.0_rp
			if(StList(Id, 2) == 1) Rtp1 = - 1.0_rp
			do Jd = 1, NumNS
				Rtp2 = + 1.0_rp
				if(StList(Jd, 2) == 1) Rtp2 = - 1.0_rp
				GrFDw (Id, Jd) = cmplx(Rtp1*Rtp2, 0.0_rp, rp) * conjg(GrFUpC(Id, Jd))
				GrFDwC(Id, Jd) = cmplx(Rtp1*Rtp2, 0.0_rp, rp) * conjg(GrFUp (Id, Jd))
			enddo
		enddo
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$