!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: A few subroutines used for performing the equal-time measurements in PQMC simulations, including:
!                  (0) Calculate the equal-time Green Function matrix by G = I - URght * ULtRtInv * ULeft for spin-up sector;
!                  (1) Calculate teh equal-time Green Function matrix for spin-down sector by canonical transformation;
!                  (2) Calculate the expectation values of all the energy terms in model Hamiltonian;
!                  (3) Calculate the Correlation functions in real space for some chosen lattice sites pairs;
!                  (4) Calculate the correlation functions for all the lattice sites used for constructing structure factors;
! COMMENT: PQMC Observation process.
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-11
! PURPOSE: Different subroutines are introduced as following:
!             
!   ObsStatics --> Subroutine to call other subroutines to perform the equal-time measurements in PQMC simulations;
!   GrFStatics --> Subroutine to calculate the equal-time Green Function matrix by G = I - URght * ULtRtInv * ULeft;
!   ObStaEnrgy --> Subroutine to calculate the expectation values of all the energy terms in model Hamiltonian;
!   CorreFunct --> Subroutine to calculate the Correlation functions in real space for some chosen lattice sites pairs;
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine ObsStatics() 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  ObsStatics() 
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to measure the static physical quantities during the sweep of PQMC in the calculations.
! KEYWORDS: Calculate the static physical quantities.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-01
! DESCRIPTION:
!
!     Measure the static physical quantities during the sweep of PQMC in the calculations from the URght, ULeft, ULtRt, ULtRtInv.
!
!     Input: (none);  Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt
! SUBROUTINES CALLED: GrFStatics, ExpectEngy, CorreFunct
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use TimeRecord
		use QMCTimeRec
		implicit none
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer(8) time1    ! Starting time point in this observable measurements
		integer(8) time2    ! Ending   time point in this observable measurements

		complex(rp), allocatable :: GrFUp (:, :)   ! The Spin-Up static Green Function <c_i c_j^+>
		complex(rp), allocatable :: GrFUpC(:, :)   ! The Spin-Up static Green Function <c_i^+ c_j>
		complex(rp), allocatable :: GrFDw (:, :)   ! The Spin-Down Static Green Function <c_i c_j^+>
		complex(rp), allocatable :: GrFDwC(:, :)   ! The Spin-Down Static Green Function <c_i^+ c_j>
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& 	  
!______________ Counting calling times and time consumed in observable measurement ______________
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		TimsStaMe = TimsStaMe + 1
		call system_clock(time1)
!______________________________________________________________________________________________________________	  
!__________________________________ Allocate Array and Initializations ________________________________________
!______________________________________________________________________________________________________________
		allocate(GrFUp (NumNS, NumNS))
		allocate(GrFUpC(NumNS, NumNS))
		allocate(GrFDw (NumNS, NumNS))
		allocate(GrFDwC(NumNS, NumNS))

		GrFUp  = cmplx(0.0_rp, 0.0_rp, rp)
		GrFUpC = cmplx(0.0_rp, 0.0_rp, rp)
		GrFDw  = cmplx(0.0_rp, 0.0_rp, rp)
		GrFDwC = cmplx(0.0_rp, 0.0_rp, rp)
!______________________________________________________________________________________________________________	  
!_____________________________ Main calculations for all the static quantities ________________________________
!______________________________________________________________________________________________________________
!**************************************************************************************************
!__________ 0. Calculate Spin-Up and spin-down equal-time Green Function matrix ___________________
!**************************************************************************************************
		call ObStaGrFun(GrFUp, GrFUpC, GrFDw, GrFDwC) 
!**************************************************************************************************
!__________ 1. Calculate the Expectation energies for all Hamiltonian terms _______________________
!**************************************************************************************************
		call ObStaEnrgy(GrFUp, GrFUpC, GrFDw, GrFDwC)
!**************************************************************************************************
!__________ 2. Calculate real space Correlation functions on several lattice sites pairs __________
!**************************************************************************************************
		call ObStaCorFR(GrFUp, GrFUpC, GrFDw, GrFDwC)
!**************************************************************************************************
!__________ 3. Calculate SpinZZ, SpinPM, GreenT, DenDen matrices, preparing for Fourier ___________
!_________________ Transforming into the reciprocal space as spin structure factors _______________
!________________________ Momentum distribution and density structure factor ______________________
!**************************************************************************************************
		call ObStaCorFK(GrFUp, GrFUpC, GrFDw, GrFDwC)
!______________________________________________________________________________________________________________	  
!___________________________________________ Deallocate the arrays ____________________________________________
!______________________________________________________________________________________________________________
		deallocate(GrFUp )
		deallocate(GrFUpC)
		deallocate(GrFDw )
		deallocate(GrFDwC)
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& 	  
!______________ Counting calling times and time consumed in observable measurement ______________
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		call system_clock(time2)
		TimeStaMe = TimeStaMe + TimeIntrvl(time1, time2)
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$