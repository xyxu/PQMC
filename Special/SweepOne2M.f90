!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine SweepOne2M(NSW) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  SweepOne2M(NSW) 
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to perform the sweep from \tau=1 to \tau=M in every Full sweep as NB=1,NSwep contains the sweep from
!             \tau=M-->\tau=1 and inverse \tau=1-->\tau=M processes.
! KEYWORDS: One-->M sweep.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-01
! DESCRIPTION:
!
!     Perform the sweep from \tau=1 to \tau=M in every Full sweep
!
!     Input: (none);  Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt, Observable
! SUBROUTINES CALLED: RghtMultK, LeftMultKInv, ObsStatics, UDVReOrtho, MatrMult_C, InvMatrxC2, SettgPhase, 
!                         UpdateFldU, UpdateFdV0, UpdateFdJ0
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use Observable
		implicit none
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer I1            ! Loop integer for dimension of matrix
		integer I2            ! Loop integer for dimension of matrix		
		integer NT            ! Loop integer for the time slices
		integer NT_ST         ! Integer recording the steps with UDV decompodition in [1, LTrot]  
		
		integer NSW           ! Loop integer for NSwep calculations in every Bin
		
		real(rp) Rtp1         ! Temporary real number
		real(rp) Rtp2         ! Temporary real number
		
		complex(rp) DetZ(2)   ! Determinant of ULtRtInv
		complex(rp) PhaseT    ! Temporary phase factor
!______________________________________________________________________________________________________________	  
!_________________________ Main calculations of Sweep from t=1 slice to t=M slice _____________________________
!______________________________________________________________________________________________________________
		do NT = 1, LTrot
!**************************************************************************************************		  
!________________________ 0. Exponential Hopping matrix to both sides _____________________________
!**************************************************************************************************	
			call RghtMultK   (NumNS, NumEl, URght)
			call LeftMultKInv(NumEl, NumNS, ULeft)
!**************************************************************************************************	  
!________________________ 2. Update the Ising fields in U interaction _____________________________
!**************************************************************************************************
			if( abs(HubbU) > 1.0E-10_rp ) then
				call RghtMultUVJ   (NumNS, NumEl, URght, NT)
				call LeftMultUVJInv(NumEl, NumNS, ULeft, NT)
				
				call UpdateFldU(NT)
			end if
!**************************************************************************************************  
!________________________ 3. Check if this NT is the time slice with UDV __________________________
!**************************************************************************************************
			if( mod(NT, NWrap) == 0 ) then
!____________________________________________________________________________________	  
!__________ (0) Calculate URght and ULtRt, ULtRtInv matrices ________________________
!____________________________________________________________________________________ 
				NT_ST = NT / NWrap
				do I1 = 1, NumEl
					do I2 = 1, NumNS
						ULeft(I1, I2) = USTMt(I2, I1, NT_ST)
					enddo
				enddo
				call UDVReOrtho(NumNS, NumEl, URght, IfUDV)
				call MatrMult_C(NumEl, NumNS, NumEl, ULeft, URght, ULtRt)
				call ULRInverse(NumEl, ULtRt, ULtRtInv, DetZ)
!____________________________________________________________________________________	  
!__________ (1) Calculate the phase factor and Compare with previous one ____________
!____________________________________________________________________________________
				call SettgPhase(DetZ, PhaseT)                
				PhaseDif = sqrt( real((Phase-PhaseT)*conjg(Phase-PhaseT)) )
				if(PhaseDif > PhaseMax) PhaseMax = PhaseDif
				Phase = PhaseT
!____________________________________________________________________________________	  
!__________ (2) Store the URght matrix in USTMt matrix ________________________________
!____________________________________________________________________________________
				do I1 = 1, NumNS
					do I2 = 1, NumEl
						USTMt(I1, I2, NT_ST) = URght(I1, I2)
					enddo
				enddo
!____________________________________________________________________________________	  
!__________ (3) End for if( mod(NT, NWrap) == 0 ) ___________________________________
!____________________________________________________________________________________	
			end if
!**************************************************************************************************  
!________________________ 4. Perform the measurements after the updating __________________________
!**************************************************************************************************
			if( (NT >= MeaSt) .and. (NT <= MeaEn) ) then
				call ObsStatics()
				NObs = NObs + 1		
			end if
!**************************************************************************************************	  
!________________________ 5. Measure the time-displayed quantities ________________________________
!**************************************************************************************************
			if( (IfTAU == 1) .and. (NT == NTAUIN) ) then
				ULeft_1    = ULeft
				URght_1    = URght
				ULtRtInv_1 = ULtRtInv
				call ObsDynamic(NSW)
				NObsT = NObsT + 1
				
				Rtp1 = 1.0_rp
				Rtp2 = dble(Phase)
				if(Rtp2 < 0.0_rp) Rtp1 = - 1.0_rp
				SigNt = SigNt + Rtp1
			end if
!**************************************************************************************************	  
!________________________ 6. End for the iteration do NT = 1, LTrot _______________________________
!**************************************************************************************************					
		enddo
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$