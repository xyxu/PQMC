!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: Several subroutines used to perform initally listing og all constants in the program  before the whole PQMC simulations.
! COMMENT: PQMC Initialization process.
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-27
! PURPOSE: Different subroutines are introduced as following:
!             
!   InitListCons --> Subroutine to list all constants defined in PQMC Simulations;
!   ListItem     --> Subroutine to output a single constant by some format.
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine InitListCons()
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  InitListCons()
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to perform the output of all constants defined in modules during the PQMC simulations.
! KEYWORDS: List all PQMC calculation constants.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-10
! DESCRIPTION:
!
!     Initialization of constants in PQMC, including:
!                  (0) Constants defined in TimeRecord module;
!                  (1) Constants defined in StdInOutSt module;
!                  (2) Constants defined in RealPrecsn module;
!                  (3) Constants defined in CoreParamt module;
!                  (4) Constants defined in Observable module.
!
!     Input:  (none)   Output: (none)
!
! MODULES USED: TimeRecord, StdInOutSt, RealPrecsn, CoreParamt, Observable.
! SUBROUTINES CALLED: ListItem
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use TimeRecord
		use StdInOutSt
		use RealPrecsn
		use CoreParamt
		use Observable
		implicit none
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		character(512) ChaRep
!______________________________________________________________________________________________________________	  
!_______________________________ Main calculations of Listing constants _______________________________________
!______________________________________________________________________________________________________________
!__________________________________________________________________________________________________	  
!________________________________ Open the output file ____________________________________________
!__________________________________________________________________________________________________
			open(112, file = Trim(FLog), access = "append")
!__________________________________________________________________________________________________	  
!___________________ 1. List Constants in TimeRecord module _______________________________________
!__________________________________________________________________________________________________
			write(112, "(<DatTimLen>x, 4x, A)") "TimeRecord"
			write(ChaRep, *) TimRat;								call ListItem(112, ChaRep, "TimRat")
			write(ChaRep, *) TimMax;								call ListItem(112, ChaRep, "TimMax")
			write(ChaRep, *) TimThh;								call ListItem(112, ChaRep, "TimThh")
			write(ChaRep, *) DatTimLen;                     call ListItem(112, ChaRep, "DatTimLen")
!__________________________________________________________________________________________________	  
!___________________ 2. List Constants in StdInOutSt module _______________________________________
!__________________________________________________________________________________________________      
			write(112, "(<DatTimLen>x, 4x, A)") "StdInOutSt"
			write(ChaRep, *) FLog;									call ListItem(112, ChaRep, "FLog")
			write(ChaRep, *) FWrn;									call ListItem(112, ChaRep, "FWrn")
			write(ChaRep, *) FErr;									call ListItem(112, ChaRep, "FErr")
			write(ChaRep, *) FMnt;									call ListItem(112, ChaRep, "FMnt")
!__________________________________________________________________________________________________	  
!___________________ 3. List Constants in RealPrecsn module _______________________________________
!__________________________________________________________________________________________________ 		
			write(112, "(<DatTimLen>x, 4x, A)") "RealPrecsn"
			write(ChaRep, *) rp;									   call ListItem(112, ChaRep, "rp")
			write(ChaRep, *) rp_ow;									call ListItem(112, ChaRep, "rp_ow")
			write(ChaRep, *) rp_od;									call ListItem(112, ChaRep, "rp_od")
			write(ChaRep, "(es25.16)") rp_one;              call ListItem(112, ChaRep, "rp_one")
			write(ChaRep, "(es25.16)") rp_pi;               call ListItem(112, ChaRep, "rp_pi")
			write(ChaRep, "(es25.16)") rp_gold;             call ListItem(112, ChaRep, "rp_gold")
			write(ChaRep, *) rp_huge;								call ListItem(112, ChaRep, "rp_huge")
			write(ChaRep, *) rp_tiny;								call ListItem(112, ChaRep, "rp_tiny")
			write(ChaRep, "(es25.16)") rp_prec;             call ListItem(112, ChaRep, "rp_prec")
			write(ChaRep, "(es25.16)") rp_Eps;             call ListItem(112, ChaRep, "rp_Eps")
!__________________________________________________________________________________________________	  
!___________________ 4. List Constants in CoreParamt module _______________________________________
!__________________________________________________________________________________________________
			write(112, "(<DatTimLen>x, 4x, A)") "CoreParamt"
			write(ChaRep, *) NumL1;									   call ListItem(112, ChaRep, "NumL1")
			write(ChaRep, *) NumL2;									   call ListItem(112, ChaRep, "NumL2")
			write(ChaRep, *) NumNC;									   call ListItem(112, ChaRep, "NumNC")
			write(ChaRep, *) NOrbt;									   call ListItem(112, ChaRep, "NOrbt")
			write(ChaRep, *) NumNS;									   call ListItem(112, ChaRep, "NumNS")
			write(ChaRep, *) IfPr1;									   call ListItem(112, ChaRep, "IfPr1")
			write(ChaRep, *) IfPr2;									   call ListItem(112, ChaRep, "IfPr2")
			write(ChaRep, "(2es25.16)") cmplx(A1Vec(1), A1Vec(2), rp);		call ListItem(112, ChaRep, "A1Vec")
			write(ChaRep, "(2es25.16)") cmplx(A2Vec(1), A2Vec(2), rp);		call ListItem(112, ChaRep, "A2Vec")
			write(ChaRep, "(2es25.16)") cmplx(L1Vec(1), L1Vec(2), rp);		call ListItem(112, ChaRep, "L1Vec")
			write(ChaRep, "(2es25.16)") cmplx(L2Vec(1), L2Vec(2), rp);		call ListItem(112, ChaRep, "L2Vec")
			write(ChaRep, "(2es25.16)") cmplx(B1Vec(1), B1Vec(2), rp);		call ListItem(112, ChaRep, "B1Vec")
			write(ChaRep, "(2es25.16)") cmplx(B2Vec(1), B2Vec(2), rp);		call ListItem(112, ChaRep, "B2Vec")
			write(ChaRep, "(2es25.16)") cmplx(X1Vec(1), X1Vec(2), rp);		call ListItem(112, ChaRep, "X1Vec")
			write(ChaRep, "(2es25.16)") cmplx(X2Vec(1), X2Vec(2), rp);		call ListItem(112, ChaRep, "X2Vec")
		
			write(ChaRep, "(es25.16)") Hopt1;									   call ListItem(112, ChaRep, "Hopt1")
			write(ChaRep, "(es25.16)") HopSC;									   call ListItem(112, ChaRep, "HopSC")
			write(ChaRep, "(es25.16)") Hopt3;									   call ListItem(112, ChaRep, "Hopt3")
			write(ChaRep, "(es25.16)") HubbU;									   call ListItem(112, ChaRep, "HubbU")
			write(ChaRep, "(es25.16)") TwstX;									   call ListItem(112, ChaRep, "TwstX")
			write(ChaRep, *) Magnt;									   call ListItem(112, ChaRep, "Magnt")
		
			write(ChaRep, "(es25.16)") BetaT;									   call ListItem(112, ChaRep, "BetaT")
			write(ChaRep, "(es25.16)") Dltau;									   call ListItem(112, ChaRep, "Dltau")
			write(ChaRep, *) LTrot;									   call ListItem(112, ChaRep, "LTrot")
			write(ChaRep, *) NWrap;									   call ListItem(112, ChaRep, "NWrap")
			write(ChaRep, *) DmUST;									   call ListItem(112, ChaRep, "DmUST")
			write(ChaRep, *) NmBIN;									   call ListItem(112, ChaRep, "NmBIN")
			write(ChaRep, *) NSwep;									   call ListItem(112, ChaRep, "NSwep")
		
			write(ChaRep, "(es25.16)") GamL(-2);                         call ListItem(112, ChaRep, "GamL(-2)")
			write(ChaRep, "(es25.16)") GamL(-1);                         call ListItem(112, ChaRep, "GamL(-1)")
			write(ChaRep, "(es25.16)") GamL(+1);                         call ListItem(112, ChaRep, "GamL(+1)")
			write(ChaRep, "(es25.16)") GamL(+2);                         call ListItem(112, ChaRep, "GamL(+2)")
			write(ChaRep, "(es25.16)") EtaL(-2);                         call ListItem(112, ChaRep, "EtaL(-2)")
			write(ChaRep, "(es25.16)") EtaL(-1);                         call ListItem(112, ChaRep, "EtaL(-1)")
			write(ChaRep, "(es25.16)") EtaL(+1);                         call ListItem(112, ChaRep, "EtaL(+1)")
			write(ChaRep, "(es25.16)") EtaL(+2);                         call ListItem(112, ChaRep, "EtaL(+2)")
		
			write(ChaRep, "(2es25.16)") LmdaU;                            call ListItem(112, ChaRep, "LmdaU")
		
			write(ChaRep, *) IfUDV;                            call ListItem(112, ChaRep, "IfUDV")
		
			write(ChaRep, *) IfTAU;                            call ListItem(112, ChaRep, "IfTAU")
			write(ChaRep, *) NmTDM;                            call ListItem(112, ChaRep, "NmTDM")
		
			write(ChaRep, *) ISeed;                            call ListItem(112, ChaRep, "ISeed")
!__________________________________________________________________________________________________	  
!___________________ 5. List Constants in Observable module _______________________________________
!__________________________________________________________________________________________________
			write(112, "(<DatTimLen>x, 4x, A)") "Observable"	
			write(ChaRep, *) MeaTt;                            call ListItem(112, ChaRep, "MeaTt")
			write(ChaRep, *) MeaSt;                            call ListItem(112, ChaRep, "MeaSt")
			write(ChaRep, *) MeaEn;                            call ListItem(112, ChaRep, "MeaEn")
			write(ChaRep, *) NmTDM;                            call ListItem(112, ChaRep, "NmTDM")
			write(ChaRep, *) NTAUIN;                           call ListItem(112, ChaRep, "NTAUIN")
!__________________________________________________________________________________________________	  
!________________________________ close the output file ___________________________________________
!__________________________________________________________________________________________________
			close(112)

	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine ListItem(FileUnit, ChaRep, Name)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  ListItem(FileUnit, ChaRep, Name)
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to perform the output process for every single constant in subroutine PQMCListCn.
! KEYWORDS: Output single constant.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-10
! DESCRIPTION:
!
!     Output single constant
!
!     Input: FileUnit --> The integer file handle for the output 
!            ChaRep   --> The character containing the values of the constant;
!            Name     --> Character for the name of the single constant.
!
!     Outpt: (none)
!
! MODULES USED: TimeRecord
! SUBROUTINES CALLED:  (none)
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use TimeRecord
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		integer FileUnit      ! The integer file handle for the output 
		character(*) ChaRep   ! The character containing the values of the constant
		character(*) Name     ! Character for the name of the single constant
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer SkipLen       ! Length of skipping part
!______________________________________________________________________________________________________________	  
!_______________________________ Main calculations of Ouputing constant _______________________________________
!______________________________________________________________________________________________________________
		SkipLen = len("                           ")
		SkipLen = SkipLen - len_Trim(Name)
		write(FileUnit, 10) Trim(Name), Trim(ChaRep)
10    format(<DatTimLen>x, 4x, <SkipLen>x, A, " = ", A)
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$