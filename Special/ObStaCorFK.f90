!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: A subroutine used to perform the equal-time meansurement of the several different correlation functions in real space for 
!              the whole finite lattice, and this calculation is used as preparations for the calculation of spin structure factors(in 
!              z direction and xy plane, momentum distribution and Density-Density correlation in reciprocal space. This is the reason
!              that we call it ObStaCorFK for the subroutine:
!                 (1) SpinZ-SpinZ correlation function;     --> SpinZZ(NumNC, NOrbt, NOrbt)
!                 (2) SpnPM-SpnPM correlation function;     --> SpinPM(NumNC, NOrbt, NOrbt)
!                 (3) Total Green Funtion with up and down; --> GreenT(NumNC, NOrbt, NOrbt)
!                 (4) Density-Density Correlation function; --> DenDen(NumNC, NOrbt, NOrbt)
!             From the SpinZZ and SpinPM matrices, we can calculate the corresponding spin structure factors in z direction and xy plane
!                 to determine long-range magnetic order in z direction and xy-plane. From GreenT, we can construct the momentum
!                 distribution, which is similar to the spectral function. From DenDen, we can construct the density structure factor.
!          This calculations of correlation functions are for all the lattice sites on the finite lattice system, with periodic boundary
!                 condition. So before the calculations, we need to get the indexes integers for the distance between lattice sites pairs, 
!                 which is the IminusJ(NumNC, NumNC) matrix calculated in InitLattStru subroutine.
! COMMENT: Calculate the real space correlation functions for the whole finite lattice.
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-27
! PURPOSE: Different subroutines are introduced as following:
!             
!   ObStaCorFK --> Subroutine to calculate the Real space correlation functions for whole finite lattice;
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine ObStaCorFK(GUp, GUpC, GDw, GDwC)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  ObStaCorFK(GUp, GUpC, GDw, GDwC)
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to calculate the three correlation functions for the 3*2 finite lattice.
!                 SpZSpZ = <Sz_i Sz_j>
!                 SpmSpm = <S^+_i S^-_j>+<S^-_i S^+_j>
!                 GreenT = GrFUpC(I1, I2) + GrFDwC(I1, I2)
!                 DenDen = <n_i * n_j>
! KEYWORDS: Calculate the three correlation functions.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-30
! DESCRIPTION:
!
!     Due to the periodic boundary condition we applied, there are five different distances for the lattice sites in 3*2 finite system.
!
!       The unit cells with maximum distance, and AA, BB, AB, BA correlation functions. 
!
!     Input: GUp  --> The spin-up Green Function calculated from PQMC;   --> <c_i c_j^+>
!            GUpC --> The spin-up Green Function calculated from PQMC;   --> <c_i^+ c_j>
!            GDw  --> The spin-down Green Function calculated from PQMC; --> <c_i c_j^+>
!            GDwC --> The spin-down Green Function calculated from PQMC; --> <c_i^+ c_j>
!
!     Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt, Observable
! SUBROUTINES CALLED:  (none)
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use Observable
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		complex(rp) GUp (NumNS, NumNS) 
		complex(rp) GUpC(NumNS, NumNS)
		complex(rp) GDw (NumNS, NumNS) 
		complex(rp) GDwC(NumNS, NumNS)
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer I1            ! Index integer for lattice site
		integer I2            ! Index integer for lattice site
		integer Nu_1          ! The unit cell index integer for the I1-th lattice site
		integer No_1          ! The orbital index integer for the I1-th lattice site
		integer Nu_2          ! The unit cell index integer for the I2-the lattice site 
		integer No_2          ! The orbital index integer for the I2-th lattice site
		
		integer imj           ! Integer index for distance of (Nu_1, Nu_2) unit cells
		
		real(rp) Itp1         ! Temporary real number
		real(rp) Itp2         ! Temporary real number
		
		real(rp) Rtp1         ! Temporary real number
		real(rp) Rtp2         ! Temporary real number

		complex(rp) Ztp1      ! Temporary complex number
		complex(rp) Ztp2      ! Temporary complex number
		complex(rp) Ztp3      ! Temporary complex number
		complex(rp) Ztp4      ! Temporary complex number
		
		complex(rp), allocatable :: TmpDen(:)     ! Temporary array for density on every lattice site
		complex(rp), allocatable :: TmpSpZ(:)     ! Temporary array for Spin Z on every lattice site
!______________________________________________________________________________________________________________	  
!__________________________________ Allocate Array and Initializations ________________________________________
!______________________________________________________________________________________________________________
		allocate(TmpDen(NumNS))
		allocate(TmpSpZ(NumNS))
		TmpDen = cmplx(0.0_rp, 0.0_rp)
		TmpSpZ = cmplx(0.0_rp, 0.0_rp)
!______________________________________________________________________________________________________________	  
!________________________________________ Main calculations ___________________________________________________
!______________________________________________________________________________________________________________
!______________________________________________________________________________________________	  
!_______________ 1. Count the sign during the calculation _____________________________________
!______________________________________________________________________________________________
		Rtp1 = 1.0_rp
		Rtp2 = dble(Phase)
		if(Rtp2 .lt. 0.0_rp) Rtp1 = -1.0_rp
		Ztp1 = Phase / cmplx(dble(Phase), 0.0_rp, rp)
!______________________________________________________________________________________________	  
!_______________ 2. Calculate the TmpDen, TmpSpZ for following use ____________________________
!______________________________________________________________________________________________
		do I1 = 1, NumNS
			TmpDen(I1) = GUpC(I1, I1) + GDwC(I1, I1)
			TmpSpZ(I1) = GUpC(I1, I1) - GDwC(I1, I1)
		enddo
!______________________________________________________________________________________________	  
!_______________ 3. Calculate DenDen, SPINZZ, SPINPM, GreenT matrices _________________________
!______________________________________________________________________________________________
		do I1 = 1, NumNS
			Nu_1 = StList(I1, 1)
			No_1 = StList(I1, 2)
			
			DenOcc(Nu_1, No_1, 1) = DenOcc(Nu_1, No_1, 1) + TmpDen(I1)        * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)
			SpinZc(Nu_1, No_1, 1) = SpinZc(Nu_1, No_1, 1) + TmpSpZ(I1)/2.0_rp * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)
			
			do I2 = 1, NumNS
				Nu_2 = StList(I2, 1)
			   No_2 = StList(I2, 2)
				imj = IminusJ(Nu_1, Nu_2)
				
				Ztp2 = GUpC(I1, I2) * GUp(I1, I2) + GDwC(I1, I2) * GDw (I1, I2)
				Ztp3 = GUpC(I1, I2) * GDw(I1, I2) + GUp (I1, I2) * GDwC(I1, I2)
				
				GreenT(imj, No_1, No_2) = GreenT(imj, No_1, No_2) + (GUpC(I1, I2) + GDwC(I1, I2))         * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)
				SPINZZ(imj, No_1, No_2) = SPINZZ(imj, No_1, No_2) + (TmpSpZ(I1)*TmpSpZ(I2) + Ztp2)/4.0_rp * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)
				SPINPM(imj, No_1, No_2) = SPINPM(imj, No_1, No_2) +                           Ztp3/2.0_rp * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)
				DenDen(imj, No_1, No_2) = DenDen(imj, No_1, No_2) + (TmpDen(I1)*TmpDen(I2) + Ztp2)        * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)
			enddo
		enddo
!______________________________________________________________________________________________________________	  
!___________________________________________ Deallocate the arrays ____________________________________________
!______________________________________________________________________________________________________________
		deallocate(TmpDen)
		deallocate(TmpSpZ)
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$