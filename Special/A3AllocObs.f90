!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: Several subroutines used to perform allocation of the observables before the whole PQMC simulations.
! COMMENT: PQMC Initialization process.
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-27
! PURPOSE: Different subroutines are introduced as following:
!             
!   InitAllocObs --> Subroutine to allocate the observables vectors and matrices used in PQMC;
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine InitAllocObs()
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  InitAllocObs()
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to perform the initialization for observables calculated in PQMC simulations, mainly initiate some
!                calculation parameters and allocate some vectors and matrices.
! KEYWORDS: Initialization of Observable module.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-10
! DESCRIPTION:
!
!     Initialization of Observable module, including:
!             (0) Initialization of some measurement parameters;
!             (1) Allocate the observable matrices and initializations.
!
!     Input:  (none)   Output: (none)
!
! MODULES USED: RealPrecsn, CoreParamt
! SUBROUTINES CALLED:  (none)
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use Observable
		use StdInOutSt
		implicit none
		
		integer I0
!______________________________________________________________________________________________________________	  
!____________________________ Main calculations of Observables Initialization _________________________________
!______________________________________________________________________________________________________________
!****************************************************************************************************  
!_____________________ 0. Integer parameters used in equal-time measurements ________________________
!****************************************************************************************************
		MeaSt = LTrot/2 - MeaTt/2      ! The starting time slice used for measurement
		MeaEn = LTrot/2 + MeaTt/2      ! The ending time slice used for measurement
!****************************************************************************************************  
!_____________________ 1. Integer parameters used in time-displayed measurements ____________________
!****************************************************************************************************
		SignT  = 0                     !
		NTAUIN = (LTrot - NmTDM) / 2   ! 
!****************************************************************************************************  
!_____________________ 2. Observables in real space calculated from PQMC for every BIN ______________
!****************************************************************************************************
		NumCal    = 12                         ! Number of lattice site pairs we actually calculate for correlation functions
		Obser     = cmplx(0.0_rp, 0.0_rp, rp)  ! Some observables including energies and double occupancy for every NmBIN simulations
		CorrFTmp  = cmplx(0.0_rp, 0.0_rp, rp)  ! Tmporary matrix used to deducting the background term in Correlation functions
		CorrFunc  = cmplx(0.0_rp, 0.0_rp, rp)  ! The three Correlation functions on 3*2 lattices plus the total occupation
!****************************************************************************************************	  
!_____________________ 3. Equal-time Correlation functions for all lattice sites ____________________
!__________________________ in real space for every BIN, used for constructing ______________________
!______________________________ Structure factors and momentum distribution _________________________
!****************************************************************************************************
		allocate(DenOcc(NumNC, NOrbt, NOrbt))    ! The density occupation on all lattice sites
		allocate(SpinZc(NumNC, NOrbt, NOrbt))    ! The spin-z component on all lattice sites
		allocate(SPINZZ(NumNC, NOrbt, NOrbt))    ! The <Sz*Sz> correlation function
		allocate(SPINPM(NumNC, NOrbt, NOrbt))    ! The <S+S->+<S-S+> correlation function 
		allocate(GREENT(NumNC, NOrbt, NOrbt))    ! Total Gup+Gdw for calculation of momentum distribution
		allocate(DenDen(NumNC, NOrbt, NOrbt))    ! <n_i*n_j> density-density correlation function
		DenOcc = cmplx(0.0_rp, 0.0_rp, rp)        ! Initialization
		SpinZc = cmplx(0.0_rp, 0.0_rp, rp)        ! Initialization
		SPINZZ = cmplx(0.0_rp, 0.0_rp, rp)        ! Initialization
		SPINPM = cmplx(0.0_rp, 0.0_rp, rp)        ! Initialization
		GREENT = cmplx(0.0_rp, 0.0_rp, rp)        ! Initialization
		DenDen = cmplx(0.0_rp, 0.0_rp, rp)        ! Initialization
!**************************************************************************************************** 
!_____________________ 4. Time-dispalyed Correlation functions for all lattice sites ________________
!____________________________ in real space for every BIN, used for constructing ____________________
!____________________________________ reciprocal space Green Function _______________________________
!****************************************************************************************************
		if(IfTAU == 1) then
			allocate(SPINZZ_Tau(NumNC, NOrbt, NOrbt, 0:NmTDM))   ! The <Sz*Sz> correlation function
			allocate(SPINPM_Tau(NumNC, NOrbt, NOrbt, 0:NmTDM))   ! The <S+S->+<S-S+> correlation function 
			allocate(GREENT_Tau(NumNC, NOrbt, NOrbt, 0:NmTDM))   ! Total Gup+Gdw for calculation of momentum distribution
			allocate(DenDen_Tau(NumNC, NOrbt, NOrbt, 0:NmTDM))   ! <n_i*n_j> density-density correlation function
			allocate(DenOcc_Tau(NumNC, NOrbt, 0:NmTDm))          ! The <A_i> * <A_j> term in the density-density correlation function
			allocate(GreenG_Tau(0:NmTDM))                        ! The time-displaced quantity used to determine the global single-particle gap
			SPINZZ_Tau = cmplx(0.0_rp, 0.0_rp, rp)                ! Initialization
			SPINPM_Tau = cmplx(0.0_rp, 0.0_rp, rp)                ! Initialization
			GREENT_Tau = cmplx(0.0_rp, 0.0_rp, rp)                ! Initialization
			DenDen_Tau = cmplx(0.0_rp, 0.0_rp, rp)                ! Initialization
			DenOcc_Tau = cmplx(0.0_rp, 0.0_rp, rp)                ! Initialization
			GreenG_Tau = cmplx(0.0_rp, 0.0_rp, rp)                ! Initialization
		end if
!****************************************************************************************************  
!_____________________ 5. Final expectation values for energies for all BIN's _______________________
!****************************************************************************************************
		allocate(REHopt1(NmBIN))     ! Expectation energy for the full NN hopping term
		allocate(REHoptd(NmBIN))     ! Expectation energy for the dimerized NN hopping term
		allocate(REHopSC(NmBIN))     ! Expectation energy for the Intrinsic SOC term
		allocate(REHopt3(NmBIN))     ! Expectation energy for the 3NN term
		allocate(REUIntr(NmBIN))     ! Expectation energy for the On-site repulsion HubbU term             --> With chemical potential
		allocate(RETotal(NmBIN))     ! Expectation energy for the whole model Hamiltonian                  --> With chemical potential
		allocate(RDouOcc(NmBIN))     ! Expectation value for the double occupancy --> The average value
		REHopt1 = 0.0_rp              ! Intialization
		REHoptd = 0.0_rp              ! Intialization
		REHopSC = 0.0_rp              ! Intialization
		REHopt3 = 0.0_rp              ! Intialization
		REUIntr = 0.0_rp              ! Intialization
		RETotal = 0.0_rp              ! Intialization
		RDouOcc = 0.0_rp              ! Intialization
!****************************************************************************************************	  
!_____________________ 6. Final expectation values for CorrFunc for all BIN's in real space _________
!****************************************************************************************************      
		allocate(RSpZSpZ(NmBIN, NumCor))  ! SpinZ-SpinZ correlation functions in real space with NumCal lattice site pairs
		allocate(RSpmSpm(NmBIN, NumCor))  ! SpnPM-SpnPM correlation functions in real space with NumCal lattice site pairs
		allocate(RSpnSpn(NmBIN, NumCor))  ! <S_i*S_j> correlation functions in real space with NumCal lattice site pairs
		allocate(RDenDen(NmBIN, NumCor))  ! Density-Density correlation functions in real space with NumCal lattice site pairs
		allocate(RGreenF(NmBIN, NumCor))  ! Occupation in real space with NumCal lattice site pairs
		RSpZSpZ = 0.0_rp                      ! Intialization
		RSpmSpm = 0.0_rp                      ! Intialization
		RSpnSpn = 0.0_rp                      ! Intialization
		RDenDen = 0.0_rp                      ! Intialization
		RGreenF = 0.0_rp                      ! Intialization
!****************************************************************************************************	  
!_____________________ 7. Final expectation values for equal-time reciporcal space __________________
!___________________________ Structures and momentum distribution for all BIN's _____________________
!****************************************************************************************************
		if(IfPr1 == 1 .and. IfPr2 ==1) then
			I0 = NumNC + NumL1 + NumL2 + 1
			allocate(KSpinZZ(NmBIN, I0, NOrbt*NOrbt))         ! Z-direction Spin structrue factor in reciprocal space 
			allocate(KSpinPM(NmBIN, I0, NOrbt*NOrbt))         ! xy-plane Spin structrue factor in reciprocal space 
			allocate(KSpnSpn(NmBIN, I0, NOrbt*NOrbt))         ! Total Spin structrue factor in reciprocal space 
			allocate(KGreenT(NmBIN, I0, NOrbt*NOrbt))         ! Momentum distribution for the system
			allocate(KDenDen(NmBIN, I0, NOrbt*NOrbt))         ! Density-Density correlation function in reciprocal space
			KSpinZZ = cmplx(0.0_rp, 0.0_rp, rp)                ! Intialization
			KSpinPM = cmplx(0.0_rp, 0.0_rp, rp)                ! Intialization
			KSpnSpn = cmplx(0.0_rp, 0.0_rp, rp)                ! Intialization
			KGreenT = cmplx(0.0_rp, 0.0_rp, rp)                ! Intialization
			KDenDen = cmplx(0.0_rp, 0.0_rp, rp)                ! Intialization
		end if
!****************************************************************************************************	  
!_____________________ 8. Final expectation values for equal-time reciporcal space __________________
!_________________________ Structures factors for AFM, Density, SOC for all BIN's ___________________
!****************************************************************************************************
		if(IfPr1 == 1 .and. IfPr2 ==1) then
			allocate(KOrderP(NmBIN, 8))
		end if
!****************************************************************************************************		  
!_____________________ 9. Final expectation values for time-displayed reciporcal space ______________
!_____________________________ Green Function and structure factors for all BIN's ___________________
!****************************************************************************************************
		if(IfTAU == 1) then
			allocate(KGreenT_Tau(0:NmTDM, 6, NmBIN))   ! Gup+Gdw for calculation of momentum distribution time-displaced
			allocate(KSpinZZ_Tau(0:NmTDM, 6, NmBIN))   ! The <Sz*Sz> correlation function time-displaced
			allocate(KSpinPM_Tau(0:NmTDM, 6, NmBIN))   ! The <S+S->+<S-S+> correlation function time-displaced
			allocate(KSpnSpn_Tau(0:NmTDM, 6, NmBIN))   ! The <S+S->+<S-S+> correlation function time-displaced
			allocate(KDenDen_Tau(0:NmTDM, 6, NmBIN))   ! <n_i*n_j> density-density correlation function time-displaced
			allocate(RGreenG_Tau(0:NmTDM, 2, NmBIN))   ! Determine the global single-particle gap
			KGreenT_Tau = 0.0_rp                        ! Initialization
			KSpinZZ_Tau = 0.0_rp                        ! Initialization 
			KSpinPM_Tau = 0.0_rp                        ! Initialization
			KSpnSpn_Tau = 0.0_rp                        ! Initialization 
			KDenDen_Tau = 0.0_rp                        ! Initialization
			RGreenG_Tau = 0.0_rp                        ! Initialization
			
			if(IfOtptGF == 1) then
				allocate(AllKGreenP_Tau(NumNC, 0:NmTDM, 2*NOrbt*NOrbt))  ! K points, imaginary-time, real and imaginary parts
				AllKGreenP_Tau = 0.0_rp
			end if
		end if
!**************************************************************************************************************	  
!______________________________ For calculating the Spin Chern Number _________________________________________
!**************************************************************************************************************
		if(IfTAU == 1) then
			allocate(GIw0k(NOrbt, NOrbt, 0:NumL1-1, 0:NumL2-1))   ! The zero-frequency Green Function at all k points
			GIw0k = cmplx(0.0_rp, 0.0_rp, rp)
		end if
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$