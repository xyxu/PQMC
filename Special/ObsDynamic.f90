!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: A few subroutines used for performing the time-displaced measurements in PQMC simulations, including:
!                  (0) Calculate the time-displayed Green Function at different imaginary time \tau;
!                  (1) Calculate other time-displaced correlation functions from the time-displaced Green Function;
! COMMENT: PQMC Observation process.
!
!          Here, we define the time-displaced Green Functions with the custom as following:
!                                                                                 / +<c_i(\tau_1)c_j^+(\tau2)>, If \tau_1 >= \tau_2
!                 G(\tau_1, \tau_2)_{ij} = <T_{\tau}[c_i(\tau_1)c_j^+(\tau2)]> = /
!                                                                                \
!                                                                                 \ -<c_j^+(\tau2)c_i(\tau_1)>, If \tau_1 < \tau_2
!                 In all the calculations, we choose \tau > 0
!
!                 GT0_{i,j} = <T[c_i(\tau)c_j^+(0)]>    = + <c_i(\tau)c_j^+>
!                 G0T_{i,j} = <T[c_i(0)c_j^+(\tau)]>    = - <c_j^+(\tau)c_i>   ****** Here we define this function with minus sign difference
!                 G00_{i,j} = <T[c_i(0)c_j^+(0)]>       = + <c_ic_j^+>
!                 GTT_{i,j} = <T[c_i(\tau)c_j^+(\tau)]> = + <c_i(\tau)c_j^+(\tau)>
!
!                 For G0T_{i,j} --> We define: G0T_{i,j} = + <c_j^+(\tau)c_i>
!
!          Initialization of these time-displaced Green Functions:
!                 GT0_{i,j} = + <c_ic_j^+>   
!                 G0T_{i,j} = + <c_j^+c_i> = \delta_{ij} - <c_ic_j^+> = \delta_{ij} - G00_{i,j}
!                 G00_{i,j} = + <c_ic_j^+>
!                 GTT_{i,j} = + <c_ic_j^+>
!
! AUTHOR:  Yuan-Yao He
! DATE:    2015-01-30
! PURPOSE: Different subroutines are introduced as following:
!             
!   ObsStatics --> Subroutine to call other subroutines to perform the equal-time measurements in PQMC simulations;
!   GrFStatics --> Subroutine to calculate the equal-time Green Function matrix by G = I - URght * ULtRtInv * ULeft;
!   ObStaEnrgy --> Subroutine to calculate the expectation values of all the energy terms in model Hamiltonian;
!   CorreFunct --> Subroutine to calculate the Correlation functions in real space for some chosen lattice sites pairs;
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine ObsDynamic(NSW)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  ObsDynamic(NSW) 
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to measure the dynamic physical quantities during the sweep of PQMC in the calculations.
! KEYWORDS: Calculate the Dynamic physical quantities.
! AUTHOR:   Yuan-Yao He
! TIME:     2015-01-30
! DESCRIPTION:
!
!     Measure the Dynamic physical quantities during the sweep of PQMC in the calculations..
!
!          Defnitions of these time-displaced Green Functions:
!                 GT0_{i,j} = + <c_i(\tau)c_j^+>
!                 G0T_{i,j} = + <c_j^+(\tau)c_i>
!                 G00_{i,j} = + <c_ic_j^+>
!                 GTT_{i,j} = + <c_i(\tau)c_j^+(\tau)>
!
!     Input: (none);  Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt
! SUBROUTINES CALLED: GrFStatics, ExpectEngy, CorreFunct
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use Observable
		use StdInOutSt
		use TimeRecord
		use QMCTimeRec
		implicit none
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer(8) time1    ! Starting time point in this observable measurements
		integer(8) time2    ! Ending   time point in this observable measurements
		
		integer NT          ! Absolute Imaginary-time in the whole PQMC simulation 
		integer NTAU        ! Imaginary-time for time-displaced measurements
		
		integer NT_ST       ! Integer for UDV decomposition
		integer NComp       ! Number of Comparations
		
		integer I0          ! Loop integer over lattice sites
		integer I1          ! Loop integer over number of electrons
	
		integer NSW         ! Loop integer for NSwep calculations in every Bin
		
		real(rp) XMAX       ! Temporary real number for XMAX_DYN
		real(rp) XMEAN      ! Temporary real number for XMEAN_DYN
		real(rp) XMEAN_DYN  ! Mean Difference between two equal-time Green Function
		real(rp) XMAX_DYN   ! Maximum Difference between two equal-time Green Function
		
		complex(rp) DetZ(2) ! Determinant of ULtRt matrix
		
		complex(rp), allocatable :: GrFUp (:, :)   ! The Spin-Up static Green Function <c_i c_j^+>
		complex(rp), allocatable :: GrFUpC(:, :)   ! The Spin-Up static Green Function <c_i^+ c_j>
		complex(rp), allocatable :: G00Up (:, :)   ! The Spin-Up equal-time Green Function <c_i(\tau=0) c_j^+>
		complex(rp), allocatable :: G0TUp (:, :)   ! The Spin-Up time-displaced Green Function <c_i(0) c_j^+(tau)>
		complex(rp), allocatable :: GT0Up (:, :)   ! The Spin-Up time-displaced Green Function <c_i(tau) c_j^+(0)>
		complex(rp), allocatable :: GTTUp (:, :)   ! The Spin-Up equal-time Green Function <c_i(tau) c_j^+(tau)>
		complex(rp), allocatable :: GTemp (:, :)   ! Used as temporary matrix for Green Function matrix 
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& 	  
!______________ Counting calling times and time consumed in observable measurement ______________
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		TimsDynMe = TimsDynMe + 1
		call system_clock(time1)
!______________________________________________________________________________________________________________	  
!__________________________________ Allocate Array and Initializations ________________________________________
!______________________________________________________________________________________________________________
		allocate(GrFUp (NumNS, NumNS))     ! <c_i c_j^+>
		allocate(GrFUpC(NumNS, NumNS))     ! <c_i^+ c_j>
		allocate(G00Up (NumNS, NumNS))     ! <c_i(\tau=0) c_j^+>
		allocate(G0TUp (NumNS, NumNS))     ! <c_i(0) c_j^+(tau)>
		allocate(GT0Up (NumNS, NumNS))     ! <c_i(tau) c_j^+(0)>
		allocate(GTTUp (NumNS, NumNS))     ! <c_i(tau) c_j^+(tau)>
		allocate(GTemp (NumNS, NumNS))     ! Temporary
		
		GrFUp  = cmplx(0.0_rp, 0.0_rp, rp)  ! Initialization
		GrFUpC = cmplx(0.0_rp, 0.0_rp, rp)  ! Initialization 
		G00Up  = cmplx(0.0_rp, 0.0_rp, rp)  ! Initialization
		G0TUp  = cmplx(0.0_rp, 0.0_rp, rp)  ! Initialization
		GT0Up  = cmplx(0.0_rp, 0.0_rp, rp)  ! Initialization
		GTTUp  = cmplx(0.0_rp, 0.0_rp, rp)  ! Initialization 
		GTemp  = cmplx(0.0_rp, 0.0_rp, rp)  ! Initialization 
!______________________________________________________________________________________________________________	  
!_____________________________ Main calculations for all the static quantities ________________________________
!______________________________________________________________________________________________________________
		XMAX_DYN  = 0.0_rp
		XMEAN_DYN = 0.0_rp
		
		NComp = 0
!**************************************************************************************************
!____________ 0. Iteration for time-displaced quantities with different tau _______________________
!**************************************************************************************************		
		do NT = NTAUIN, NTAUIN + NmTDM - 1
!__________________________________________________________________________________________	  
!____________ (0) Get the tau value for present time-displaced quantities _________________
!__________________________________________________________________________________________
			NTAU = NT - NTAUIN
!__________________________________________________________________________________________	  
!____________ (1) For mod(NT, NWRAP)==0 or NTAU ==0 case, Perform some calculations _______
!__________________________________________________________________________________________
			if( (mod(NT, NWRAP) == 0) .or. (NTAU == 0) ) then
!_________________________________________________________________________________	  
!__________ For NT > NTAUIN case, we get the ULeft_1 matrix from USTMt ___________
!_________________________________________________________________________________
				NT_ST = NT / NWRAP
				if(NTAU > 0) then
					do I1 = 1, NumEl
						do I0 = 1, NumNS
							ULeft_1(I1, I0) = USTMt(I0, I1, NT_ST)
						enddo
					enddo
				end if
!_________________________________________________________________________________	  
!__________ Calculate ULtRt and its inverse for the next Green Function __________
!_________________________________________________________________________________				
				call MatrMult_C(NumEl, NumNS, NumEl, ULeft_1, URght_1, ULtRt)
				call ULRInverse(NumEl, ULtRt, ULtRtInv_1, DetZ)
!_________________________________________________________________________________	  
!__________ Calculate ULtRt and its inverse for the next Green Function __________
!_________________________________________________________________________________
				call CalGrFuncT(NumEl, NumNS, ULeft_1, URght_1, ULtRtInv_1, GrFUp, GrFUpC)
!_________________________________________________________________________________	  
!_______ Initialize time-displaced Green Functions for NTAU == 0 case ____________
!_________________________________________________________________________________
				if(NTAU == 0) then
					G00Up = GrFUp
					GTTUp = GrFUp
					GT0Up = GrFUp
					G0TUp = GrFUpC
					call ObDynMeasu(NTAU, GT0Up, G0TUp, GTTUp, G00Up)
				else
					XMAX  = 0.0_rp
					XMEAN = 0.0_rp
					call MatrCmpr_C(NumNS, NumNS, GTTUp, GrFUp, XMAX, XMEAN)
					if(XMAX > XMAX_DYN) XMAX_DYN = XMAX
					XMEAN_DYN = XMEAN_DYN + XMEAN
					NComp = NComp + 1
					
					GTTUP = GrFUp
					
					call MatrMult_C(NumNS, NumNS, NumNS, GTTUp, GT0Up, GTemp)
					GT0Up = GTemp
					
					call MatrMult_C(NumNS, NumNS, NumNS, G0TUp, GrFUpC, GTemp)
					G0TUp = GTemp
				end if
			end if
!__________________________________________________________________________________________	  
!____________ (2) Propogate the quantities from NTAU to NTAU+1  ___________________________
!__________________________________________________________________________________________
			call PropagateT   (GT0Up, NT+1)
			call PropagateTInv(G0TUp, NT+1)
			call PropagateTInv(GTTUp, NT+1)
			call PropagateT   (GTTUp, NT+1)
!__________________________________________________________________________________________	  
!____________ (3) Measurement for \tau = NTAU+1  __________________________________________
!__________________________________________________________________________________________
			call ObDynMeasu(NTAU+1, GT0Up, G0TUp, GTTUp, G00Up)
!__________________________________________________________________________________________	  
!____________ (4) Wrap UR matrix: Calculate B_{NT} * UR, and ______________________________
!_____________ B_{NT} = U * V * Vz * C3 * C2 * C1 * K3 * K2 * K1 * CL * KL * B ____________
!__________________________________________________________________________________________         
			call ObDynURWrp(NT+1)
!__________________________________________________________________________________________	  
!____________ (5) For the mod(NT1, NWrap)==0 case, apply UDV decomposition ________________
!__________________________________________________________________________________________
			if( (mod(NT+1, NWrap) == 0) .and. ((NT+1) .ne. (NTAUIN+NmTDM)) ) then
				call UDVReOrtho(NumNS, NumEl, URght_1, IfUDV)
			end if
!__________________________________________________________________________________________	  
!____________ (6) End for the time-displaced measurements _________________________________
!__________________________________________________________________________________________
		enddo
!**************************************************************************************************
!____________ 1. Output the XMAX_DYN and XMEAN_DYN ________________________________________________
!**************************************************************************************************		
      write(MonitorOt, "('ObsDynamic: NSW, XMAX_DYN, XMEAN_DYN = ', I7, 2es25.16)") NSW, XMAX_DYN, XMEAN_DYN/NComp
!______________________________________________________________________________________________________________	  
!___________________________________________ Deallocate the arrays ____________________________________________
!______________________________________________________________________________________________________________
		deallocate(GrFUp )
		deallocate(GrFUpC)
		deallocate(G00Up )
		deallocate(GTTUp )
		deallocate(G0TUp )
		deallocate(GT0Up )
		deallocate(GTemp )
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& 	  
!______________ Counting calling times and time consumed in observable measurement ______________
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		call system_clock(time2)
		TimeDynMe = TimeDynMe + TimeIntrvl(time1, time2)
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$




!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine CalGrFuncT(N1, N2, UL, UR, ULRInv, GrFUp, GrFUpC)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  CalGrFuncT(N1, N2, UL, UR, ULRInv, GrFUp, GrFUpC)
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to calculate the equal-time Green Function, used in time-displaced measurements.
!                 GrFUp  = <c_i c_j^+>             --> GrFUp = I - UR * ULRInv * UL --> Spin-up
!                 GrFUpC = <c_i^+ c_j> = I - GrF^T --> GrFUpC_{ij} = -GrFUp_{ji}    --> Spin-up
! KEYWORDS: Calculate the equal-time Green Function..
! AUTHOR:   Yuan-Yao He
! TIME:     2015-01-29
! DESCRIPTION:
!
!     Calculate the static Green Function from UR, UL and ULRInv.
!             GrF  = I - UR * ULRInv * UL
!
!     Input: N1, N2 --> Dimension of UL, UR, ULRInv matrices;
!            UL     --> Input matrix;
!            UR     --> Input matrix;
!            ULRInv --> Input matrix;
!
!     Outpt: GrFUp  --> The spin-up Green Function calculated from PQMC; --> <c_i c_j^+>
!            GrFUpC --> The spin-up Green Function calculated from PQMC; --> <c_i^+ c_j>  
!
! MODULES USED: RealPrecsn, CoreParamt, StdInOutSt
! SUBROUTINES CALLED: GenerateIfIt
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		integer N1                       ! Dimension of the UL, UR, ULRInv matrices
		integer N2                       ! Dimension of the UL, UR, ULRInv matrices
		complex(rp)     UL(N1, N2)       ! Input matrix
		complex(rp)     UR(N2, N1)       ! Input matrix
		complex(rp) ULRInv(N1, N1)       ! Input matrix
		complex(rp) GrFUp (N2, N2)       ! Output matrix <c_i c_j^+>
		complex(rp) GrFUpC(N2, N2)       ! Output matrix <c_i^+ c_j>
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer I1          ! Loop integer
		integer I2          ! Loop integer
		integer I3          ! Loop integer
!______________________________________________________________________________________________________________	  
!__________________________________ Allocate Array and Initializations ________________________________________
!______________________________________________________________________________________________________________
		GrFUp  = cmplx(0.0_rp, 0.0_rp, rp)
		GrFUpC = cmplx(0.0_rp, 0.0_rp, rp)
!______________________________________________________________________________________________________________	  
!_______________________________ Main calculations of PQMC constants __________________________________________
!______________________________________________________________________________________________________________
!__________________________________________________________________________________________	  
!_____________________ 0. Calculate GrFUp = I - UR * ULRInv * UL __________________________
!__________________________________________________________________________________________
		do I1 = 1, N2
			do I2 = 1, N1
				do I3 = 1, N1
					GrFUp(I1, I2) = GrFUp(I1, I2) + UR(I1, I3) * ULRInv(I3, I2)
				enddo
			enddo
		enddo
		
		do I1 = 1, N2
			do I2 = 1, N2
				do I3 = 1, N1
					GrFUpC(I1, I2) = GrFUpC(I1, I2) + GrFUp(I1, I3) * UL(I3, I2)
				enddo
			enddo
		enddo

		GrFUp = - GrFUpC
		do I1 = 1, N2
			GrFUp(I1, I1) = cmplx(1.0_rp, 0.0_rp, rp) + GrFUp(I1, I1)
		enddo
!__________________________________________________________________________________________	  
!_____________________ 1. Calculate GrFUpC = I - GrFUp^T __________________________________
!__________________________________________________________________________________________
		GrFUpC = cmplx(0.0_rp, 0.0_rp, rp)
		do I1 = 1, N2
			do I2 = 1, N2
				GrFUpC(I1, I2) = - GrFUp(I1, I2)
			enddo
		enddo
		do I1 = 1, N2
			GrFUpC(I1, I1) = cmplx(1.0_rp, 0.0_rp, rp) + GrFUpC(I1, I1)
		enddo
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine ObDynURWrp(NT)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  ObDynURWrp(NT)
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to perform the UR wrap process during the time-displaced measurements, which is actually used to
!              perform the following matrix multiplications:
!                B_{NT} * UR, and B_{NT} = U * V * Vz * C3 * C2 * C1 * K3 * K2 * K1 * CL * KL *  * B
! KEYWORDS: UR wrap process during time-displaced measurements.
! AUTHOR:   Yuan-Yao He
! TIME:     2015-01-30
! DESCRIPTION:
!
!     Calculate B_{NT} * UR, and B_{NT} = U * V * Vz * C3 * C2 * C1 * K3 * K2 * K1 * CL * KL *  * B
!
!     Input: URght_1 --> Input UR matrix;
!            NT1     --> Input imaginary time;
!
!     Outpt: URght_1 --> Final UR matrix.
!
! MODULES USED: RealPrecsn, CoreParamt, StdInOutSt
! SUBROUTINES CALLED: GenerateIfIt
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		integer NT                  ! Imaginary time
!______________________________________________________________________________________________________________	  
!________________________________________ UR wrap process _____________________________________________________
!______________________________________________________________________________________________________________
!______________________________________________________________________________________________________
!____ Calculate B_{NT} * UR, and B_{NT} = U * V * Vz * C3 * C2 * C1 * K3 * K2 * K1 * CL * KL *  * B ___
!______________________________________________________________________________________________________
!_____________________________________________________________________________________________
!_____________ 0. Calculate UR = B * UR, and B = exp(-\Delta\tau * K) ________________________
!_____________________________________________________________________________________________
		call RghtMultK(NumNS, NumEl, URght_1)
!_____________________________________________________________________________________________
!_____________ 5. Calculate UR = U * UR ______________________________________________________
!_____________________________________________________________________________________________
		if( abs(HubbU) > 1.0E-10_rp ) then
			call RghtMultUVJ(NumNS, NumEl, URght_1, NT)
		endif
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$