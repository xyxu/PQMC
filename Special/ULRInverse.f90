!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: A subroutine used to calculating the inversed matrix of ULtRt matrix in PQMC simulations, by calling the subroutine 
!              InvMatrxC2(LDA, A, AInv, Det) in the General folder.
!          And we set the counting time quantity used to count the time consumed by this calculation of inverse matrix.
! COMMENT: Calculating the matrix inverse of ULtRt. 
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-27
! PURPOSE: Different subroutines are introduced as following:
!             
!   SweepMeasu     --> Subroutine to call subroutines to perform main PQMC simulations;
!   InitMCObserv   --> Subroutine to perform initialization of observables for every BIN;
!   QMCTimeRecInit --> Subroutine to perform initialization of time counting for every BIN;
!   SweepM2One     --> Subroutine to perform sweep from M time slice to 1 time slice in a sweep;
!   SweepIntVl     --> Subroutine to perform interval processing of the phase setting in a sweep;
!   SweepOne2M     --> Subroutine to perform sweep from 1 time slice to M time slice in a sweep;
!   PostStatic     --> Subroutine to perform Postprocess data, mainly average and outout for a BIN;
!   SaveCalTim     --> Subroutine to perform output the consumed time for all parts for a BIN.
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine ULRInverse(LDA, A, AInv, Det) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  ULRInverse(LDA, A, AInv, Det)
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to calculate the inverse matrix of ULtRt matrix and count the time consumed in this calculation of
!              matrix inverse. 
! KEYWORDS: Matrix inverse of ULtRt matrix.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-27
! DESCRIPTION:
!
!     Performs the calculations of inverse matrix of ULtRt matrix, to get the ULtRtInv matrix.
!
!     Input: LDA  --> Dimension of A matrix;
!            A    --> Input complex square matrix;
!
!     Outpt: AInv --> Result output complex diagonal matrix.
!            Det  --> The determinant of A matrix.
!
! MODULES USED: RealPrecsn, CoreParamt
! SUBROUTINES CALLED: UDVReOrtho, MatrMult_C, InvMatrxC2, SettgPhase
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use TimeRecord
		use QMCTimeRec
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		integer LDA                   ! Dimension of input A matrix 
		complex(rp) Det(2)            ! The determinant of A matrix
		complex(rp) A(LDA, LDA)       ! Input A matrix
		complex(rp) AInv(LDA, LDA)    ! Output Inv(A) matrix
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer(8) time1    ! Starting time point in this matrix inverse process
		integer(8) time2    ! Ending   time point in this matrix inverse process
!______________________________________________________________________________________________________________	  
!_______________________________ Main calculations of matrix inverse __________________________________________
!______________________________________________________________________________________________________________		
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& 	  
!________________ Counting calling times and time consumed in matrix inverse ____________________
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&       
		TimsMtInv = TimsMtInv + 1
		call system_clock(time1)
!________________________________________________________________________________________________	  
!_____________________________ Calculation of the matrix inverse ________________________________
!________________________________________________________________________________________________ 		
		call InvMatrxC2(LDA, A, AInv, Det)
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& 	  
!________________ Counting calling times and time consumed in matrix inverse ____________________
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&       
		call system_clock(time2)
		TimeMtInv = TimeMtInv + TimeIntrvl(time1, time2)

	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$