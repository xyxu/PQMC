!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: A few subroutines used for calculating the time used for the subroutine calculations.
! COMMENT: Time Recording for (C)DMFT calculations.
! AUTHOR:  Yuan-Yao He, Rong-Qiang He
! DATE:    2014-11-18
! PURPOSE: Different subroutines are introduced as following:
!             
!   SubroutineSkp --> Subroutine to record the information if some subroutine is skipped in the calculations;
!   SubroutineBgn --> Subroutine to record the information if some subroutine begins in the calculations;
!   SubroutineEnd --> Subroutine to record the information if some subroutine ends in the calculations.
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________                            
	subroutine SubroutineSkp(CaptionName, EmptyLen)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  SubroutineSkp(CaptionName, EmptyLen)
! TYPE:     subroutine
! PURPOSE:  This Subroutine store the information if some subroutine in the whole calculations is skipped.
! KEYWORDS: Calculation information recording.
! AUTHOR:   Yuan-Yao He, Rong-Qiang He
! TIME:     2014-11-18
! DESCRIPTION:
!
!     Input: CaptionName --> The name of the subroutine that has been skipped;
!            EmptyLen    --> The length of the spacing for the output of the information.
!
!     Output: (none).
!
! MODULES USED: StdInOutSt, TimeRecord
! SUBROUTINES CALLED:  PresentMoment
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this Module ________________________________________
!______________________________________________________________________________________________________________
		use StdInOutSt
		use TimeRecord
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		character(*) CaptionName  ! The name of the subroutine that has been skipped
		integer EmptyLen          ! The length of the spacing for the output of the information
!______________________________________________________________________________________________________________	  
!______________________________________ Storing the information _______________________________________________
!______________________________________________________________________________________________________________
			call PresentMoment()
			open(50, file = trim(FLog), access = "append")
			write(50, "(A, <EmptyLen>x, '  > ', A, ' Skipped')") DatTim, CaptionName
			close(50)

	end subroutine	 
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________                            
	subroutine SubroutineBgn(CaptionName, EmptyLen, time1)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  SubroutineBgn(CaptionName, EmptyLen, time1)
! TYPE:     subroutine
! PURPOSE:  This Subroutine store the information if some subroutine begins in the calculations.
! KEYWORDS: Calculation information recording.
! AUTHOR:   Yuan-Yao He, Rong-Qiang He
! TIME:     2014-11-18
! DESCRIPTION:
!
!     Input: CaptionName --> The name of the subroutine that has bagan to calculate;
!            EmptyLen    --> The length of the spacing for the output of the information;
!            time1       --> The exact time that the subroutine begins to calculate.
!
!     Output: (none).
!
! MODULES USED: StdInOutSt, TimeRecord
! SUBROUTINES CALLED:  PresentMoment
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this Module ________________________________________
!______________________________________________________________________________________________________________
		use StdInOutSt
		use TimeRecord
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		character(*) CaptionName  ! The name of the subroutine that has been skipped
		integer EmptyLen          ! The length of the spacing for the output of the information
		integer(8) time1          ! The exact time that the subroutine begins to calculate.
!______________________________________________________________________________________________________________	  
!______________________________________ Storing the information _______________________________________________
!______________________________________________________________________________________________________________
			call PresentMoment()
			open(50, file = Trim(FLog), access = "append")
			write(50, "(A, <EmptyLen>x, '  + ', A, ' Bgn')") DatTim, CaptionName
			close(50)
		
		call system_clock(time1)

	end subroutine	 
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________                            
	subroutine SubroutineEnd(CaptionName, EmptyLen, time1, time2)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  SubroutineEnd(CaptionName, EmptyLen, time1, time2)
! TYPE:     subroutine
! PURPOSE:  This Subroutine store the information if some subroutine ends in the calculations and it calculate the time that it costs 
!               in the calculation as TimeIntrvl(time1, time2)  -->  TimeIntrvl(time1, time2) / 3600 .
! KEYWORDS: Calculation information recording.
! AUTHOR:   Yuan-Yao He, Rong-Qiang He
! TIME:     2014-11-18
! DESCRIPTION:
!
!     Input: CaptionName --> The name of the subroutine that has bagan to calculate;
!            EmptyLen    --> The length of the spacing for the output of the information;
!            time1       --> The exact time that the subroutine begins to calculate.
!            time2       --> The exact time that the subroutine ends to calculate.
!
!     Output: (none).
!
! MODULES USED: StdInOutSt, TimeRecord
! SUBROUTINES CALLED:  PresentMoment
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this Module ________________________________________
!______________________________________________________________________________________________________________
		use StdInOutSt
		use TimeRecord
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		character(*) CaptionName  ! The name of the subroutine that has been skipped
		integer EmptyLen          ! The length of the spacing for the output of the information
		integer(8) time1          ! The exact time that the subroutine begins to calculate.
		integer(8) time2          ! The exact time that the subroutine ends to calculate.
!______________________________________________________________________________________________________________	  
!______________________________________ Storing the information _______________________________________________
!______________________________________________________________________________________________________________
		call system_clock(time2)
		
			call PresentMoment()
			open(50, file = trim(FLog), access = "append")
			write(50, "(A, <EmptyLen>x, '  - ', A, ' End', ', Elapsed Time = ', F14.5, 's = ', F12.5, 'm = ', F10.5, 'h')") &
				& DatTim, CaptionName, TimeIntrvl(time1, time2), TimeIntrvl(time1, time2)/60, TimeIntrvl(time1, time2)/3600
			close(50)

	end subroutine	 
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$            