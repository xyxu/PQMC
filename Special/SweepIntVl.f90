!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine SweepIntVl() 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  SweepIntVl() 
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to perform the calculations URght, ULeft and ULtRt, ULtRtInv between the sweep (M-->1) and (1-->M) in
!               a full sweep. Full sweep as NB=1,NSwep contains the sweep from \tau=M-->\tau=1 and inverse \tau=1-->\tau=M processes.
! KEYWORDS: Interval sweep calculation.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-01
! DESCRIPTION:
!
!     Performs the calculations URght, ULeft and ULtRt, ULtRtInv between the sweep (M-->1) and (1-->M) in a full sweep.
!
!     Input: (none);  Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt
! SUBROUTINES CALLED: UDVReOrtho, MatrMult_C, InvMatrxC2, SettgPhase
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		implicit none
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________		
		complex(rp) DetZ(2)   ! Determinant of ULtRtInv
		complex(rp) PhaseT    ! Temporary phase factor
!______________________________________________________________________________________________________________	  
!____________________ Main calculations of Reset URght and calculate ULeft, ULtRt, ULtRtInv ___________________
!______________________________________________________________________________________________________________
!__________________________________________________________________________________________________
!___________________________ 1. URght, ULeft, ULtRt, ULtRtInv _____________________________________
!__________________________________________________________________________________________________
		URght(1:NumNS, 1:NumEl) = ProjM(1:NumNS, 1:NumEl)           ! Set the UR matrix
		call UDVReOrtho(NumEl, NumNS, ULeft, IfUDV)                 ! perform the UDV decomposition for ULeft matrix
		call MatrMult_C(NumEl, NumNS, NumEl, ULeft, URght, ULtRt)   ! Calculate ULtRt      = ULeft * URght
		call ULRInverse(NumEl, ULtRt, ULtRtInv, DetZ)               ! Calculate ULtRt^{-1} = ULtRt^{-1}
!__________________________________________________________________________________________________
!__________________________ 2. Reset the phase factor in the calculations _________________________
!__________________________________________________________________________________________________
		call SettgPhase(DetZ, PhaseT)                
		PhaseDif = sqrt( real((Phase-PhaseT)*conjg(Phase-PhaseT)) )
		if(PhaseDif > PhaseMax) PhaseMax = PhaseDif
		Phase = PhaseT
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$