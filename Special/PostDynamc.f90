!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: Several subroutines used to perform the data process for the data from time-displaced measurements.
!          These time-displaced physical observables includes:
!              (0) Time-displaced       single-particle Green Function GREENT_Tau(NumNC, NOrbt, NOrbt, 0:NmTDM);
!              (1) Time-displaced         Spz-Spz correlation function SPINZZ_Tau(NumNC, NOrbt, NOrbt, 0:NmTDM);
!              (2) Time-displaced         Spm-Spm correlation function SPINPM_Tau(NumNC, NOrbt, NOrbt, 0:NmTDM);
!              (3) Time-displaced density-density correlation function DenDen_Tau(NumNC, NOrbt, NOrbt, 0:NmTDM);
!          These real space correlation functions are preparing for the calculation of reciprocal space correlations.
! COMMENT: Post process for time-displaced physical observables in every BIN simulation.  
! AUTHOR:  Yuan-Yao He
! DATE:    2015-01-30
! PURPOSE: Different subroutines are introduced as following:
!             
!    PostDynamc       --> Subroutine to calculate average values for the time-displaced observables;
!    FourierTrans_Tau --> Subroutine to perform Fourier Transformation for correlation functions.
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine PostDynamc(NB) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  PostDynamc(NB) 
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to perform the post process for the time-displaced physical observables and perform the Fourier 
!                  Transformations for them. 
! KEYWORDS: Post Process of time-displaced observables.
! AUTHOR:   Yuan-Yao He
! TIME:     2015-01-30
! DESCRIPTION:
!
!     Post Process of observables.
!
!     Input: NB --> The iteration number of NmBin for the PQMC simulation;  
!
!     Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt, Observable
! SUBROUTINES CALLED:  (none)
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________
		use RealPrecsn
		use CoreParamt
		use Observable
		use TimeRecord
		use QMCTimeRec
		use StdInOutSt
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		integer NB           ! Number index of the BIN 
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer(8) time1     ! Starting time point in this data post process
		integer(8) time2     ! Ending   time point in this data post process
		
		integer NT           ! Loop integer for imaginary-time
		integer NmPt         ! Number of k points; =2 for K point; =3 for M point;
		
		integer I0           ! Loop integer index
		integer I1           ! Loop integer index
		integer I2           ! Loop integer index
		integer I3           ! Loop integer index
		integer Nu_1         ! The unit cell index integer for the I1-th lattice site
		integer No_1         ! The orbital index integer for the I1-th lattice site
		integer Nu_2         ! The unit cell index integer for the I2-the lattice site 
		integer No_2         ! The orbital index integer for the I2-th lattice site
		
		integer Iu           ! Loop integer for unit cell
		integer Nk           ! Loop integer for k points
		integer NDim         ! Length of the observables
		
		integer imj
		
		integer Itp1                  ! Temporary real number
		integer Itp2                  ! Temporary real number
		integer ITmp(5)               ! Temporary real number

		complex(rp) Ztp1              ! Temporary complex number
		complex(rp) ZTmp(6, 3)        ! Temporary complex number
		
		complex(rp), allocatable :: TmpDenOcc_Tau(:, :, :)
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& 	  
!____________________ Counting calling times and time consumed in post process __________________
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&       
		TimsDatPr = TimsDatPr + 1
		call system_clock(time1)
!______________________________________________________________________________________________________________	  
!__________________ Main calculations for Average values of time-displaced observables ________________________
!______________________________________________________________________________________________________________
		Ztp1 = cmplx(1.0_rp, 0.0_rp, rp) / cmplx(dble(NObsT), 0.0_rp, rp)
!**************************************************************************************************	  
!__________ 0. The average values for time-displaced quantities ___________________________________
!**************************************************************************************************
!_______________________________________________________________________________________	  
!__________ (0) Calculate average values for static observables ________________________
!_______________________________________________________________________________________
		SpinZZ_Tau = SpinZZ_Tau * Ztp1
		SpinPM_Tau = SpinPM_Tau * Ztp1
		DenDen_Tau = DenDen_Tau * Ztp1
		GreenT_Tau = GreenT_Tau * Ztp1
		DenOcc_Tau = DenOcc_Tau * Ztp1
		
		GreenG_Tau = GreenG_Tau * Ztp1
		GreenG_Tau = GreenG_Tau / cmplx(dble(NumNC), 0.0_rp, rp)
		GreenG_Tau = GreenG_Tau / cmplx(dble(NOrbt), 0.0_rp, rp)
!**************************************************************************************************	  
!__________ 1. Calculate the DenDen_Tau1 matrix ___________________________________________________
!**************************************************************************************************
		allocate(TmpDenOcc_Tau(NumNC, NOrbt, NOrbt))
		do NT = 0, NmTDM
			TmpDenOcc_Tau = cmplx(0.0_rp, 0.0_rp, rp)
			do I1 = 1, NumNS
				Nu_1 = StList(I1, 1)
				No_1 = StList(I1, 2)
				do I2 = 1, NumNS
					Nu_2 = StList(I2, 1)
					No_2 = StList(I2, 2)
					imj = IminusJ(Nu_1, Nu_2)
					TmpDenOcc_Tau(imj, No_1, No_2) = TmpDenOcc_Tau(imj, No_1, No_2) + DenOcc_Tau(Nu_1, No_1, NT) * DenOcc_Tau(Nu_2, No_2, 0)
				enddo
			enddo
			DenDen_Tau(:, :, :, NT) = DenDen_Tau(:, :, :, NT) - TmpDenOcc_Tau(:, :, :)
		enddo
		deallocate(TmpDenOcc_Tau)
!**************************************************************************************************	  
!__________ 2. Perform fourier transformations for time-displaced quantities ______________________
!**************************************************************************************************
!_______________________________________________________________________
!_____________________ Fourier Tranformation ___________________________ 
!_______________________________________________________________________
		if(IfPr1 == 1 .and. IfPr2 ==1) then
			call FourierTrans_Tau()
		end if
!**************************************************************************************************	  
!__________ 3. Store the results of the Fourier Transformation at K point _________________________
!**************************************************************************************************
!_______________________________________________________________________________________	  
!__________ (0) The two different K points _____________________________________________
!_______________________________________________________________________________________
		if(IfKPt == 1) then
			I1 =     NumL1 / 3
			I2 = 2 * NumL2 / 3
			ITmp(1) = I1 * NumL2 + I2 + 1
			I1 = 2 * NumL1 / 3
			I2 =     NumL2 / 3
			ITmp(2) = I1 * NumL2 + I2 + 1
		end if
!_______________________________________________________________________________________	  
!__________ (1) The three different M points ___________________________________________
!_______________________________________________________________________________________		
		if(IfMPt == 1) then
			I1 = 0
			I2 = NumL2 / 2
			ITmp(3) = I1 * NumL2 + I2 + 1
			I1 = NumL1 / 2
			I2 = 0
			ITmp(4) = I1 * NumL2 + I2 + 1
			I1 = NumL1 / 2
			I2 = NumL2 / 2
			ITmp(5) = I1 * NumL2 + I2 + 1
		end if		
!_______________________________________________________________________________________	  
!__________ (2) Store the values of the calculations for output ________________________
!_______________________________________________________________________________________
		do NT = 0, NmTDM
			ZTmp = cmplx(0.0_rp, 0.0_rp, rp)
!______________________________________________________________________________  
!_____________________________ Gamma Point ____________________________________
!______________________________________________________________________________			
			I1 = 0
			I2 = 0
			Itp1 = InvKpList(I1, I2)
			do No_1 = 1, NOrbt
				ZTmp(1, 1) = ZTmp(1, 1) + GreenT_Tau(Itp1, No_1, No_1, NT)
				ZTmp(2, 1) = ZTmp(2, 1) + SpinZZ_Tau(Itp1, No_1, No_1, NT)
				ZTmp(3, 1) = ZTmp(3, 1) + SpinPM_Tau(Itp1, No_1, No_1, NT)
				ZTmp(4, 1) = ZTmp(4, 1) + DenDen_Tau(Itp1, No_1, No_1, NT)
			enddo
			ZTmp(1:4, 1) = ZTmp(1:4, 1) / cmplx(dble(NOrbt), 0.0_rp, rp)
!______________________________________________________________________________  
!_____________________________ K Point ________________________________________
!______________________________________________________________________________
			if(IfKPt == 1) then
				do No_1 = 1, NOrbt
					ZTmp(1, 2) = ZTmp(1, 2) + ( GreenT_Tau(ITmp(1), No_1, No_1, NT) + GreenT_Tau(ITmp(2), No_1, No_1, NT) ) / 2.0_rp
					ZTmp(2, 2) = ZTmp(2, 2) + ( SpinZZ_Tau(ITmp(1), No_1, No_1, NT) + SpinZZ_Tau(ITmp(2), No_1, No_1, NT) ) / 2.0_rp
					ZTmp(3, 2) = ZTmp(3, 2) + ( SpinPM_Tau(ITmp(1), No_1, No_1, NT) + SpinPM_Tau(ITmp(2), No_1, No_1, NT) ) / 2.0_rp
					ZTmp(4, 2) = ZTmp(4, 2) + ( DenDen_Tau(ITmp(1), No_1, No_1, NT) + DenDen_Tau(ITmp(2), No_1, No_1, NT) ) / 2.0_rp
				enddo
				ZTmp(1:4, 2) = ZTmp(1:4, 2) / cmplx(dble(NOrbt), 0.0_rp, rp)
			end if
!______________________________________________________________________________  
!_____________________________ M Point ________________________________________
!______________________________________________________________________________
			if(IfMPt == 1) then
				do No_1 = 1, NOrbt
					ZTmp(1, 3) = ZTmp(1, 3) + ( GreenT_Tau(ITmp(3), No_1, No_1, NT) + GreenT_Tau(ITmp(4), No_1, No_1, NT) + GreenT_Tau(ITmp(5), No_1, No_1, NT) ) / 3.0_rp
					ZTmp(2, 3) = ZTmp(2, 3) + ( SpinZZ_Tau(ITmp(3), No_1, No_1, NT) + SpinZZ_Tau(ITmp(4), No_1, No_1, NT) + SpinZZ_Tau(ITmp(5), No_1, No_1, NT) ) / 3.0_rp
					ZTmp(3, 3) = ZTmp(3, 3) + ( SpinPM_Tau(ITmp(3), No_1, No_1, NT) + SpinPM_Tau(ITmp(4), No_1, No_1, NT) + SpinPM_Tau(ITmp(5), No_1, No_1, NT) ) / 3.0_rp
					ZTmp(4, 3) = ZTmp(4, 3) + ( DenDen_Tau(ITmp(3), No_1, No_1, NT) + DenDen_Tau(ITmp(4), No_1, No_1, NT) + DenDen_Tau(ITmp(5), No_1, No_1, NT) ) / 3.0_rp
				enddo
				ZTmp(1:4, 3) = ZTmp(1:4, 3) / cmplx(dble(NOrbt), 0.0_rp, rp)
			end if
!_______________________________________________________________________________________	  
!__________ (3) Save Data for the BIN average calculations and errorbar ________________
!_______________________________________________________________________________________			
			do I1 = 1, 3
				KGreenT_Tau(NT, 2*I1 - 1, NB) = real(ZTmp(1, I1))
				KGreenT_Tau(NT, 2*I1    , NB) = imag(ZTmp(1, I1))
				
				KSpinZZ_Tau(NT, 2*I1 - 1, NB) = real(ZTmp(2, I1))
				KSpinZZ_Tau(NT, 2*I1    , NB) = imag(ZTmp(2, I1))
				
				KSpinPM_Tau(NT, 2*I1 - 1, NB) = real(ZTmp(3, I1))
				KSpinPM_Tau(NT, 2*I1    , NB) = imag(ZTmp(3, I1))
				
				KSpnSpn_Tau(NT, 2*I1 - 1, NB) = real( ZTmp(2, I1) + ZTmp(3, I1) )
				KSpnSpn_Tau(NT, 2*I1    , NB) = imag( ZTmp(2, I1) + ZTmp(3, I1) )
				
				KDenDen_Tau(NT, 2*I1 - 1, NB) = real(ZTmp(4, I1))
				KDenDen_Tau(NT, 2*I1    , NB) = imag(ZTmp(4, I1))
			enddo
!_______________________________________________________________________________________	  
!__________ (4) Calculate the global single-particle gap _______________________________
!_______________________________________________________________________________________
			RGreenG_Tau(NT, 1, NB) = real(GreenG_Tau(NT))
			RGreenG_Tau(NT, 2, NB) = imag(GreenG_Tau(NT))
		enddo
!**************************************************************************************************	  
!__________ 4. Output the time-displaced data only at some k points _______________________________
!**************************************************************************************************      
			do NT = 0, NmTDM	
!____________________________________________________________________________	  
!___________________ KGreenT_Tau, KSpinZZ_Tau, KSpinPM_Tau __________________
!_________________________ KSpnSpn_Tau, KDenDen_Tau _________________________
!____________________________________________________________________________
				write(OutPF(32), "(es25.16, A)", advance = "no") NT * Dltau, char(9)
				write(OutPF(33), "(es25.16, A)", advance = "no") NT * Dltau, char(9)
				write(OutPF(34), "(es25.16, A)", advance = "no") NT * Dltau, char(9)
				write(OutPF(35), "(es25.16, A)", advance = "no") NT * Dltau, char(9)
				write(OutPF(36), "(es25.16, A)", advance = "no") NT * Dltau, char(9)
				do I1 = 1, 6
					write(OutPF(32), "(A, es25.16)", advance = "no") char(9), KGreenT_Tau(NT, I1, NB)
					write(OutPF(33), "(A, es25.16)", advance = "no") char(9), KSpinZZ_Tau(NT, I1, NB)
					write(OutPF(34), "(A, es25.16)", advance = "no") char(9), KSpinPM_Tau(NT, I1, NB)
					write(OutPF(35), "(A, es25.16)", advance = "no") char(9), KSpnSpn_Tau(NT, I1, NB)
					write(OutPF(36), "(A, es25.16)", advance = "no") char(9), KDenDen_Tau(NT, I1, NB)
					if(I1 == 2 .or. I1 == 4) then
						write(OutPF(32), "(A)", advance = "no") char(9)
						write(OutPF(33), "(A)", advance = "no") char(9)
						write(OutPF(34), "(A)", advance = "no") char(9)
						write(OutPF(35), "(A)", advance = "no") char(9)
						write(OutPF(36), "(A)", advance = "no") char(9)
					end if
				enddo
				write(OutPF(32), "()"),
				write(OutPF(33), "()")
				write(OutPF(34), "()")
				write(OutPF(35), "()")
				write(OutPF(36), "()")
!____________________________________________________________________________	  
!_______________________________ RGreenG_Tau ________________________________
!____________________________________________________________________________				
				write(OutPF(37), "(es25.16, A)", advance = "no") NT * Dltau, char(9)
				write(OutPF(37), "(A, es25.16, A, es25.16)", advance = "no") char(9), RGreenG_Tau(NT, 1, NB), char(9), RGreenG_Tau(NT, 2, NB)
				write(OutPF(37), "()")				
			enddo
!**************************************************************************************************	  
!__________ 5. Save all the physical single-particle Green Function _______________________________
!**************************************************************************************************
			if(NB > 1) then
				if(IfOtptGF == 1) then
					do Nk = 1, NumNC
						do NT = 0, NmTDM
							do I1 = 1, NOrbt
								do I2 = 1, NOrbt
									I0 = (I1-1)*NOrbt + I2
									I3 = I0 + NOrbt * NOrbt
									AllKGreenP_Tau(Nk, NT, I0) = AllKGreenP_Tau(Nk, NT, I0) + real(GreenT_Tau(Nk, I1, I2, NT))
									AllKGreenP_Tau(Nk, NT, I3) = AllKGreenP_Tau(Nk, NT, I3) + imag(GreenT_Tau(Nk, I1, I2, NT))
								enddo
							enddo
						enddo
					enddo
				end if
			end if
!**************************************************************************************************	  
!__________ 6. Calculate the Spin Chern Number in every BIN _______________________________________
!**************************************************************************************************
			call Z2ChernNumber(NB)

	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$




!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine FourierTrans_Tau() 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  FourierTrans_Tau() 
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to Perform Fourier transformation for GreenT_Tau, SpinZZ_Tau, SpinPM_Tau, DenDen_Tau into 
!                   reciprocal space.
! KEYWORDS: Fourier transformation for time-displaced quantities.
! AUTHOR:   Yuan-Yao He
! TIME:     2015-01-30
! DESCRIPTION:
!
!     Fourier Transformation.
!
!     Input: (none).    Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt, Observable
! SUBROUTINES CALLED: GrFStatics, ExpectEngy, CorreFunct
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use Observable
		implicit none
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer Nk        ! Loop integer for different k points
		integer No1       ! Loop integer for orbital
		integer No2       ! Loop integer for orbital
		integer imj       ! Loop integer for distances between two unit cells
		
		integer NT        ! Integer index for imaginary-time
		
		integer I1        ! Index integer for distance vector between two unit cells in a1 direction
		integer I2        ! Index integer for distance vector between two unit cells in a2 direction
		integer Nx        ! Loop integer over 0 --> NumL1-1
		integer Ny        ! Loop integer over 0 --> NumL2-1
		
		real(rp) VecK(2)     ! The k point in BZ region during calculations
		real(rp) VecR(2)     ! Distance vector for two unit cells
		
		real(rp) Rtp1        ! Real temporary number

		complex(rp), allocatable :: TmpGreenT_Tau(:, :, :, :) 
		complex(rp), allocatable :: TmpSpinZZ_Tau(:, :, :, :)
		complex(rp), allocatable :: TmpSpinPM_Tau(:, :, :, :)
		complex(rp), allocatable :: TmpDenDen_Tau(:, :, :, :)
!______________________________________________________________________________________________________________	  
!__________________________________ Allocate Array and Initializations ________________________________________
!______________________________________________________________________________________________________________
		allocate(TmpGreenT_Tau(NumNC, NOrbt, NOrbt, 0:NmTDM))
		allocate(TmpSpinZZ_Tau(NumNC, NOrbt, NOrbt, 0:NmTDM))
		allocate(TmpSpinPM_Tau(NumNC, NOrbt, NOrbt, 0:NmTDM))
		allocate(TmpDenDen_Tau(NumNC, NOrbt, NOrbt, 0:NmTDM))
		TmpGreenT_Tau = GreenT_Tau
		TmpSpinZZ_Tau = SpinZZ_Tau
		TmpSpinPM_Tau = SpinPM_Tau
		TmpDenDen_Tau = DenDen_Tau
!______________________________________________________________________________________________________________	  
!_____________________________ Main calculations for all the static quantities ________________________________
!______________________________________________________________________________________________________________
!_______________________________________________________________________________________________	  
!________________ 1. Re-Initialization before the Fourier Transformation _______________________
!_______________________________________________________________________________________________
		GreenT_Tau = cmplx(0.0_rp, 0.0_rp, rp)
		SpinZZ_Tau = cmplx(0.0_rp, 0.0_rp, rp)
		SpinPM_Tau = cmplx(0.0_rp, 0.0_rp, rp)
		DenDen_Tau = cmplx(0.0_rp, 0.0_rp, rp)
!_______________________________________________________________________________________________	  
!________________ 0. Fourier Transformations for these correlation functions ___________________
!_______________________________________________________________________________________________
		do NT = 0, NmTDM
			Nk = 0
			do Nx = 0, NumL1-1
				do Ny = 0, NumL2-1
					Nk = Nk + 1
					VecK = Nx * X1Vec + Ny * X2Vec
					do No1 = 1, NOrbt
						do No2 = 1, NOrbt
							do imj = 1, NumNC
								I1 = UCList(imj, 1)
								I2 = UCList(imj, 2)
								I1 = I1 - NumL1/2 - mod(NumL1, 2)
								I2 = I2 - NumL2/2 - mod(NumL2, 2)
								VecR = I1 * A1Vec + I2 * A2Vec
								Rtp1 = dot_product(VecK, VecR)
								GreenT_Tau(Nk, No1, No2, NT) = GreenT_Tau(Nk, No1, No2, NT) + exp(cmplx(0.0_rp, Rtp1, rp)) * TmpGreenT_Tau(imj, No1, No2, NT)
								SpinZZ_Tau(Nk, No1, No2, NT) = SpinZZ_Tau(Nk, No1, No2, NT) + exp(cmplx(0.0_rp, Rtp1, rp)) * TmpSpinZZ_Tau(imj, No1, No2, NT)
								SpinPM_Tau(Nk, No1, No2, NT) = SpinPM_Tau(Nk, No1, No2, NT) + exp(cmplx(0.0_rp, Rtp1, rp)) * TmpSpinPM_Tau(imj, No1, No2, NT)
								DenDen_Tau(Nk, No1, No2, NT) = DenDen_Tau(Nk, No1, No2, NT) + exp(cmplx(0.0_rp, Rtp1, rp)) * TmpDenDen_Tau(imj, No1, No2, NT)
							enddo
						enddo
					enddo
				enddo
			enddo
		enddo
!_______________________________________________________________________________________________	  
!________________ 1. Normalize correlation function values by unit cell number _________________
!_______________________________________________________________________________________________
		GreenT_Tau = GreenT_Tau / cmplx(NumNC*1.0_rp, 0.0_rp, rp)
		SpinZZ_Tau = SpinZZ_Tau / cmplx(NumNC*1.0_rp, 0.0_rp, rp)
		SpinPM_Tau = SpinPM_Tau / cmplx(NumNC*1.0_rp, 0.0_rp, rp)
		DenDen_Tau = DenDen_Tau / cmplx(NumNC*1.0_rp, 0.0_rp, rp)
!______________________________________________________________________________________________________________	  
!___________________________________________ Deallocate the arrays ____________________________________________
!______________________________________________________________________________________________________________
		deallocate(TmpGreenT_Tau)
		deallocate(TmpSpinZZ_Tau)
		deallocate(TmpSpinPM_Tau)
		deallocate(TmpDenDen_Tau)
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$




!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine Z2ChernNumber(NB) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  Z2ChernNumber(NB) 
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to calculate the Chern number for the spin-up part of the bilayer system in every BIN
! KEYWORDS: Chern Number.
! AUTHOR:   Yuan-Yao He
! TIME:     2015-07-13
! DESCRIPTION:
!
!     Fourier Transformation.
!
!     Input: (none).    Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt, Observable
! SUBROUTINES CALLED: GrFStatics, ExpectEngy, CorreFunct
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use Observable
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		integer NB             ! Number index of the BIN 
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer Ix
		integer Iy
		
		complex(rp) Chern
		real(rp) Z2Inv(5)
		
		complex(rp),external :: FuncIntkukvUp
!______________________________________________________________________________________________________________	  
!_____________________________ Main calculations for Chern Number _____________________________________________
!______________________________________________________________________________________________________________
!_______________________________________________________________________________________________	  
!____________________ 0. Calculate the GIw0k matrix ____________________________________________
!_______________________________________________________________________________________________
		call CalculateGIw0k()
!_______________________________________________________________________________________________	  
!____________________ 1. Calculate the Chern Number ____________________________________________
!_______________________________________________________________________________________________
		Chern = cmplx(0.0_rp, 0.0_rp, rp)
		do Ix = 0, NumL1-1
			do Iy = 0, NumL2-1
				Chern = Chern + FuncIntkukvUp(Ix, Iy)
			enddo
		enddo
		Chern = Chern / cmplx(0.0_rp, 8.0_rp*rp_pi, rp)
!_______________________________________________________________________________________________	  
!____________________ 2. Calculate the Z2 invariant ____________________________________________
!_______________________________________________________________________________________________
		if(mod(NumL1, 6) == 0 .and. mod(NumL2, 6) == 0) then
			call Z2Invariant(Z2Inv)
		end if
!_______________________________________________________________________________________________	  
!____________________ 3. Store the Chern Number and Z2 result __________________________________
!_______________________________________________________________________________________________
			if(mod(NumL1, 6) == 0 .and. mod(NumL2, 6) == 0) then
				open(ExtPF(1), file = "Output/97_Z2Invariant.txt", access = "append")
				write(ExtPF(1), "(I6, A)", advance = "no") NB, char(9)
				do Ix = 1, 5
					write(ExtPF(1), "(A, es25.16)", advance = "no") char(9), Z2Inv(Ix)
				enddo 
				write(ExtPF(1), "()")
				close(ExtPF(1))
			end if
			
			open(ExtPF(2), file = "Output/98_ChernNumber.txt", access = "append")
			write(ExtPF(2), "(I6, A, A, es25.16, A, es25.16)") NB, char(9), char(9), real(Chern), char(9), imag(Chern)
			close(ExtPF(2))
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$





!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine Z2Invariant(Z2Inv) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  Z2Invariant(Z2Inv) 
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to calculate the Z2 invariant in every BIn data.
! KEYWORDS: Chern Number.
! AUTHOR:   Yuan-Yao He
! TIME:     2015-07-18
! DESCRIPTION:
!
!     Fourier Transformation.
!
!     Input: (none).    Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt, Observable
! SUBROUTINES CALLED: GrFStatics, ExpectEngy, CorreFunct
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use Observable
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		real(rp) Z2Inv(5)       ! Number index of the BIN 
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer Ix              ! Index for k points
		integer Iy              ! Index for k points
		integer Nk              ! Index for k points
!______________________________________________________________________________________________________________	  
!_____________________________ Main calculations for Chern Number _____________________________________________
!______________________________________________________________________________________________________________
!_______________________________________________________________________________________________	  
!____________________ 0. Parity at Gamma point _________________________________________________
!_______________________________________________________________________________________________
		Z2Inv = 0.0_rp
!_______________________________________________________________________________________________	  
!____________________ 1. Parity at Gamma point _________________________________________________
!_______________________________________________________________________________________________
		Ix = 0
		Iy = 0
		Nk = Ix * NumL2 + Iy + 1
		call ParityTRI(Nk, Z2Inv(1))
!_______________________________________________________________________________________________	  
!____________________ 2. Parity at M1 point ____________________________________________________
!_______________________________________________________________________________________________
		Ix = NumL1 / 2
		Iy = 0
		Nk = Ix * NumL2 + Iy + 1
		call ParityTRI(Nk, Z2Inv(2))
!_______________________________________________________________________________________________	  
!____________________ 3. Parity at M2 point ____________________________________________________
!_______________________________________________________________________________________________
		Ix = 0
		Iy = NumL2 / 2
		Nk = Ix * NumL2 + Iy + 1
		call ParityTRI(Nk, Z2Inv(3))
!_______________________________________________________________________________________________	  
!____________________ 4. Parity at M3 point ____________________________________________________
!_______________________________________________________________________________________________
		Ix = NumL1 / 2
		Iy = NumL2 / 2
		Nk = Ix * NumL2 + Iy + 1
		call ParityTRI(Nk, Z2Inv(4))
!_______________________________________________________________________________________________	  
!____________________ 5. Total Z2 parity at all TRI points _____________________________________
!_______________________________________________________________________________________________
		Z2Inv(5) = Z2Inv(1) * Z2Inv(2) * Z2Inv(3) * Z2Inv(4)
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$





!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine ParityTRI(Nk, ParityK) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  ParityTRI(Nk, ParityK) 
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to calculate the parity for occupied bands at some definite TRI points.
! KEYWORDS: Z2 invariant.
! AUTHOR:   Yuan-Yao He
! TIME:     2015-07-18
! DESCRIPTION:
!
!     Fourier Transformation.
!
!     Input: (none).    Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt, Observable
! SUBROUTINES CALLED: GrFStatics, ExpectEngy, CorreFunct
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use Observable
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		integer Nk                                              ! K point 
		real(rp) ParityK                                        ! Number index of the BIN 
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer NT
		
		real(rp) AlphK        ! Temporary complex number
		
		complex(rp) TmpMt(NOrbt, NOrbt)                   ! Temporary 6*6 complex matrix
		complex(rp), allocatable :: GTauKTmp(:, :, :)
!______________________________________________________________________________________________________________	  
!__________________________________ Allocate Array and Initializations ________________________________________
!______________________________________________________________________________________________________________
		allocate(GTauKTmp(1:NOrbt, 1:NOrbt, 0:NmTDM))
		GTauKTmp = cmplx(0.0_rp, 0.0_rp, rp)
!______________________________________________________________________________________________________________	  
!_____________________________ Main calculations for Chern Number _____________________________________________
!______________________________________________________________________________________________________________
!_______________________________________________________________________________________________	  
!____________________ 0. Obtain the physical Green Function ____________________________________
!_______________________________________________________________________________________________
		GTauKTmp = - GreenT_Tau(Nk, 1:NOrbt, 1:NOrbt, 0:NmTDM)
		do NT = 0, NmTDM
			TmpMt(1:NOrbt, 1:NOrbt) = GTauKTmp(1:NOrbt, 1:NOrbt, NT)
			GTauKTmp(1:NOrbt, 1:NOrbt, NT) = ( TmpMt + conjg(transpose(TmpMt)) )/2.0_rp
		enddo
!_______________________________________________________________________________________________	  
!____________________ 1. Some Symmetry dealing with GTauK ______________________________________
!_______________________________________________________________________________________________
		AlphK = 0.0_rp
		do NT = 0, NmTDM
			AlphK = AlphK + real(GTauKTmp(1, 2, NT))
		enddo
		AlphK = 2 * Dltau * AlphK
		AlphK = AlphK - Dltau * real(GTauKTmp(1, 2, 0) + GTauKTmp(1, 2, NmTDM))
!_______________________________________________________________________________________________	  
!____________________ 2. The final parity result _______________________________________________
!_______________________________________________________________________________________________
		if(AlphK > 0.0_rp) then
			ParityK = + 1.0_rp
		else
			ParityK = - 1.0_rp
		end if
!______________________________________________________________________________________________________________	  
!___________________________________________ Deallocate the arrays ____________________________________________
!______________________________________________________________________________________________________________
		deallocate(GTauKTmp)
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$




!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	complex(rp) function FuncIntkukvUp(Ix, Iy) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  FuncIntkukvUp(Ix, Iy) 
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to calculate the integrand of Chern Number expression in the square BZ region form.
! KEYWORDS: Chern Number calculations.
! AUTHOR:   Yuan-Yao He
! TIME:     2015-07-13
! DESCRIPTION:
!
!     Fourier Transformation.
!
!     Input: (none).    Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt, Observable
! SUBROUTINES CALLED: GrFStatics, ExpectEngy, CorreFunct
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use Observable
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		integer Ix           ! Integer index for k point in b1 direction
		integer Iy           ! Integer index for k point in b1 direction
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer Ix1          ! Ix+1
		integer Ix2          ! Ix-1
		integer Iy1          ! Iy+1
		integer Iy2          ! Iy-1
		
		integer I0
		
		complex(rp) QVa00(NOrbt, NOrbt)    ! Function value at (Ix , Iy )
		complex(rp) QVa10(NOrbt, NOrbt)    ! Function value at (Ix1, Iy )
		complex(rp) QVa20(NOrbt, NOrbt)    ! Function value at (Ix2, Iy )
		complex(rp) QVa01(NOrbt, NOrbt)    ! Function value at (Ix , Iy1)
		complex(rp) QVa02(NOrbt, NOrbt)    ! Function value at (Ix , Iy2)
		
		complex(rp) TmpMt(NOrbt, NOrbt)    ! Temporary 6*6 complex matrix
!______________________________________________________________________________________________________________	  
!__________________________________ Allocate Array and Initializations ________________________________________
!______________________________________________________________________________________________________________
		
!______________________________________________________________________________________________________________	  
!_____________________________ Main calculations for Chern Number _____________________________________________
!______________________________________________________________________________________________________________
!_______________________________________________________________________________________________	  
!____________________ 0. Calculate Ix1,Ix2,Iy1,Iy2 _____________________________________________
!_______________________________________________________________________________________________
		Ix1 = Ix + 1
		if(Ix1 > NumL1-1) then
			Ix1 = Ix1 - NumL1
		end if
		
		Ix2 = Ix - 1
		if(Ix2 < 0) then
			Ix2 = Ix2 + NumL1
		end if
		
		Iy1 = Iy + 1
		if(Iy1 > NumL2-1) then
			Iy1 = Iy1 - NumL2
		end if
		
		Iy2 = Iy - 1
		if(Iy2 < 0) then
			Iy2 = Iy2 + NumL2
		end if
!_______________________________________________________________________________________________	  
!____________________ 1. Calculate the P matrix at different k points __________________________
!_______________________________________________________________________________________________
		call FuncPUp(Ix , Iy , QVa00)
		call FuncPUp(Ix1, Iy , QVa10)
		call FuncPUp(Ix2, Iy , QVa20)
		call FuncPUp(Ix , Iy1, QVa01)
		call FuncPUp(Ix , Iy2, QVa02)
!_______________________________________________________________________________________________	  
!____________________ 2. Calculate the integrand function value at (Ix, Iy) point ______________
!_______________________________________________________________________________________________
!______________________________________________________________________________	  
!________________ (0) Calculate the matrix ____________________________________
!______________________________________________________________________________
		TmpMt = cmplx(0.0_rp, 0.0_rp, rp)
		TmpMt = TmpMt + matmul(QVa10, QVa01) - matmul(QVa01, QVa10)
		TmpMt = TmpMt + matmul(QVa01, QVa20) - matmul(QVa20, QVa01)
		TmpMt = TmpMt + matmul(QVa20, QVa02) - matmul(QVa02, QVa20)
		TmpMt = TmpMt + matmul(QVa02, QVa10) - matmul(QVa10, QVa02)
		TmpMt = matmul(QVa00, TmpMt)
!______________________________________________________________________________	  
!________________ (1) Function value is the trace _____________________________
!______________________________________________________________________________
		FuncIntkukvUp = cmplx(0.0_rp, 0.0_rp, rp)
		do I0 = 1, NOrbt
			FuncIntkukvUp = FuncIntkukvUp + TmpMt(I0, I0)
		enddo
		
	end function
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$




!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine FuncPUp(Ix, Iy, RsMat) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  FuncPUp(Ix, Iy, RsMat)
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to calculate the projector operator matrix in the expression of Chern Number, constructed by the 
!                            zero-frequency Green Function matrix.
! KEYWORDS: Chern Number.
! AUTHOR:   Yuan-Yao He
! TIME:     2015-07-13
! DESCRIPTION:
!
!     Fourier Transformation.
!
!     Input: (none).    Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt, Observable
! SUBROUTINES CALLED: GrFStatics, ExpectEngy, CorreFunct
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use Observable
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		integer Ix                         ! Integer index for k point in b1 direction
		integer Iy                         ! Integer index for k point in b1 direction
		
		complex(rp) RsMat(NOrbt, NOrbt)    ! The result 4*4 matrix
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer IERR0

		integer I0
		integer I1
		
		real(rp) HmltR(NOrbt, NOrbt)       ! Real part of GIw0k matrix
		real(rp) HmltI(NOrbt, NOrbt)       ! Imaginary part of GIw0k matrix
		real(rp) EgVal(NOrbt)              ! The eigenvalues
		real(rp) EgVcR(NOrbt, NOrbt)       ! Real part of eigenvector matrix
		real(rp) EgVcI(NOrbt, NOrbt)       ! Imaginary part of eigemvector matrix
!______________________________________________________________________________________________________________	  
!_____________________________ Main calculations for Chern Number _____________________________________________
!______________________________________________________________________________________________________________
!_______________________________________________________________________________________________	  
!____________________ 0. Diagonalize the GIw0k matrix ___________________________________________
!_______________________________________________________________________________________________
		do I0 = 1, NOrbt
			do I1 = 1, NOrbt
				HmltR(I0, I1) = real(GIw0k(I0, I1, Ix, Iy))
				HmltI(I0, I1) = imag(GIw0k(I0, I1, Ix, Iy))
			enddo
		enddo
		call HermDiag2(NOrbt, NOrbt, HmltR, HmltI, EgVal, EgVcR, EgVcI, IERR0)
!_______________________________________________________________________________________________	  
!____________________ 1. Construct the projection matrix __________________________________________________
!_______________________________________________________________________________________________
		RsMat = cmplx(0.0_rp, 0.0_rp, rp)
		do I0 = 1, NOrbt
			do I1 = 1, NOrbt
				RsMat(I0, I1) = RsMat(I0, I1) + cmplx(EgVcR(I0, 1), EgVcI(I0, 1), rp) * cmplx(EgVcR(I1, 1), -EgVcI(I1, 1), rp)
			enddo
		enddo
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$




!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine CalculateGIw0k() 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  CalculateGIw0k() 
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to calculate the zero-frequency single-particle Green Function at all possible k points.
! KEYWORDS: Chern Number.
! AUTHOR:   Yuan-Yao He
! TIME:     2015-07-13
! DESCRIPTION:
!
!     Fourier Transformation.
!
!     Input: (none).    Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt, Observable
! SUBROUTINES CALLED: GrFStatics, ExpectEngy, CorreFunct
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use Observable
		implicit none
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer Ix
		integer Iy
		
		integer I0
		integer I1
		
		integer NT
		
		integer Nk
		
		complex(rp) AIw0k(12)
		
		complex(rp) TmpMt(NOrbt, NOrbt)    ! Temporary 4*4 complex matrix

		complex(rp), allocatable :: GTauK(:, :, :)
!______________________________________________________________________________________________________________	  
!__________________________________ Allocate Array and Initializations ________________________________________
!______________________________________________________________________________________________________________
		allocate(GTauK(NOrbt, NOrbt, 0:NmTDM))
		GTauK = cmplx(0.0_rp, 0.0_rp, rp)
!______________________________________________________________________________________________________________	  
!_____________________________ Main calculations for Chern Number _____________________________________________
!______________________________________________________________________________________________________________
!_______________________________________________________________________________________________	  
!_______________________ Iteration for all the k points ________________________________________
!_______________________________________________________________________________________________
		Nk = 0
		do Ix = 0, NumL1-1
			do Iy = 0, NumL2-1
				Nk = Nk + 1
!_______________________________________________________________________________	  
!____________________ 0. Obtain the GTauK data _________________________________
!_______________________________________________________________________________
				GTauK = cmplx(0.0_rp, 0.0_rp, rp)
				do NT = 0, NmTDM
					do I0 = 1, NOrbt
						do I1 = 1, NOrbt
							if(I0 == I1) then
								GTauK(I0, I0, NT) = real(GreenT_Tau(Nk, I0, I0, NT))
							else
								GTauK(I0, I1, NT) = GreenT_Tau(Nk, I0, I1, NT)
							end if
						enddo
					enddo
				enddo
!_______________________________________________________________________________	  
!____________________ 1. Restore the physical Green Function ___________________
!_______________________________________________________________________________            
				GTauK = - GTauK
!_______________________________________________________________________________	  
!____________________ 2. Construct the Hermitian GTauK matrix __________________
!_______________________________________________________________________________            
				do NT = 0, NmTDM
					! Hermitian matrix
					TmpMt(1:NOrbt, 1:NOrbt) = GTauK(1:NOrbt, 1:NOrbt, NT)
					GTauK(1:NOrbt, 1:NOrbt, NT) = ( TmpMt + conjg(transpose(TmpMt)) )/2.0_rp
				enddo
!_______________________________________________________________________________	  
!____________________ 3. Calculate the GIw0k0 matrix at k point ________________
!_______________________________________________________________________________ 
				AIw0k = cmplx(0.0_rp, 0.0_rp, rp)
				do NT = 0, NmTDM
					AIw0k(1)  = AIw0k(1)  + GTauK(1, 1, NT) - GTauK(2, 2, NT)
					AIw0k(2)  = AIw0k(2)  + GTauK(1, 2, NT)
				enddo
				AIw0k(1) = AIw0k(1) * Dltau
				AIw0k(1) = AIw0k(1) - Dltau/2.0_rp * ( GTauK(1, 1, 0) - GTauK(2, 2, 0) + GTauK(1, 1, NmTDM) - GTauK(2, 2, NmTDM) )
				AIw0k(2) = AIw0k(2) * Dltau * 2.0_rp
				AIw0k(2) = AIw0k(2) - Dltau * ( GTauK(1, 2, 0) + GTauK(1, 2, NmTDM) )
!_______________________________________________________________________________	  
!____________________ 4. Construct the GIw0k matrix ____________________________
!_______________________________________________________________________________ 
				GIw0k(1, 1, Ix, Iy) = + AIw0k(1)
				GIw0k(2, 2, Ix, Iy) = - AIw0k(1)
				GIw0k(1, 2, Ix, Iy) = + AIw0k(2)
				GIw0k(2, 1, Ix, Iy) = + conjg(AIw0k(2))
			enddo
		enddo
!______________________________________________________________________________________________________________	  
!____________________________ Deallocate allocated arrays in the simulations __________________________________
!______________________________________________________________________________________________________________
		deallocate(GTauK)
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$