!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: A single subroutine used for perform the updating process for the Ising fields of U interaction term in the calculations. 
!                During the calculations, we perform the UDV decomposition for the B_M*B_{M-1}\cdotsB_{2}B_1*P matrix and finally get
!                a simplified expression for equal-time Green Function. The we need to get the same expression for the Green Function
!                So in this subroutine we need to update the URght and ULtRtInv matrices.
!          Includes four different subroutines.
! COMMENT: Called in updating process.
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-10
! PURPOSE: Different subroutines are introduced as following:
!             
!   UpdateFldU --> Perform the updating process of the Ising fields of U interaction term in HS transformation.
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine UpdateFldU(NT)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  UpdateFldU(NT)
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to perform the updating process of Ising fields for U interaction term.
! KEYWORDS: Updating pf Ising fields in U interaction term.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-11
! DESCRIPTION:
!
!     Updating pf Ising fields in U interaction term.
!
!     Input: NT --> Input time slice number in the calculations;
!            
!     Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt, Observable
! SUBROUTINES CALLED: NRanf, Ranf.
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use Observable
		use TimeRecord
		use QMCTimeRec
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		integer NT   ! Index for time slice
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer(8) time1    ! Starting time point in this U updating process
		integer(8) time2    ! Ending   time point in this U updating process

		integer I1          ! Loop integer for [1, NumEl]
		integer I2          ! Loop integer for [1, NumEl]
		integer I4          ! Loop integer for [1, NumNS]
		
		integer NrFlip            ! Field flip integer
		
		real(rp) Accep            ! Count the number of acceptance
		
		real(rp) Rdom             ! The random number
		real(rp) Weght            ! The weight
		real(rp) Ratio_Re         ! The real metropolis ratio
		real(rp) Ratio_Re_abs     ! The absolute Metropolis ratio
		
		complex(rp) Dlt44         ! The DltU1 value in (i, l) indexes
		complex(rp) Denom         ! The denomenator in Green Function updating
		complex(rp) G44Up         ! Coefficient in Metropolis Ratio
		complex(rp) RatioUp       ! The metropolis ratio in a single spin flavor
		complex(rp) RatioTot      ! The total metropolis ratio for all spin flavors
		
		complex(rp), allocatable :: V1(:)      ! Temporary complex vector
		complex(rp), allocatable :: U1(:)      ! Temporary complex vector
		complex(rp), allocatable :: VHlp1(:)   ! Temporary complex vector
		complex(rp), allocatable :: UHlp1(:)   ! Temporary complex vector
		
		integer, external :: NRanf
		real(rp), external :: Ranf
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& 	  
!____________________ Counting calling times and time consumed in U updating ____________________
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&       
		TimsUIntr = TimsUIntr + 1
		call system_clock(time1)
!______________________________________________________________________________________________________________	  
!__________________________________ Allocate Array and Initializations ________________________________________
!______________________________________________________________________________________________________________
		allocate(V1(NumEl))
		allocate(U1(NumEl))
		allocate(VHlp1(NumEl))
		allocate(UHlp1(NumEl))
		VHlp1 = cmplx(0.0_rp, 0.0_rp, rp)
		UHlp1 = cmplx(0.0_rp, 0.0_rp, rp)
!______________________________________________________________________________________________________________	  
!_______________________________ Main calculations of the U interaction updating ______________________________
!______________________________________________________________________________________________________________
		Accep = 0.0_rp
!________________________________________________________________________________________________	  
!_________________________ Iteration of all the lattice sites ___________________________________
!________________________________________________________________________________________________
			do I4 = 1, NumNS
!___________________________________________________________________________________	  
!_________________ Flip the Ising field on I4-th site ______________________________
!___________________________________________________________________________________
				NrFlip = NRanf(ISeed, 3)      ! Flip the Ising field
!___________________________________________________________________________________	  
!____ Calculate Metropolis Ratio and some vectors for Green Function updating ______
!___________________________________________________________________________________			
				Dlt44 = DltU1(IsngbU(I4, NT), NrFlip)                       ! Construct the diagonal matrix \Delta with only one nonzero element
				do I1 = 1, NumEl                                            ! Get the I4-th row of URght as V1 and I4-th column of ULeft as V2
					VHlp1(I1) = Dlt44 * URght(I4, I1)
					UHlp1(I1) = ULeft(I1, I4)
				enddo
				V1 = cmplx(0.0_rp, 0.0_rp, rp)
				U1 = cmplx(0.0_rp, 0.0_rp, rp)
				do I1 = 1, NumEl                                     ! The I4-th row of \Delta * U_R * (U_L * U_R)^{-1}
					do I2 = 1, NumEl
						V1(I1) = V1(I1) + VHlp1(I2) * ULtRtInv(I2, I1)
					enddo
				enddo
			   G44Up = cmplx(0.0_rp, 0.0_rp, rp)
			   do I1 = 1, NumEl                                     ! The (I4, I4)-th element of \Delta * U_R * (U_L * U_R)^{-1} * U_L		
			   	G44Up = G44Up + V1(I1) * UHlp1(I1)
			   enddo
!___________________________________________________________________________________	  
!_______________ Multiplying some constants with Metropolis Ratio __________________
!___________________________________________________________________________________			
			   RatioUp  = (cmplx(1.0_rp, 0.0_rp, rp) + G44Up) * DltU2(IsngbU(I4, NT), NrFlip)
			   RatioTot = RatioUp * conjg(RatioUp)
			   Ratio_Re = DGamL(IsngbU(I4, NT), NrFlip) * dble(RatioTot * Phase) / dble(Phase) 
			   Ratio_Re_Abs = abs(Ratio_Re)
!___________________________________________________________________________________	  
!_______________ Check if the new configuration is accepted or not _________________
!___________________________________________________________________________________			
			   Rdom = Ranf(ISeed)
			   if(Ratio_RE_Abs > Rdom) then
				   Accep = Accep + 1.0_rp
				   Weght = sqrt(dble(RatioTot*conjg(RatioTot)))
				   Phase = Phase * RatioTot / cmplx(Weght, 0.0_rp, rp)
!__________________________________________________________________________  
!___ If accepted, updates the U_R and U_L in Green Function expression ____
!__________________________________________________________________________				
				   do I1 = 1, NumEl
					   do I2 = 1, NumEl
						   U1(I1) = U1(I1) + ULtRtInv(I1, I2) * UHlp1(I2)
					   enddo
				   enddo
				   Denom = cmplx(1.0_rp, 0.0_rp, rp) / (cmplx(1.0_rp, 0.0_rp, rp) + G44Up)
				   V1 = V1 * Denom
				   do I1 = 1, NumEl              ! Update (U_L * U_R)^{-1}
				   	do I2 = 1, NumEl
					   	ULtRtInv(I1, I2) = ULtRtInv(I1, I2) - U1(I1) * V1(I2)
					   enddo
				   enddo
				   do I1 = 1, NumEl              ! Update U_R
					   URght(I4, I1) = URght(I4, I1) + Dlt44 * URght(I4, I1)
				   enddo
!__________________________________________________________________________  
!____________________ If accepted, flip the IsngbU field ___________________
!__________________________________________________________________________				
				   IsngbU(I4, NT) = FlipL(IsngbU(I4, NT), NrFlip)			
			   end if
			enddo
!______________________________________________________________________________________________________________	  
!___________ Count the number of times for calling UpdateFldU subroutine and acceptance ratio _________________
!______________________________________________________________________________________________________________
		Obser(30) = Obser(30) + cmplx(     1.0_rp, 0.0_rp, rp)
		Obser(29) = Obser(29) + cmplx(Accep/NumNS, 0.0_rp, rp)
!______________________________________________________________________________________________________________	  
!___________________________________________ Deallocate the arrays ____________________________________________
!______________________________________________________________________________________________________________
		deallocate(V1)
		deallocate(U1)
		deallocate(VHlp1)
		deallocate(UHlp1)
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&  
!____________________ Counting calling times and time consumed in U updating ____________________
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&       
		call system_clock(time2)
		TimeUIntr = TimeUIntr + TimeIntrvl(time1, time2)
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$