!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: A few subroutines used for the sweep and measure procedure in PQMC simulation, this is the main calculations in PQMC, 
!             including the following several parts:
!                (1) Calculations of all BINs;                              --> SweepMeasu()
!                (2) Calculations of all sweeps in a single BIN;            --> SweepMeasu()
!                (3) Initialization of observables for every BIN;           --> InitMCObserv()
!                (4) Initialization of time counting for every BIN;         --> QMCTimeRecInit()
!                (3) Sweep from M time slice to 1 time slice in a sweep;    --> SweepM2One()
!                (4) Interval processing of the phase setting in a sweep;   --> SweepIntVl()
!                (5) Sweep from 1 time slice to M time slice in a sweep;    --> SweepOne2M()
!                (6) Postprocess data, mainly average and outout for a BIN; --> PostStatic()
!                (7) Output the consumed time for all parts for a BIN.      --> SaveCalTim()
! COMMENT: PQMC Main simulation process. 
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-27
! PURPOSE: Different subroutines are introduced as following:
!             
!   SweepMeasu     --> Subroutine to call subroutines to perform main PQMC simulations;
!   InitMCObserv   --> Subroutine to perform initialization of observables for every BIN;
!   QMCTimeRecInit --> Subroutine to perform initialization of time counting for every BIN;
!   SweepM2One     --> Subroutine to perform sweep from M time slice to 1 time slice in a sweep;
!   SweepIntVl     --> Subroutine to perform interval processing of the phase setting in a sweep;
!   SweepOne2M     --> Subroutine to perform sweep from 1 time slice to M time slice in a sweep;
!   PostStatic     --> Subroutine to perform Postprocess data, mainly average and outout for a BIN;
!   SaveCalTim     --> Subroutine to perform output the consumed time for all parts for a BIN.
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine SweepMeasu() 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  SweepMeasu() 
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to perform the sweep for the LTrot time slices (and flipping), during the sweep we also perform the
!              meansurement for both equal-time and unequal-time physical quantities.
! KEYWORDS: Sweep and Measurement.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-01
! DESCRIPTION:
!
!     Totally, we perform NmBin different sweep and measurement.
!     In every Bin, we have NSwep different, full sweep for the space-time lattices;
!     A full sweep means a sweep from t=M to t=1 and, then from t=1 back to t=M.
!
!     Input: (none);  Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt, Observable
! SUBROUTINES CALLED: InitMCObserv, SweepM2One, SweepIntVl, SweepOne2M, PostStatic
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use Observable
		use TimeRecord
		use QMCTimeRec
		use StdInOutSt
		implicit none
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer(8) time1     ! Starting time for every BIN simulation
		integer(8) time2     ! Ending time for every BIN simulation

		integer I1           ! Loop integer for dimension of matrix
		integer I2           ! Loop integer for dimension of matrix
		integer NB           ! Loop integer for NmBin calculations
		integer NSW          ! Loop integer for NSwep calculations in every Bin
		character(40) FlBIN  ! Write the integer NB into this character 
!______________________________________________________________________________________________________________	  
!__________________________________ Allocate Array and Initializations ________________________________________
!______________________________________________________________________________________________________________
		PhaseDif = 0.0_rp     ! The phase difference between two calculations
		PhaseMax = 0.0_rp     ! The maximum phase difference between two calculations
		
		AcptU = 0.0_rp        ! The global Accepting Ratio for intra-layer U interaction
!______________________________________________________________________________________________________________	  
!____________________________ Main calculations of Sweep and Measure __________________________________________
!______________________________________________________________________________________________________________
		do NB = 1, NmBin
!**************************************************************************************************
!_______________ 0. Record the time consumed by every BIN Simulation ______________________________
!**************************************************************************************************
			write(FlBIN, "('BIN_',i2.2)") NB
			call SubroutineBgn(Trim(FlBIN), 15, time1)
!**************************************************************************************************
!_______________ 1. Some initializations of observables for every NBin ____________________________
!**************************************************************************************************
			call InitMCObserv()
!**************************************************************************************************
!_______________ 2. Initialization of Counting time quantities for every NBin _____________________
!**************************************************************************************************			
			call QMCTimeRecInit()
!**************************************************************************************************  
!_______________ 3. NSwep different sweeps in one bin _____________________________________________
!**************************************************************************************************         
			do NSW = 1, NSwep
!_______________________________________________________________________________________	  
!______________ (0) set the UL for every sweep _________________________________________
!_______________________________________________________________________________________ 
				do I1 = 1, NumEl
					do I2 = 1, NumNS
						ULeft(I1, I2) = conjg(ProjM(I2, I1))
					enddo
				enddo
!______________________________________________________________________________________	  
!______________ (1) sweep from t=M slice to t=0 slice _________________________________
!______________________________________________________________________________________
				call SweepM2One()
!______________________________________________________________________________________	  
!______________ (2) Reset URght and calculate ULeft, ULtRt, ULtRtInv __________________
!______________________________________________________________________________________
				call SweepIntVl()
!______________________________________________________________________________________	  
!______________ (3) sweep from t=0 slice to t=M slice _________________________________
!______________________________________________________________________________________
				call SweepOne2M(NSW)
!______________________________________________________________________________________	  
!______________ (4) End for the NSW=1-->NSwep iteration _______________________________
!______________________________________________________________________________________ 				
			enddo
!**************************************************************************************************  
!_______________ OO. Output the Ising fields of U and J interaction _______________________________
!**************************************************************************************************
         call IsngOutput()
!**************************************************************************************************	  
!_______________ 4. PostProcess for the Physical quantities, static and Dynamic ___________________
!**************************************************************************************************		
			call PostStatic(NB)        ! Post process for equal-time physical observables
			if(IfTAU == 1) then
				call PostDynamc(NB)        ! Post process for time-displaced physical observables
			end if
!**************************************************************************************************
!_______________ 5. Record the time consumed by every BIN Simulation ______________________________
!**************************************************************************************************
			call SubroutineEnd(Trim(FlBIN), 15, time1, time2)
			TimeSgBIN = TimeIntrvl(time1, time2)
!**************************************************************************************************
!_______________ 6. Output the consumed time in this BIN simulation to the information file _______
!**************************************************************************************************
			call SaveCalTim(NB)
!____________________________________________________________________________________________________	  
!__________________________ End for the NB=1-->NmBin iteration ______________________________________
!____________________________________________________________________________________________________  			
		enddo
		
			write(*, "()")
			write(*, "('SweepMeasu: Accepting Ratio U0 = ', es25.16)") AcptU / NmBin
			
			write(MonitorOt, "()")
			write(MonitorOt, "('SweepMeasu: Accepting Ratio U0 = ', es25.16)") AcptU / NmBin

	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$




!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine SaveCalTim(NB)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  SaveCalTim(NB)
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to output the calling times and consumed calculation time by all different parts in a single BIN 
!              QMC simulations, including the following parts:
!               (1) Intra-layer U interaction updating     --> TimsUIntr, TimeUIntr
!               (2) Intra-layer V interaction updating     --> TimsVIntr, TimeVIntr
!               (3) Intra-layer J interaction updating     --> TimsJIntr, TimeJIntr
!               (4) Inter-layer J interaction updating     --> TimsJLayr, TimeJLayr
!               (5) Wrapping for Hopping term              --> TimsKWrap, TimeKWrap
!               (6) Wrapping for Intra-layer U interaction --> TimsUWrap, TimeUWrap
!               (7) Wrapping for Intra-layer V interaction --> TimsVWrp0, TimeVWrp0
!               (8) Wrapping for Intra-layer J interaction --> TimsJWrp0, TimeJWrp0
!               (9) Wrapping for Inter-layer J interaction --> TimsJWrpq, TimeJWrpq
!              (10) UDV decomposition                      --> TimsUDVOt, TimeUDVOt
!              (11) Observables Measurement                --> TimsStaMe, TimeStaMe
!              (12) Total time For a BIN                   --> TimsSgBIN
! KEYWORDS: Output calculation time.
! AUTHOR:   Yuan-Yao He
! TIME:     2015-01-12
! DESCRIPTION:
!
!     Output all the calculation time consumed in PQMC simulation in a single BIN.
!
!     Input: (none);  Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt, Observable
! SUBROUTINES CALLED: InitMCObserv, SweepM2One, SweepIntVl, SweepOne2M, PostStatic
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use TimeRecord
		use QMCTimeRec
		use Observable
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		integer NB           ! Loop integer for NmBin calculations
!______________________________________________________________________________________________________________	  
!____________________________ Main calculations of Sweep and Measure __________________________________________
!______________________________________________________________________________________________________________
			TimeTotal = TimeUIntr + TimeKWrap + TimeUWrap + TimeUDVOt + TimeStaMe + TimeDynMe + TimeDatPr
			write(OutPF(1), "()")
			write(OutPF(1), "()")
			write(OutPF(1), "()")
			write(OutPF(1), "('++++++++++++++++++++++++++++++++++++++ BIN ', I2.2, ' +++++++++++++++++++++++++++++++++++++++++++++++++')") NB
			write(OutPF(1), "(A12, I12)") "    LTrot = ", LTrot
			write(OutPF(1), "(A12, I12)") "    NSwep = ", NSwep
			write(OutPF(1), "(A12, I12)") "    N_Mea = ", (MeaTt+1)*2
			write(OutPF(1), "(A12, I12)") "TimsUIntr = ", TimsUIntr
			write(OutPF(1), "(A12, I12)") "TimsKWrap = ", TimsKWrap
			write(OutPF(1), "(A12, I12)") "TimsUWrap = ", TimsUWrap
			write(OutPF(1), "(A12, I12)") "TimsUDVOt = ", TimsUDVOt
			write(OutPF(1), "(A12, I12)") "TimsMtInv = ", TimsMtInv
			write(OutPF(1), "(A12, I12)") "TimsStaMe = ", TimsStaMe
			write(OutPF(1), "(A12, I12)") "TimsDynMe = ", TimsDynMe
			write(OutPF(1), "(A12, I12)") "TimsDatPr = ", TimsDatPr
			write(OutPF(1), "(A12, F16.5, A4, F13.5, A4, F10.5, A1)") "TimeUIntr = ", TimeUIntr, "s = ", Sec2Min(TimeUIntr), "m = ", Sec2Hour(TimeUIntr), "h"
			write(OutPF(1), "(A12, F16.5, A4, F13.5, A4, F10.5, A1)") "TimeKWrap = ", TimeKWrap, "s = ", Sec2Min(TimeKWrap), "m = ", Sec2Hour(TimeKWrap), "h"
			write(OutPF(1), "(A12, F16.5, A4, F13.5, A4, F10.5, A1)") "TimeUWrap = ", TimeUWrap, "s = ", Sec2Min(TimeUWrap), "m = ", Sec2Hour(TimeUWrap), "h"
			write(OutPF(1), "(A12, F16.5, A4, F13.5, A4, F10.5, A1)") "TimeUDVOt = ", TimeUDVOt, "s = ", Sec2Min(TimeUDVOt), "m = ", Sec2Hour(TimeUDVOt), "h"
			write(OutPF(1), "(A12, F16.5, A4, F13.5, A4, F10.5, A1)") "TimeMtInv = ", TimeMtInv, "s = ", Sec2Min(TimeMtInv), "m = ", Sec2Hour(TimeMtInv), "h"
			write(OutPF(1), "(A12, F16.5, A4, F13.5, A4, F10.5, A1)") "TimeStaMe = ", TimeStaMe, "s = ", Sec2Min(TimeStaMe), "m = ", Sec2Hour(TimeStaMe), "h"
			write(OutPF(1), "(A12, F16.5, A4, F13.5, A4, F10.5, A1)") "TimeDynMe = ", TimeDynMe, "s = ", Sec2Min(TimeDynMe), "m = ", Sec2Hour(TimeDynMe), "h"
			write(OutPF(1), "(A12, F16.5, A4, F13.5, A4, F10.5, A1)") "TimeDatPr = ", TimeDatPr, "s = ", Sec2Min(TimeDatPr), "m = ", Sec2Hour(TimeDatPr), "h"
			write(OutPF(1), "(A12, F16.5, A4, F13.5, A4, F10.5, A1)") "TimeTotal = ", TimeTotal, "s = ", Sec2Min(TimeTotal), "m = ", Sec2Hour(TimeTotal), "h"
			write(OutPF(1), "(A12, F16.5, A4, F13.5, A4, F10.5, A1)") "TimeSgBIN = ", TimeSgBIN, "s = ", Sec2Min(TimeSgBIN), "m = ", Sec2Hour(TimeSgBIN), "h"
			write(OutPF(1), "('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')")
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$




!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine IsngOutput()
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  IsngOutput()
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to output the Ising fields of intra-layer U interaction and inter-layer J interaction. 
! KEYWORDS: Output Ising fields.
! AUTHOR:   Yuan-Yao He
! TIME:     2015-04-13
! DESCRIPTION:
!
!     Output all the Ising fields in PQMC simulation in a single BIN.
!
!     Input: (none);  Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt, Observable
! SUBROUTINES CALLED: (none)
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		implicit none
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________		
		integer I2
		integer I1
		integer I3
!______________________________________________________________________________________________________________	  
!____________________________ Output the Ising fields for the calculations ____________________________________
!______________________________________________________________________________________________________________
!VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV	  
!________________________ (0). Intra-Layer U interaction ___________________________________________
!VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
		if( abs(HubbU) > 1.0E-10_rp ) then
			open(ExtPF(50), file = "Output/AA_UIntrIsingField.txt")
			do I2 = 1, NumNS
				write(ExtPF(50), "(I4)", advance = "no") IsngbU(I2, 1)
				do I1 = 2, LTrot
					write(ExtPF(50), "(A, I4)", advance = "no") char(9), IsngbU(I2, I1)
				enddo
				write(ExtPF(50), "()")
			enddo
			close(ExtPF(50))
		end if
			
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$