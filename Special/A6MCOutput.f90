!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: Several subroutines used to perform Initialization of PQMC output files before the whole PQMC simulations.
! COMMENT: PQMC Initialization process.
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-27
! PURPOSE: Different subroutines are introduced as following:
!             
!   InitMCOutput --> Subroutine to Initialization of PQMC output files used in PQMC;
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine InitMCOutput()
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  InitMCOutput()
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to perform the initialization for all output files handles for PQMC data. 
! KEYWORDS: Initialization of PQMC output file handles.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-10
! DESCRIPTION:
!
!     Initialization of PQMC output file handles, including:
!             (0) Initializations of The output files Handles integer;
!             (1) Open the output files for the PQMC calculations;
!             (2) Output the PQMC calculation information.
!
!     Input:  (none)   Output: (none)
!
! MODULES USED: RealPrecsn, CoreParamt
! SUBROUTINES CALLED:  (none)
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use Observable
		use StdInOutSt
		implicit none
!______________________________________________________________________________________________________________	  
!_____________________________ Main calculations of PQMC output file handles __________________________________
!______________________________________________________________________________________________________________
!**************************************************************************************************	  
!___________________ 0. The output files Handles integer __________________________________________
!**************************************************************************************************
!________________________________________________________________________________________	  
!_______ (0) Calculation information and noninteracting single-particle energy level ____
!________________________________________________________________________________________
		OutPF(1)  = 229     ! For outputting the PQMC simulations information and time consumed in every BIN simulation
		OutPF(2)  = 231     ! For Outputting the Single-Particle energy levels of the noninteracting Hamiltonian matrix
!________________________________________________________________________________________	  
!_______ (1) Expectation energies and accepting ratio for all BINs simulations __________
!________________________________________________________________________________________		
		OutPF(3)  = 233     ! For outputting the PQMC calculated Expectation energies for all the terms for all the NmBIN simulations
		OutPF(4)  = 237     ! For outputting the accepting ratio for U, V, J interactions in every BIN calculation
!________________________________________________________________________________________	  
!_______ (2) Real space correlation functions for all BINs simulations __________________
!________________________________________________________________________________________		
		OutPF(5)  = 239     ! For Outputting the Sz-Sz correlation function in real space for all the NmBIN simulations --> Without -<A_i>*<A_j>
		OutPF(6)  = 241     ! For Outputting the S+*S- correlation function in real space for all the NmBIN simulations
		OutPF(7)  = 243     ! For Outputting the S_i*S_j correlation function in real space for all the NmBIN simulations
		OutPF(8)  = 247     ! For Outputting the ni-nj correlation function in real space for all the NmBIN simulations --> With -<A_i>*<A_j>
		OutPF(9)  = 249     ! For Outputting the <c_{i\u}^+ c_{j\u}> + <c_{i\d}^+ c_{j\d}>
!________________________________________________________________________________________	  
!_______ (3) Reciprocal space correlation functions at (0, 0) for all BINs simulations __
!________________________________________________________________________________________			
		OutPF(10) = 251     ! For Outputting <c_{i\u}^+ c_{j\u}> + <c_{i\d}^+ c_{j\d}> 
		OutPF(11) = 253     ! For Outputting Sz-Sz correlation function in reciprocal space for all the NmBIN simulations --> Without -<A_i>*<A_j>
		OutPF(12) = 257     ! For Outputting S+*S- correlation function in reciprocal space for all the NmBIN simulations
		OutPF(13) = 259     ! For Outputting total spin-spin correlation function in reciprocal space
		OutPF(14) = 261     ! For Outputting ni-nj correlation function in reciprocal space for all the NmBIN simulations --> With -<A_i>*<A_j>	
!________________________________________________________________________________________	  
!_______ (4) Final average and errorbar for expectation energies and accepting ratio ____
!________________________________________________________________________________________		
		OutPF(15) = 263     ! For average expectation energy values and errorbar for all terms after all NBIN calculations
		OutPF(16) = 267     ! For Outputting the accepting ratio for U, V, J interactions
!________________________________________________________________________________________	  
!_______ (5) Final average and errorbar for Real space correlation functions ____________
!________________________________________________________________________________________			
		OutPF(17) = 269     ! Output the mean and error bar of <Sz * Sz> correlation function (real space) --> Without -<A_i>*<A_j>
		OutPF(18) = 271     ! Output the mean and error bar of (<S+*S->+<S-*S+>)(real space)
		OutPF(19) = 273     ! Output the mean and error bar of <S_i * S_j> correlation function (real space)
		OutPF(20) = 277     ! Output the mean and error bar of <n_i*n_j> correlation function (real space) --> With -<A_i>*<A_j>
		OutPF(21) = 279     ! Output the mean and error bar of (n_{ij\u}+n_{ij\d}) occupancy (real space)	
!________________________________________________________________________________________	  
!_______ (6) Final average and errorbar for reciprocal space correlation functions ______
!________________________________________________________________________________________			
		OutPF(22) = 281     ! Output the momentum distribution after all the NmBIN simulations
		OutPF(23) = 283     ! Errorbar of momentum distribution after all the NmBIN simulations
		
		OutPF(24) = 287     ! Output the z-direction spin structure factor after all the NmBIN simulations  --> Without -<A_i>*<A_j>
		OutPF(25) = 289     ! Errorbar of z-direction spin structure factor after all the NmBIN simulations --> Without -<A_i>*<A_j>
		
		OutPF(26) = 291     ! Output the xy-plane spin structure factor after all the NmBIN simulations
		OutPF(27) = 293     ! Errorbar of xy-plane spin structure factor after all the NmBIN simulations
		
		OutPF(28) = 297     ! Output the spin-spin correlation function after all the NmBIN simulations	
		OutPF(29) = 299     ! Errorbar of spin-spin correlation function after all the NmBIN simulations  
		
		OutPF(30) = 301     ! Output the density-density after all the NmBIN simulations                    --> With -<A_i>*<A_j>        
		OutPF(31) = 303     ! Errorbar of density-density after all the NmBIN simulations                   --> With -<A_i>*<A_j>	 
!________________________________________________________________________________________	  
!_______ (7) Reciprocal space time-displaced physical quantities in all Bin's ___________
!________________________________________________________________________________________
		OutPF(32) = 307     ! Output the average of GreenT_tau(k)
		OutPF(33) = 309     ! Output the average of <Sz(\tau) * Sz>(k)
		OutPF(34) = 311     ! Output the average of <Spm(\tau) * Spm>(k)
		OutPF(35) = 313     ! Output the average of <Spn(\tau) * Spn>(k)
		OutPF(36) = 317     ! Output the average of <n_i(\tau) * n_j>(k)
		OutPF(37) = 319     ! Output the average of global single-particle Green Function
!________________________________________________________________________________________	  
!_______ (8) Mean And Errorbar time-displaced physical quantities _______________________
!________________________________________________________________________________________
		OutPF(38) = 321     ! Output the final mean value of GreenT_tau(k) after all BINs
		OutPF(39) = 323     ! Output the final mean value of <Sz(\tau) * Sz>(k) after all BINs
		OutPF(40) = 327     ! Output the final mean value of <Spm(\tau) * Spm>(k) after all BINs
		OutPF(41) = 329     ! Output the final mean value of <Spn(\tau) * Spn>(k) after all BINs
		OutPF(42) = 331     ! Output the final mean value of <n_i(\tau) * n_j>(k) after all BINs
		OutPF(43) = 333     ! Output the final mean value of global single-particle Green Function
!________________________________________________________________________________________	  
!_______ (8) Final average and errorbar for all structure factors _______________________
!________________________________________________________________________________________		
		OutPF(44) = 337		
!________________________________________________________________________________________	  
!_______ (9) Other output files _________________________________________________________
!________________________________________________________________________________________		
		ExtPF(1)  = 999
		ExtPF(2)  = 997
		ExtPF(3)  = 993
		ExtPF(4)  = 991
		ExtPF(5)  = 989
		ExtPF(6)  = 987
		ExtPF(7)  = 983
		ExtPF(8)  = 981
		
		ExtPF(44) = 573     ! 
		ExtPF(45) = 577     ! 
		ExtPF(46) = 579     !  
		ExtPF(47) = 581     ! 
		ExtPF(48) = 583     ! 
		ExtPF(49) = 587     ! 
		ExtPF(50) = 589     ! Store the Ising fields for                 U interaction
!**************************************************************************************************  
!___________________ 1. Open the output files for the PQMC calculations ___________________________
!**************************************************************************************************
!________________________________________________________________________________________	  
!_______ (0) Calculation information and noninteracting single-particle energy level ____
!________________________________________________________________________________________
			open( OutPF(1), file = "Output/00_PQMCInformation.txt", access = "append")   ! Output the model parameters information
			open( OutPF(2), file = "Output/00_NonInteractBand.txt")   ! Output the single-particle energy levels for noninteracting system
!________________________________________________________________________________________	  
!_______ (1) Expectation energies and accepting ratio for all BINs simulations __________
!________________________________________________________________________________________		
			open( OutPF(3), file = "Output/01_QMCExpectEnergy.txt", access = "append")   ! Output the PQMC calculated expectation energy values for all terms in Hamiltonian
			open( OutPF(4), file = "Output/01_UpdtAcceptRatio.txt", access = "append")   ! Output the accepting ratio for U, V, J interactions
!________________________________________________________________________________________	  
!_______ (2) Real space correlation functions for all BINs simulations __________________
!________________________________________________________________________________________
			open( OutPF(5), file = "Output/02_RSpinZZCorrFunc.txt", access = "append")   ! Output the <Sz*Sz> correlation function in real space    --> Without -<A_i>*<A_j>
			open( OutPF(6), file = "Output/02_RSpinPMCorrFunc.txt", access = "append")   ! Output the (<S+*S->+<S-*S+>) correlation function in real space
			open( OutPF(7), file = "Output/02_RSpnSpnCorrFunc.txt", access = "append")   ! Output the (<S_i*S_j>) correlation function in real space
			open( OutPF(8), file = "Output/02_RDenDenCorrFunc.txt", access = "append")   ! Output the <n_i*n_j> correlation function in real space  --> With -<A_i>*<A_j>
			open( OutPF(9), file = "Output/02_ROccupy_On_Site.txt", access = "append")   ! Output the total occupation <c_i^+ c_j> for (i,j) sites
!________________________________________________________________________________________	  
!_______ (3) Reciprocal space correlation functions at (0, 0) for all BINs simulations __
!________________________________________________________________________________________
			if(IfPr1 == 1 .and. IfPr2 ==1) then
				open(OutPF(10), file = "Output/03_KGreenTCorrFunc.txt", access = "append")   ! Momentum distribution correlation at (0, 0) point
				open(OutPF(11), file = "Output/03_KSpinZZCorrFunc.txt", access = "append")   ! SpZ-SpZ correlation at (0, 0) point               --> Without -<A_i>*<A_j>
				open(OutPF(12), file = "Output/03_KSpinPMCorrFunc.txt", access = "append")   ! Sp+-Sp- correlation at (0, 0) point     
				open(OutPF(13), file = "Output/03_KSpnSpnCorrFunc.txt", access = "append")   ! Spin-Spin correlation at (0, 0) point             --> Without -<A_i>*<A_j>
				open(OutPF(14), file = "Output/03_KDenDenCorrFunc.txt", access = "append")   ! Density-Density correlation at (0, 0) point       --> With -<A_i>*<A_j> 
			end if 
!________________________________________________________________________________________	  
!_______ (4) Final average and errorbar for expectation energies and accepting ratio ____
!________________________________________________________________________________________	
			open(OutPF(15), file = "Output/05_QMCEnergyAverag.txt")   ! Output the average expectation energy values and errorbar for all terms after all NBIN calculations
			open(OutPF(16), file = "Output/05_QMCAcceptgRatio.txt")   ! Output average accepting ratio for U, V, J interactions during updating	
!________________________________________________________________________________________	  
!_______ (5) Final average and errorbar for Real space correlation functions ____________
!________________________________________________________________________________________		
			open(OutPF(17), file = "Output/06_RSpinZZ_AvgErrB.txt")   ! Output the mean and error bar of <Sz * Sz> correlation function (real space) --> Without -<A_i>*<A_j>
			open(OutPF(18), file = "Output/06_RSpinPM_AvgErrB.txt")   ! Output the mean and error bar of (<S+*S->+<S-*S+>)(real space)           
			open(OutPF(19), file = "Output/06_RSpnSpn_AvgErrB.txt")   ! Output the mean and error bar of <S_i * S_j> (real space) 
			open(OutPF(20), file = "Output/06_RDenDen_AvgErrB.txt")   ! Output the mean and error bar of <n_i*n_j> correlation function (real space) --> With -<A_i>*<A_j>
			open(OutPF(21), file = "Output/06_ROccupy_AvgErrB.txt")   ! Output the total occupation <c_i^+ c_j> for (i,j) sites
!________________________________________________________________________________________	  
!_______ (6) Final average and errorbar for reciprocal space correlation functions ______
!________________________________________________________________________________________
			if(IfPr1 == 1 .and. IfPr2 ==1) then 
				open(OutPF(22), file = "Output/07_KGreenT_Average.txt")   ! Output the momentum distribution after all the NmBIN simulations 
				open(OutPF(23), file = "Output/07_KGreenTErrorBar.txt")   ! Errorbar of momentum distribution after all the NmBIN simulations 
				
				open(OutPF(24), file = "Output/07_KSpinZZ_Average.txt")   ! Output the z-direction spin structure factor after all the NmBIN simulations  --> Without -<A_i>*<A_j>
				open(OutPF(25), file = "Output/07_KSpinZZErrorBar.txt")   ! Errorbar of z-direction spin structure factor after all the NmBIN simulations --> Without -<A_i>*<A_j>
				
				open(OutPF(26), file = "Output/07_KSpinPM_Average.txt")   ! Output the xy-plane spin structure factor after all the NmBIN simulations
				open(OutPF(27), file = "Output/07_KSpinPMErrorBar.txt")   ! Errorbar of xy-plane spin structure factor after all the NmBIN simulations
				
				open(OutPF(28), file = "Output/07_KSpnSpn_Average.txt")   ! Output the Total spin structure factor after all the NmBIN simulations     
				open(OutPF(29), file = "Output/07_KSpnSpnErrorBar.txt")   ! Errorbar of Total spin structure factor after all the NmBIN simulations
				
				open(OutPF(30), file = "Output/07_KDenDen_Average.txt")   ! Output the density-density after all the NmBIN simulations                    --> With -<A_i>*<A_j>
				open(OutPF(31), file = "Output/07_KDenDenErrorBar.txt")   ! Errorbar of density-density after all the NmBIN simulations                   --> With -<A_i>*<A_j>
			end if
!________________________________________________________________________________________	  
!_______ (7) Time-displaced quantities in every BIN at some k points ____________________
!________________________________________________________________________________________
			if(IfTAU == 1) then
				open(OutPF(32), file = "Output/04_KGreenTCorFun_Tau.txt", access = "append")   ! Output the average of GreenT_tau(k) after all BINs
				open(OutPF(33), file = "Output/04_KSpinZZCorFun_Tau.txt", access = "append")   ! Output the average of <Sz(\tau) * Sz>(k) after all BINs
				open(OutPF(34), file = "Output/04_KSpinPMCorFun_Tau.txt", access = "append")   ! Output the average of <Spm(\tau) * Spm>(k) after all BINs
				open(OutPF(35), file = "Output/04_KSpnSpnCorFun_Tau.txt", access = "append")   ! Output the average of <Spn(\tau) * Spn>(k) after all BINs
				open(OutPF(36), file = "Output/04_KDenDenCorFun_Tau.txt", access = "append")   ! Output the average of <n_i(\tau) * n_j>(k) after all BINs
				open(OutPF(37), file = "Output/04_RGreenGCorFun_Tau.txt", access = "append")   ! Output the average of global single-particle GF
				
				open(OutPF(38), file = "Output/08_KGreenTAvgErr_Tau.txt")   ! Output the final mean value of GreenT_tau(k) after all BINs
				open(OutPF(39), file = "Output/08_KSpinZZAvgErr_Tau.txt")   ! Output the final mean value of <Sz(\tau) * Sz>(k) after all BINs
				open(OutPF(40), file = "Output/08_KSpinPMAvgErr_Tau.txt")   ! Output the final mean value of <Spm(\tau) * Spm>(k) after all BINs
				open(OutPF(41), file = "Output/08_KSpnSpnAvgErr_Tau.txt")   ! Output the final mean value of <Spn(\tau) * Spn>(k) after all BINs
				open(OutPF(42), file = "Output/08_KDenDenAvgErr_Tau.txt")   ! Output the final mean value of <n_i(\tau) * n_j>(k) after all BINs
				open(OutPF(43), file = "Output/08_RGreenGAvgErr_Tau.txt") ! Output the final mean value of global single-particle GF
			end if
!________________________________________________________________________________________	  
!_______ (8) Final average and errorbar for all structure factors _______________________
!________________________________________________________________________________________
			if(IfPr1 == 1 .and. IfPr2 ==1) then
				open(OutPF(44), file = "Output/09_KStructureFactors.txt")
			end if
!**************************************************************************************************  
!___________________ 2. Output the information for the PQMC calculations __________________________
!**************************************************************************************************
			write(OutPF(1), "('===============================================================================================')")
			write(OutPF(1), "('PQMC Simulation of t1-td-t3-U Kane-Mele-Hubbard Model:')")
			write(OutPF(1), "('                                 Linear Size : NumL1 = ',     I12)") NumL1
			write(OutPF(1), "('                                 Linear Size : NumL2 = ',     I12)") NumL2
			write(OutPF(1), "('           Number of Orbitals in a unit cell : NOrbt = ',     I12)") NOrbt
			write(OutPF(1), "('               Total Number of lattice sites : NumNS = ',     I12)") NumNS
			write(OutPF(1), "('                         Number of electrons : NumEl = ',     I12)") NumEl
			write(OutPF(1), "('          Nearest-Neighbor hopping parameter : Hopt1 = ', es25.16)") Hopt1
			write(OutPF(1), "('        Dimerized Nearest-Neighbor parameter : Dimer = ', es25.16)") Dimer
			write(OutPF(1), "('                     Intrinsic SOC parameter : HopSC = ', es25.16)") HopSC
			write(OutPF(1), "('    Third-Nearest-Neighbor hopping parameter : Hopt3 = ', es25.16)") Hopt3
			write(OutPF(1), "('                   On-Site Coulomb Repulsion : HubbU = ', es25.16)") HubbU
			write(OutPF(1), "('              Twist in x-direction Phi/Phi_0 : TwstX = ', es25.16)") TwstX
			write(OutPF(1), "('           External Magnetic field Phi/Phi_0 : Magnt = ',     I12)") Magnt
			write(OutPF(1), "('               Effective Inverse Temperature : BetaT = ', es25.16)") BetaT
			write(OutPF(1), "('                     Imaginary-time Interval : Dltau = ', es25.16)") Dltau
			write(OutPF(1), "('               Number of time slices in PQMC : LTrot = ',     I12)") LTrot
			write(OutPF(1), "('             Frequency for UDV decomposition : NWrap = ',     I12)") NWrap
			write(OutPF(1), "('     Number of times for independent Measure : NmBin = ',     I12)") NmBin
			write(OutPF(1), "('       Number of full Sweeps in a single BIN : NSwep = ',     I12)") NSwep
			write(OutPF(1), "('         Starting time slice for measurement : MeaSt = ',     I12)") MeaSt
			write(OutPF(1), "('           Ending time slice for measurement : MeaEn = ',     I12)") MeaEn
			write(OutPF(1), "('       Number of time slices for measurement : MeaTt = ',     I12)") MeaTt
			write(OutPF(1), "('Value of the Random Seed for PQMC simulation : ISeed = ',     I12)") Iseed
			write(OutPF(1), "()")
			write(OutPF(1), "('===============================================================================================')")
!__________________________________________________________________________________________________	  
!___________________ 3. Some Output presetting for output files ___________________________________
!__________________________________________________________________________________________________
			write(OutPF(3), "(A6, A19, A32, A32, A32, A32, A32, A40, A32, A30)") "BIN", "EHopt1", &
				& "EHoptd", "EHopSC", "EHopt3", "EUIntr", "ETotal", &
				& "DouOcc", "TotOcc", "Sign"
			write(OutPF(4), "(A6, A19)") "BIN", "AcptU"
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$