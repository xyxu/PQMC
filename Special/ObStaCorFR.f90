!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: A subroutine used to perform the equal-time meansurement of the several different correlation functions in real space for 
!              site pairs with different distance, including the following correlation functions:
!                 (1) SpinZ-SpinZ correlation function;                    --> SpZSpZ
!                 (2) SpnPM-SpnPM correlation function;                    --> SpmSpm
!                 (3) Full vector kind of Spin-Spin correlation function;  --> SpnSpn = SpZSpZ + SpmSpm/2.0_rp
!                 (4) Density-Density Correlation function;                --> DesDes
!                 (5) Full occupation at some lattice sites;               --> Occupy = n_iu + n_id
!             Here, the expectation values of correlation functions with and without -<A_i><A_j> are both calculated. And the calculated
!                 occupation on some site always has a small imaginary part, which is comparable with the errorbar estimated from the 
!                 measurement times in a BIN simulation. Its real part is always exactly equal to one. In this occupation results, we can
!                 actually check if the PQMC results are reasonable. 
!          The calculated lattice sites pairs in real space includes the following 8 lattice sites pairs:
!                 (1) 0,1,2,3-nearest-neighbors of the 1-th lattice site;  --> (1,1), (1,2), (1,3), (1,4)
!                 (2) AA, BB, AB, BA sites in the two farthest unit cell;  --> Distance (L/2, L/2)
! COMMENT: Calculate the real space correlation functions.
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-27
! PURPOSE: Different subroutines are introduced as following:
!             
!   ObStaCorFR --> Subroutine to calculate the Real space correlation functions with several different lattice sites pairs;
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine ObStaCorFR(GUp, GUpC, GDw, GDwC)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  ObStaCorFR(GUp, GUpC, GDw, GDwC)
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to calculate the three correlation functions for the 3*2 finite lattice.
!                 SpZSpZ = <Sz_i Sz_j>
!                 SpmSpm = <S^+_i S^-_j>+<S^-_i S^+_j>
!                 DesDes = <n_i * n_j>
! KEYWORDS: Calculate the three correlation functions.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-01
! DESCRIPTION:
!
!     Due to the periodic boundary condition we applied, there are five different distances for the lattice sites in 3*2 finite system.
!       (1, 1) --> 0
!       (1, 2) --> 1
!       (1, 3) --> sqrt(3.0)
!       (1, 4) --> 2
!       The unit cells with maximum distance, and AA, BB, AB, BA correlation functions. 
!
!     Input: GUp  --> The spin-up Green Function calculated from PQMC;   --> <c_i c_j^+>
!            GUpC --> The spin-up Green Function calculated from PQMC;   --> <c_i^+ c_j>
!            GDw  --> The spin-down Green Function calculated from PQMC; --> <c_i c_j^+>
!            GDwC --> The spin-down Green Function calculated from PQMC; --> <c_i^+ c_j>
!
!     Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt, Observable
! SUBROUTINES CALLED:  (none)
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use Observable
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		complex(rp) GUp (NumNS, NumNS) 
		complex(rp) GUpC(NumNS, NumNS)
		complex(rp) GDw (NumNS, NumNS) 
		complex(rp) GDwC(NumNS, NumNS)
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer Id            ! Loop integer
		integer Jd            ! Loop integer
		integer I1            ! Index integer for lattice site
		integer I2            ! Index integer for lattice site
		
		integer XLatt(NumCor) ! Lattice sites for calculating the correlation function
		integer YLatt(NumCor) ! Lattice sites for calculating the correlation function
		
		real(rp) Rtp1         ! Temporary real number
		real(rp) Rtp2         ! Temporary real number

		complex(rp) Ztp1      ! Temporary complex number
		complex(rp) Ztp2      ! Temporary complex number
		complex(rp) Ztp3      ! Temporary complex number
		complex(rp) Ztp4      ! Temporary complex number
		complex(rp) Ztp5      ! Temporary complex number
		complex(rp) Ztp6      ! Temporary complex number
		
		complex(rp), allocatable :: TmpDen(:)     ! Temporary array for density on every lattice site
		complex(rp), allocatable :: TmpSpZ(:)     ! Temporary array for Spin Z on every lattice site
!______________________________________________________________________________________________________________	  
!__________________________________ Allocate Array and Initializations ________________________________________
!______________________________________________________________________________________________________________
		allocate(TmpDen(NumNS))
		allocate(TmpSpZ(NumNS))
		TmpDen = cmplx(0.0_rp, 0.0_rp)
		TmpSpZ = cmplx(0.0_rp, 0.0_rp)
!______________________________________________________________________________________________________________	  
!________________________________________ Main calculations ___________________________________________________
!______________________________________________________________________________________________________________
!______________________________________________________________________________________________	  
!_______________________ 0. Check NumCal and the lattice sites for correlation ________________
!______________________________________________________________________________________________
		NumCal = 4

		XLatt(1) = 1;  YLatt(1) = 1
		XLatt(2) = 1;  YLatt(2) = 2
		XLatt(3) = 1;  YLatt(3) = 3
		XLatt(4) = 1;  YLatt(4) = 4
!______________________________________________________________________________________________	  
!_______________________ 1. Calculate the TmpDen and TmpSpZ arrays ____________________________
!______________________________________________________________________________________________
		do Id = 1, NumNS
			TmpDen(Id) = GUpC(Id, Id) + GDwC(Id, Id)
			TmpSpZ(Id) = GUpC(Id, Id) - GDwC(Id, Id)
		enddo
!______________________________________________________________________________________________	  
!_______________________ 2. Count the sign during the calculation _____________________________
!______________________________________________________________________________________________
		Rtp1 = 1.0_rp
		Rtp2 = dble(Phase)
		if(Rtp2 .lt. 0.0_rp) Rtp1 = -1.0_rp
		Ztp1 = Phase / cmplx(dble(Phase), 0.0_rp, rp)
!______________________________________________________________________________________________	  
!_______________________ 3. Calculate SpZSpZ, SpmSpm, DesDes __________________________________
!______________________________________________________________________________________________
		do Jd = 1, NumCal
			I1 = XLatt(Jd)
			I2 = YLatt(Jd)
			Ztp2 = GUpC(I1, I2) * GUp(I1, I2) + GDwC(I1, I2) * GDw(I1, I2)
			Ztp3 = GUpC(I1, I2) * GDw(I1, I2) + GUp(I1, I2) * GDwC(I1, I2)
			Ztp4 = GUpC(I1, I2) + GDwC(I1, I2)
			Ztp5 = ( TmpSpZ(I1) * TmpSpZ(I2) + Ztp2 )/4.0_rp
			Ztp6 = TmpDen(I1) * TmpDen(I2) + Ztp2
			
			CorrFunc(Jd, 1) = CorrFunc(Jd, 1) +                 Ztp5 * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)    ! SpinZZ
			CorrFunc(Jd, 2) = CorrFunc(Jd, 2) +         Ztp3/2.0_rp  * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)    ! SpinPM
			CorrFunc(Jd, 3) = CorrFunc(Jd, 3) + (Ztp5 + Ztp3/2.0_rp) * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)    ! SpnSpn
			CorrFunc(Jd, 4) = CorrFunc(Jd, 4) +                 Ztp6 * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)    ! DenDen
			CorrFunc(Jd, 5) = CorrFunc(Jd, 5) +                 Ztp4 * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)    ! GreenT		
			
			CorrFTmp(Jd, 1) = CorrFTmp(Jd, 1) + TmpSpZ(I1)/2.0_rp * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)
			CorrFTmp(Jd, 2) = CorrFTmp(Jd, 2) + TmpSpZ(I2)/2.0_rp * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)
			CorrFTmp(Jd, 3) = CorrFTmp(Jd, 3) + TmpDen(I1)        * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)
			CorrFTmp(Jd, 4) = CorrFTmp(Jd, 4) + TmpDen(I2)        * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)
		enddo
!______________________________________________________________________________________________________________	  
!___________________________________________ Deallocate the arrays ____________________________________________
!______________________________________________________________________________________________________________
		deallocate(TmpDen)
		deallocate(TmpSpZ)
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$