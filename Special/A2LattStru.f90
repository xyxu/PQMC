!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: Several subroutines used to perform initialization of the finite lattice structure before the whole PQMC simulations.
! COMMENT: PQMC Initialization process.
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-27
! PURPOSE: Different subroutines are introduced as following:
!             
!   InitLattStru --> Subroutine to perform initializations for finite lattice structure applied in PQMC;
!   GenerateList --> Subroutine to generate the lists used for the finite lattice calculations;
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine InitLattStru()
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  InitLattStru()
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to perform the initialization for the finite lattice we applied in the PQMC calculations.
! KEYWORDS: Initialization of PQMC finite lattice.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-10
! DESCRIPTION:
!
!     Initialization of PQMC finite lattice, including:
!             (0) Some constants for the lattice;
!             (1) Allocate the corresponding arrays;
!             (2) Calculate the arrays for using in PQMC.
!
!     Input:  (none)   Output: (none)
!
! MODULES USED: RealPrecsn, CoreParamt
! SUBROUTINES CALLED: GenerateList
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		implicit none
		
		integer I0
!______________________________________________________________________________________________________________	  
!_______________________________ Main calculations of PQMC finite lattice _____________________________________
!______________________________________________________________________________________________________________
!**************************************************************************************************	  
!_________________ 0. Some constants for the lattice ______________________________________________
!**************************************************************************************************
		NOrbt = 2               ! Number of orbitals in a single unit cell
		NumNC = NumL1 * NumL2   ! Number of unit cells in the finite lattice
		NumNS = NumNC * NOrbt   ! Total number of Lattice sites in single layer
		NumEl = NumNS / 2       ! Number of electrons in each spin sector
		
		A1Vec(1) = sqrt(3.0_rp)                    ! The a1 lattice vector in real space                 --> A1Vec = (sqrt(3), 0)
		A1Vec(2) = 0.0_rp                          ! The a1 lattice vector in real space                 --> A1Vec = (sqrt(3), 0)
		A2Vec(1) = sqrt(3.0_rp) / 2.0_rp           ! The a2 lattice vector in real space                 --> A2Vec = (sqrt(3)/2, 3/2)
		A2Vec(2) = 3.0_rp / 2.0_rp                 ! The a2 lattice vector in real space                 --> A2Vec = (sqrt(3)/2, 3/2)
		
		L1Vec = NumL1 * A1Vec                      ! Total length of a1 direction for the lattice system --> L1Vec = NumL1 * A1Vec
		L2Vec = NumL2 * A2Vec                      ! Total length of a2 direction for the lattice system --> L2Vec = NumL2 * A2Vec
		
		B1Vec(1) =  + 1.0_rp                       ! The b1 basic vector in reciprocal space
		B1Vec(2) =  - 1.0_rp / sqrt(3.0_rp)        ! The b1 basic vector in reciprocal space
		B1Vec    = 2.0_rp*rp_pi/sqrt(3.0) * B1Vec  ! The b1 basic vector in reciprocal space
		B2Vec(1) = 0.0_rp                          ! The b2 basic vector in reciprocal space   
		B2Vec(2) = 1.0_rp                          ! The b2 basic vector in reciprocal space   
		B2Vec    = 4.0_rp*rp_pi/3.0_rp * B2Vec     ! The b2 basic vector in reciprocal space   
		
		X1Vec = B1Vec / NumL1   ! Vector used to generate the discrete k points in BZ
		X2Vec = B2Vec / NumL2   ! Vector used to generate the discrete k points in BZ
!**************************************************************************************************	  
!_________________ 1. Allocate and the arrays for the finite lattice ______________________________
!**************************************************************************************************
!______________________________________________________________________________________	  
!_________ (0). Nearest-Neighbor unit cells of all unit cells _________________________
!______________________________________________________________________________________
		I0 = NumNC + NumL1 + NumL2 + 1
		allocate(   UCList(    NumNC,       2)   )    ! The two integer indexes for a unit cell          
		allocate(InvUCList(    NumL1,   NumL2)   )    ! From two integer indexes to get Unit cell number
		allocate(   KpList(       I0,       2)   )    ! The two integer indexes for a single k point
		allocate(InvKpList(  0:NumL1, 0:NumL2)   )    ! From two integer indexes to get k point number
		allocate( NNUCList(NumNC, -1 : 1, -1 : 1))    ! Record the six NN Unit cells for all unit cells
		UCList    = 0
		InvUCList = 0
		NNUCList  = 0
		KpList    = 0
		InvKpList = 0
!______________________________________________________________________________________	  
!_________ (1). NN, 2NN and 3NN lattice sites information _____________________________
!______________________________________________________________________________________
		allocate(   StList(NumNS,     2))       ! Return the unit cell integer index for site and 1 or 2 for sublattice
		allocate(InvStList(NumNC, NOrbt))       ! From UC index and Orbit to get the lattice site index
		allocate(   NNBond(NumNC,   0:3))       ! The NN lattice sites for A sublattice sites
		allocate(  TNNBond(NumNC,   0:3))       ! The third-NN lattice sites for A sublattice sites
		StList    = 0
		InvStList = 0
		NNBond    = 0
		TNNBond   = 0
!______________________________________________________________________________________	  
!_________ (2). Distance indexes for two unit cells in the finite lattice _____________
!______________________________________________________________________________________
		allocate(IminusJ(NumNC, NumNC))         ! The integer index for (i-j) and i,j are unit cell indexes
		IminusJ = 0
!______________________________________________________________________________________  
!_________ (3). Open or periodic boundary conditions in a1 and a2 directions __________
!______________________________________________________________________________________
		allocate(NNStBnd(NumNC, 1:3)       )    ! Matrix elements 0, 1 indicating whether has NN hopping for (I1, I2) sites
		allocate(NNUCBnd(NumNC, -1:1, -1:1))    ! Matrix elements 0, 1 indicating whether has NN unit cells for some unit cell
		allocate(TNNsBnd(NumNC, 1:3)       )    ! Matrix elements 0, 1 indicating whether has 3NN hopping for (I1, I2) sites
		NNStBnd = 1                              ! Initialization
		NNUCBnd = 1                              ! Initialization
		TNNsBnd = 1                              ! Initialization
!**************************************************************************************************	  
!_________________ 2. Calculate all the above defined lists for the finite lattice ________________
!**************************************************************************************************
		call GenerateList()
!**************************************************************************************************	  
!_________________ 3. Generate the Boundary Condition matrix for the calculations _________________
!**************************************************************************************************
		call Bound_Matrix()
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$




!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine GenerateList() 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  GenerateList()
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to generate the lists matrices defined in the CoreParamt module for honeycomb lattice.
! KEYWORDS: List matrices.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-11-29
! DESCRIPTION:
!
!     In this subroutine, we need to evaluate all the following matrices: 
!            (1) UCList(NumNC, 2)
!            (2) InvUCList(-NumL1 : NumL1, -NumL2 : NumL2)
!            (3) NNUCList(NumNC, -1 : 1, -1 : 1)
!            (4) StList(NumNS, 2)
!            (5) InvStList(NumNC, NOrbt)
!            (6) NNBond(NumNC, 0:3)
!            (7) TNNBond(NumNC, 0:3)
!            (8) KpList(NumNC, NumNS)
!            (9) InvKpList(-NumL1 : NumL1, -NumL2 : NumL2)
!
!     And we adopt the periodic boundary conditions.
!
!     Input:  (none)   Output: (none)
!
! MODULES USED: RealPrecsn, CoreParamt, StdInOutSt
! SUBROUTINES CALLED: ErrOut
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use StdInOutSt
		implicit none
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer I1    ! Loop integer
		integer I2    ! Loop integer
		integer Id    ! Loop integer
		integer Jd    ! Loop integer
		integer Itp1  ! Temporary integer used in calculations
		integer Itp2  ! Temporary integer used in calculations
		integer Itp3  ! Temporary integer used in calculations
		integer Itp4  ! Temporary integer used in calculations
		
		integer I1x       ! Index integer for I1-th unit cell in a1 direction
		integer I1y       ! Index integer for I1-th unit cell in a2 direction
		integer I2x       ! Index integer for I2-th unit cell in a1 direction
		integer I2y       ! Index integer for I2-th unit cell in a2 direction
		
		integer ImJ   ! The integer index for the distance between two unit cells
!______________________________________________________________________________________________________________	  
!_______________________________ Main calculations of the List matrices _______________________________________
!______________________________________________________________________________________________________________
!____________________________________________________________________________________________________	  
!________ 1. Calculate UCList(NumNC, 2) and InvUCList(-NumL1 : NumL1, -NumL2 : NumL2) _______________
!____________________________________________________________________________________________________
		Itp1 = 0
		do I2 = 1, NumL2                          ! Loop over a2 direction unit cells
			do I1 = 1, NumL1                       ! Loop over a1 direction unit cells
				Itp1 = Itp1 + 1                     ! Count the number of unit cells
				UCList(Itp1, 1)   = I1              ! The number of a_1 in position vector of Itp1 unit cell 
				UCList(Itp1, 2)   = I2              ! The number of a_2 in position vector of Itp1 unit cell
				InvUCList(I1, I2) = Itp1            ! determine the number of unit cell according to the (I1, I2) integer
			enddo
		enddo
		if(Itp1 .ne. NumNC) then                  ! If Nc /= NumNC, there must be something wrong
			write(ErrMsg, "('Subroutine GenerateList: Itp1 /= NumNC; Itp1, NumNC = ', 2I10)") Itp1, NumNC
			call ErrOut(ErrMsg)
		end if
!____________________________________________________________________________________________________	  
!________ 2. Calculate NNUCList(NumNC, -1 : 1, -1 : 1) ______________________________________________
!____________________________________________________________________________________________________
		do Jd = 1, NumNC                                         ! Loop over all unit cells
			Itp1 = UCList(Jd, 1)                                  ! Jd = Itp1 * a_1 + Itp2 * a_2
			Itp2 = UCList(Jd, 2)                                  ! Jd = Itp1 * a_1 + Itp2 * a_2
			do I1 = -1, 1                                         ! Loop over all NN unit cells in a1 direction
				do I2 = -1, 1                                      ! Loop over all NN unit cells in a2 direction
					Itp3 = Itp1 + I1                                ! new Id-th unit cell
					Itp4 = Itp2 + I2                                ! new Id-th unit cell
					if(Itp3 <= 0) Itp3 = Itp3 + NumL1               ! Periodic Boundary Condition in a1 direction
					if(Itp4 <= 0) Itp4 = Itp4 + NumL2               ! Periodic Boundary Condition in a2 direction
					if(Itp3 > NumL1) Itp3 = Itp3 - NumL1            ! Periodic Boundary Condition in a1 direction
					if(Itp4 > NumL2) Itp4 = Itp4 - NumL2            ! Periodic Boundary Condition in a2 direction
					NNUCList(Jd, I1, I2) = InvUCList(Itp3, Itp4)    ! Give the integer index to the NN UC
				enddo
			enddo
		enddo
!____________________________________________________________________________________________________	  
!____________________ 3. Calculate StList(NumNS, 2) and InvStList(NumNC, NOrbt) _____________________
!____________________________________________________________________________________________________
		do I1 = 1, NumNS                   ! Lattice site integer index: N_i = NOrbt * (N_u-1) + I_u, I_u \in [1, 2]
			Itp1 = (I1-1)/NOrbt + 1         ! N_u = N_i / NOrbt
			StList(I1, 1) = Itp1            ! The first element records the integer index of unit cell
			Itp2 = I1 - 2 * (Itp1 - 1)      ! I_u = mod(N_i, NOrbt) = N_i - NOrbt * N_u
			StList(I1, 2) = Itp2            ! The second element records the site index inside this unit cell
			InvStList(Itp1, Itp2) = I1      ! From (N_u, I_u) to get the N_i
		enddo
!____________________________________________________________________________________________________	  
!____________________ 4. Calculate NNBond(NumNC, 0:3) _______________________________________________
!____________________________________________________________________________________________________
		do Id = 1, NumNC                         ! Loop over all unit cells
			NNBond(Id, 0) = InvStList(Id, 1)      ! The first element is the A sublattice site integer index in Id-th unit cell
			NNBond(Id, 1) = InvStList(Id, 2)      ! The Second element is the up NN B sublattice site
			Itp1 = NNUCList(Id, +1, -1)           ! The (+1,-1) NN unit cell index integer
			NNBond(Id, 2) = InvStList(Itp1, 2)    ! The Third element is the right-down NN B sublattice site
			Itp2 = NNUCList(Id, +0, -1)           ! The (+0,-1) NN unit cell index integer
			NNBond(Id, 3) = InvStList(Itp2, 2)    ! The fourth element is the left-down NN B sublattice site
		enddo
!____________________________________________________________________________________________________	  
!____________________ 5. Calculate TNNBond(NumNC, 0:3) ______________________________________________
!____________________________________________________________________________________________________
		do Id = 1, NumNC                         ! Loop over all unit cells
			TNNBond(Id, 0) = InvStList(Id, 1)     ! The first element is the A sublattice site integer index in Id-th unit cell
			Itp1 = NNUCList(Id, +1, 0)            ! 
			TNNBond(Id, 1) = InvStList(Itp1, 2)   ! The Second element is the right-up 3NN B sublattice site
			Itp2 = NNUCList(Id, -1, 0)            ! 
			TNNBond(Id, 2) = InvStList(Itp2, 2)   ! The third element is the left-up 3NN B sublattice site
			Itp3 = NNUCList(Id, +1, -1)           ! 
			TNNBond(Id, 3) = NNBond(Itp3, 3)      ! The Fourth element is the down 3NN B sublattice site
		enddo
!____________________________________________________________________________________________________	  
!________________ 6. Calculate KpList(NumNC, NumNS) and InvKpList(-NumL1 : NumL1, -NumL2 : NumL2) ___
!____________________________________________________________________________________________________
		Itp1 = 0
		do I2 = 0, NumL2                          ! Loop over X2 direction vector
			do I1 = 0, NumL1                       ! Loop over X1 direction vector
				Itp1 = Itp1 + 1                     ! Count the number of k point
				KpList(Itp1, 1) = I1                ! The number of X_1 in position vector of Itp1 unit cell 
				KpList(Itp1, 2) = I2                ! The number of X_2 in position vector of Itp1 unit cell
				InvKpList(I1, I2) = Itp1            ! determine the number index of k point according to the (I1, I2) integer
			enddo
		enddo
!____________________________________________________________________________________________________	  
!________________ 7. Calculate IminusJ(NumNC, NumNC) matrix used for calculating correlation ________
!____________________________________________________________________________________________________
		do Id = 1, NumNC
			do Jd = 1, NumNC
				I1x = UCList(Id, 1)
				I1y = UCList(Id, 2)
				I2x = UCList(Jd, 1)
				I2y = UCList(Jd, 2)
				I1  = I1x - I2x
				I2  = I1y - I2y
				
				if( I1*1.0_rp < -NumL1/2.0_rp+1.0E-8_rp ) then
					I1 = I1 + NumL1
				else if( I1*1.0_rp > NumL1/2.0_rp+1.0E-8_rp ) then
					I1 = I1 - NumL1
				end if
				
				if( I2*1.0_rp < -NumL2/2.0_rp+1.0E-8_rp ) then
					I2 = I2 + NumL2
				else if( I2*1.0_rp > NumL2/2.0_rp+1.0E-8_rp ) then
					I2 = I2 - NumL2
				end if
				
				I1 = I1 + NumL1/2 + mod(NumL1, 2)
				I2 = I2 + NumL2/2 + mod(NumL2, 2)
				
				ImJ = InvUCList(I1, I2)
				
				IminusJ(Id, Jd) = ImJ
			enddo
		enddo

		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$




!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine Bound_Matrix() 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  Bound_Matrix()
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to construct the NNStBnd, NNUCBnd and TNNsBnd integer matrices.
! KEYWORDS: Calculate the switch integers.
! AUTHOR:   Yuan-Yao He
! TIME:     2015-01-04
! DESCRIPTION:
!
!     In this subroutine, we calculate the NNStBnd, NNUCBnd and TNNsBnd integer matrices.
!
!     Input:  (none)   Output: (none)
!
! MODULES USED: RealPrecsn, CoreParamt
! SUBROUTINES CALLED:  (none)
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		implicit none
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________ 
		integer Iu                      ! Loop integer for unit cells
!______________________________________________________________________________________________________________	  
!_______________________________ Main calculations of the List matrices _______________________________________
!______________________________________________________________________________________________________________
!*****************************************************************************************************	  
!________________ 0. Construct BndMat, HbUMat, ExJMat, HbVMat matrices for both PBC __________________
!*****************************************************************************************************
		NNStBnd = 1                      ! For PBC condition
		NNUCBnd = 1                      ! For PBC condition 
		TNNsBnd = 1                      ! For PBC condition
!*****************************************************************************************************	  
!________________ 1. Construct the BndMat matrix for Open boundary condition case ____________________
!*****************************************************************************************************
!__________________________________________________________________________________________	  
!________________________ (0) OBC in a1 direction for the system __________________________
!__________________________________________________________________________________________
		if(IfPr1 == 0) then
!____________________________________________________________________________  
!____________________ <0> For NN lattice sites ______________________________
!____________________________________________________________________________
			do Iu = NumL1, NumNC, NumL1
				NNStBnd(Iu, 2) = 0
			enddo
!____________________________________________________________________________  
!____________________ <1> For NNN lattice sites _____________________________
!____________________________________________________________________________
			do Iu = NumL1, NumNC, NumL1
				NNUCBnd(Iu, +1, -1) = 0
				NNUCBnd(Iu, +1,  0) = 0
				NNUCBnd(Iu, +1, +1) = 0
			enddo
			do Iu = 1, (NumL2-1)*NumL1+1, NumL1
				NNUCBnd(Iu, -1, -1) = 0
				NNUCBnd(Iu, -1,  0) = 0
				NNUCBnd(Iu, -1, +1) = 0
			enddo
!____________________________________________________________________________  
!____________________ <2> For 3NN lattice sites _____________________________
!____________________________________________________________________________
			do Iu = NumL1, NumNC, NumL1
				TNNsBnd(Iu, 1) = 0
				TNNsBnd(Iu, 3) = 0
			enddo
			do Iu = 1, (NumL2-1)*NumL1+1, NumL1
				TNNsBnd(Iu, 2) = 0
			enddo
		end if
!__________________________________________________________________________________________	  
!________________________ (1) OBC in a2 direction for the system __________________________
!__________________________________________________________________________________________
		if(IfPr2 == 0) then
!____________________________________________________________________________  
!____________________ <0> For NN lattice sites ______________________________
!____________________________________________________________________________
			do Iu = 1, NumL1, 1
				NNStBnd(Iu, 2) = 0
				NNStBnd(Iu, 3) = 0
			enddo
!____________________________________________________________________________  
!____________________ <1> For NNN lattice sites _____________________________
!____________________________________________________________________________
			do Iu = 1, NumL1, 1
				NNUCBnd(Iu, -1, -1) = 0
				NNUCBnd(Iu,  0, -1) = 0
				NNUCBnd(Iu, +1, -1) = 0
			enddo
			do Iu = (NumL2-1)*NumL1+1, NumNC, 1
				NNUCBnd(Iu, -1, +1) = 0
				NNUCBnd(Iu,  0, +1) = 0
				NNUCBnd(Iu, +1, +1) = 0
			enddo
!____________________________________________________________________________  
!____________________ <2> For 3NN lattice sites _____________________________
!____________________________________________________________________________
			do Iu = 1, NumL1, 1
				TNNsBnd(Iu, 3) = 0
			enddo
			if(NumL2 > 1) then
				do Iu = NumL1+1, NumL1+NumL1, 1
					TNNsBnd(Iu, 3) = 0
				enddo
			end if
		end if
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$