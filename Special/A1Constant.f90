!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: Several subroutines used to perform initialization of some constants before the whole PQMC simulations.
! COMMENT: PQMC Initialization process.
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-27
! PURPOSE: Different subroutines are introduced as following:
!             
!   InitConstant --> Subroutine to perform initializations for some constants in PQMC;
!   GenerateIfIt --> Subroutine to determine whether to calculate some terms in the Model Hamiltonian;
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine InitConstant()
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  InitConstant()
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to perform the initialization for some constants in the PQMC simulations.
! KEYWORDS: Initialization of constants in PQMC.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-10
! DESCRIPTION:
!
!     Initialization of constants in PQMC, including:
!             (0) Some calculation constants;
!             (1) The normalized model U, V, J parameters by interactions;
!             (2) Check if the parameters falls into the regions where minus sign probelm is absent;
!             (3) Generate the if's integers to control whether to calculate some term.
!
!     Input:  (none)   Output: (none)
!
! MODULES USED: RealPrecsn, CoreParamt, StdInOutSt
! SUBROUTINES CALLED: GenerateIfIt
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use StdInOutSt
		implicit none
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		logical MinusSign

		integer time
		real(rp), external :: Ranf
!______________________________________________________________________________________________________________	  
!_______________________________ Main calculations of PQMC constants __________________________________________
!______________________________________________________________________________________________________________
		Dltau = BetaT / LTrot   ! \Delta\tau for PQMC
		DmUST = LTrot / NWrap   ! Dimension of the USTMt matrix
		
		call SYSTEM_CLOCK(time)
		ISeed = nint( abs(time - 1014748364) * (sqrt(5.0_rp)-1.0_rp)/8.0_rp )
		ISeed = nint(Ranf(ISeed) * ISeed)
		
		write(*, "('InitConsts: ', I12, I12)") time, ISeed
      
      write(MonitorOt, "('InitConsts: ', I12, I12)") time, ISeed
		write(MonitorOt, "()") 
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$