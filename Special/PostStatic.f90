!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: Several subroutines used to perform the data process in every BIN simulation after the simulation process in this BIN. This 
!              data process only calculates the average values for static observables, since we have measured them for 2*20*Nswep times.
!              And then we output the average values calculated in every BIN simulation, preparing for the final data processing after
!              all the BIN simulations.
!          These static observables includes:
!              (1) The expectation energies for all the terms in model Hamiltonian, and double occupancy, number of electrons, sign;
!              (2) The accepting ratio for U, V, J interactions during the Ising fields updating process;
!              (3) Some special correlation functions in real space for some chosen lattice sites pairs;
!              (4) Correlation functions for the whole finite lattice, and transform it into reciprocal space;
!              (5) 
! COMMENT: Post process for static observables in every BIN simulation.  
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-30
! PURPOSE: Different subroutines are introduced as following:
!             
!    PostStatic   --> Subroutine to calculate average values for the static observables;
!    FourierTrans --> Subroutine to perform Fourier Transformation for correlation functions.
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	

	
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine PostStatic(NB) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  PostStatic(NB) 
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to perform the post process for the PQMC calculated values of observables, namely calculates the 
!                average values for the observables in a single BIN for NSwep calculations.
! KEYWORDS: Post Process of observables.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-30
! DESCRIPTION:
!
!     Post Process of observables.
!
!     Input: NB --> The iteration number of NmBin for the PQMC simulation;  
!
!     Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt, Observable
! SUBROUTINES CALLED:  (none)
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________
		use RealPrecsn
		use CoreParamt
		use Observable
		use TimeRecord
		use QMCTimeRec
		use StdInOutSt
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		integer NB           ! Number index of the BIN 
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer(8) time1     ! Starting time point in this data post process
		integer(8) time2     ! Ending   time point in this data post process

		integer I1           ! Loop integer index
		integer I2           ! Loop integer index
		integer Nu_1         ! The unit cell index integer for the I1-th lattice site
		integer No_1         ! The orbital index integer for the I1-th lattice site
		integer Nu_2         ! The unit cell index integer for the I2-the lattice site 
		integer No_2         ! The orbital index integer for the I2-th lattice site
		
		integer Iu           ! Loop integer for unit cell
		integer Nk           ! Loop integer for k points
		integer NDim         ! Length of the observables
		
		integer imj
		
		real(rp) Itp1        ! Temporary real number
		real(rp) Itp2        ! Temporary real number
									  
		real(rp) TAcptU     ! The accepting ratio of intra-layer U interaction in this BIN simulation
		
		real(rp) VecK(2)     ! k point coordinate

		complex(rp) Ztp1         ! Temporary complex number
		complex(rp) ZTemp(7)     ! Temporary complex number
		
		complex(rp), allocatable :: Tmp_DenOcc(:, :)  ! Average density occupation on all lattice sites
		complex(rp), allocatable :: Tmp_SpinZc(:, :)  ! Average spin-z component on all lattice sites
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& 	  
!____________________ Counting calling times and time consumed in post process __________________
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&       
		TimsDatPr = TimsDatPr + 1
		call system_clock(time1)
!______________________________________________________________________________________________________________	  
!_____________________________ Main calculations for Average values of observables ____________________________
!______________________________________________________________________________________________________________
		Ztp1 = cmplx(1.0_rp, 0.0_rp, rp) / cmplx(dble(NObs), 0.0_rp, rp)
!**************************************************************************************************	  
!__________ 0. The average values for Obser(1:10) and output ______________________________________
!**************************************************************************************************
!_______________________________________________________________________________________	  
!__________ (0) Calculate average values for static observables ________________________
!_______________________________________________________________________________________
		Obser = Obser * Ztp1
!_______________________________________________________________________________________	  
!__________ (2) Record the calculated data for final data process ______________________
!_______________________________________________________________________________________      
		REHopt1(NB) = real(Obser(1))  / NumNS
		REHoptd(NB) = real(Obser(2))  / NumNS
		REHopSC(NB) = real(Obser(3))  / NumNS
		REHopt3(NB) = real(Obser(4))  / NumNS
		REUIntr(NB) = real(Obser(5))  / NumNS
		RETotal(NB) = real(Obser(6))  / NumNS
		RDouOcc(NB) = real(Obser(7))
!_______________________________________________________________________________________	  
!__________ (3) Output the average values from this BIN calculations ___________________
!_______________________________________________________________________________________
			write(OutPF(3), "(I6)", advance = "no") NB
			do I1 = 1, 6
				write(OutPF(3), "(A, es25.16)", advance = "no") char(9), real(Obser(I1)) / NumNS
			enddo
			write(OutPF(3), "(A)", advance = "no") char(9)
			do I1 = 7, 9
				write(OutPF(3), "(A, es25.16)", advance = "no") char(9), real(Obser(I1))
			enddo
			
			write(OutPF(3), "()")
!**************************************************************************************************	  
!__________ 1. The average values for accepting ratios for U, V, J interaction ____________________
!**************************************************************************************************
!_______________________________________________________________________________________	  
!__________ (0) Initialization of accepting ratio ______________________________________
!_______________________________________________________________________________________
		TAcptU = 0.0_rp
!_______________________________________________________________________________________	  
!__________ (1) Record the calculated data for final data process ______________________
!_______________________________________________________________________________________
		if(real(Obser(30)) > rp_prec) then
			TAcptU = real(Obser(29)/Obser(30))
			AcptU  = AcptU + TAcptU
		end if
!_______________________________________________________________________________________	  
!__________ (2) Output the average values from this BIN calculations ___________________
!_______________________________________________________________________________________
      write(OutPF(4), "(I6, A, A, es25.16, A, es25.16)") NB, char(9), char(9), TAcptU
!**************************************************************************************************	  
!__________ 2. The average values for correlations functions in real space and output _____________
!**************************************************************************************************
!_______________________________________________________________________________________	  
!__________ (0) Calculate average values for corrlation functions ______________________
!_______________________________________________________________________________________
		CorrFunc = CorrFunc * Ztp1
		CorrFTmp = CorrFTmp * Ztp1
		
		write(*, "('PostStatic: ', I4, I12, 2es25.16)") NB, ISeed, real(CorrFunc(2, 3)), imag(CorrFunc(2, 3))

      write(MonitorOt, "()")
		write(MonitorOt, "('PostStatic: ', I4, I12, 2es25.16)") NB, ISeed, real(CorrFunc(2, 3)), imag(CorrFunc(2, 3))
		write(MonitorOt, "()")
		write(MonitorOt, "()")
!_______________________________________________________________________________________	  
!__________ (2) Record the calculated data for final data process ______________________
!_______________________________________________________________________________________
		RSpZSpZ(NB, 1:NumCal) = real(CorrFunc(1:NumCal, 1))
		RSpmSpm(NB, 1:NumCal) = real(CorrFunc(1:NumCal, 2))
		RSpnSpn(NB, 1:NumCal) = real(CorrFunc(1:NumCal, 3))
		do I1 = 1, NumCal
			RDenDen(NB, I1) = real( CorrFunc(I1, 4) - CorrFTmp(I1, 3) * CorrFTmp(I1, 4) )
		enddo
		RGreenF(NB, 1:NumCal) = real(CorrFunc(1:NumCal, 5))
!_______________________________________________________________________________________	  
!__________ (3) Output the average values from this BIN calculations ___________________
!_______________________________________________________________________________________
			do I1 = 5, 9
				write(OutPF(I1), "(I6)", advance = "no") NB
			enddo
			do I2 = 1, NumCal
				write(OutPF(5), "(A, es25.16)", advance = "no") char(9), RSpZSpZ(NB, I2)
				write(OutPF(6), "(A, es25.16)", advance = "no") char(9), RSpmSpm(NB, I2)
				write(OutPF(7), "(A, es25.16)", advance = "no") char(9), RSpnSpn(NB, I2)
				write(OutPF(8), "(A, es25.16)", advance = "no") char(9), RDenDen(NB, I2)
				write(OutPF(9), "(A, es25.16)", advance = "no") char(9), RGreenF(NB, I2)
			enddo
			do I1 = 5, 9
				write(OutPF(I1), "(A)", advance = "no") char(9)
			enddo
			do I2 = 1, NumCal
				write(OutPF(5), "(A, es25.16)", advance = "no") char(9), imag(CorrFunc(I2, 1))
				write(OutPF(6), "(A, es25.16)", advance = "no") char(9), imag(CorrFunc(I2, 2))
				write(OutPF(7), "(A, es25.16)", advance = "no") char(9), imag(CorrFunc(I2, 3))
				write(OutPF(8), "(A, es25.16)", advance = "no") char(9), imag(CorrFunc(I2, 4))
				write(OutPF(9), "(A, es25.16)", advance = "no") char(9), imag(CorrFunc(I2, 5))
			enddo
			do I1 = 5, 9
				write(OutPF(I1), "()") 
			enddo
!**************************************************************************************************	  
!__________ 3. Average values for correlations functions and transform to reciprocal space ________
!**************************************************************************************************
!_______________________________________________________________________________________	  
!__________ (0) Calculate average values for corrlation functions ______________________
!_______________________________________________________________________________________
		DenOcc = DenOcc * Ztp1
		SpinZc = SpinZc * Ztp1
		SpinZZ = SpinZZ * Ztp1
		SpinPM = SpinPM * Ztp1
		GreenT = GreenT * Ztp1
		DenDen = DenDen * Ztp1
!_______________________________________________________________________________________	  
!__________ (2) Recalculate the DenOcc and SpinZc into the same formulation ____________
!_______________________________________________________________________________________
		allocate(Tmp_DenOcc(NumNC, NOrbt))
		allocate(Tmp_SpinZc(NumNC, NOrbt))
		Tmp_DenOcc = DenOcc(:, :, 1)
		Tmp_SpinZc = SpinZc(:, :, 1)
		DenOcc = cmplx(0.0_rp, 0.0_rp, rp)
		SpinZc = cmplx(0.0_rp, 0.0_rp, rp)
		do I1 = 1, NumNS
			Nu_1 = StList(I1, 1)
			No_1 = StList(I1, 2)
			do I2 = 1, NumNS
				Nu_2 = StList(I2, 1)
				No_2 = StList(I2, 2)
				imj = IminusJ(Nu_1, Nu_2)
				
				DenOcc(imj, No_1, No_2) = DenOcc(imj, No_1, No_2) + Tmp_DenOcc(Nu_1, No_1) * Tmp_DenOcc(Nu_2, No_2)
				SpinZc(imj, No_1, No_2) = SpinZc(imj, No_1, No_2) + Tmp_SpinZc(Nu_1, No_1) * Tmp_SpinZc(Nu_2, No_2)
			enddo
		enddo
		deallocate(Tmp_DenOcc)
		deallocate(Tmp_SpinZc)
!_______________________________________________________________________________________	  
!__________ (3) Perform Fourier transformation for SpinZZ, SpinPM, GreenT, DenDen ______
!_______________________________________________________________________________________
!_________________________________________________________________
!______________ Calculate <A*B>-<A>*<B> __________________________
!_________________________________________________________________
		DenDen = DenDen - DenOcc
!_________________________________________________________________
!______________ Fourier Transformation ___________________________
!_________________________________________________________________
		if(IfPr1 == 1 .and. IfPr2 ==1) then 
			call FourierTrans(NB)
		end if
!_______________________________________________________________________________________	  
!__________ (4) Output the (0, 0) point data of SpinZZ, SpinPM, GreenT, DenDen _________
!_______________________________________________________________________________________
			if(IfPr1 == 1 .and. IfPr2 ==1) then 
				Iu = InvKpList(0, 0)
				
				ZTemp = cmplx(0.0_rp, 0.0_rp, rp)
				do I1 = 1, NOrbt
					Itp1 = (NOrbt+1)*I1-NOrbt
					ZTemp(1) = ZTemp(1) + KGreenT(NB, Iu, Itp1)
					ZTemp(2) = ZTemp(2) + KSpinZZ(NB, Iu, Itp1)
					ZTemp(3) = ZTemp(3) + KSpinPM(NB, Iu, Itp1)
					ZTemp(4) = ZTemp(4) + KSpnSpn(NB, Iu, Itp1)
					ZTemp(5) = ZTemp(5) + KDenDen(NB, Iu, Itp1)
				enddo
				ZTemp = ZTemp / cmplx(dble(NOrbt), 0.0_rp, rp)
				
				do Itp2 = 10, 14
					write(OutPF(Itp2), "(I8, A)", advance = "no") NB, char(9)
				enddo
				write(OutPF(10), "(es25.16, A, es25.16)") real(ZTemp(1))      , char(9), imag(ZTemp(1))
				write(OutPF(11), "(es25.16, A, es25.16)") real(ZTemp(2))/NumNC, char(9), imag(ZTemp(2))/NumNC
				write(OutPF(12), "(es25.16, A, es25.16)") real(ZTemp(3))/NumNC, char(9), imag(ZTemp(3))/NumNC
				write(OutPF(13), "(es25.16, A, es25.16)") real(ZTemp(4))/NumNC, char(9), imag(ZTemp(4))/NumNC
				write(OutPF(14), "(es25.16, A, es25.16)") real(ZTemp(5))/NumNC, char(9), imag(ZTemp(5))/NumNC
!_________________________________________________________________________
!_______________ Structure factors for order parameters __________________
!_________________________________________________________________________				
				KOrderP(NB, 1) = real(ZTemp(2))/NumNC; KOrderP(NB, 2) = imag(ZTemp(2))/NumNC
				KOrderP(NB, 3) = real(ZTemp(3))/NumNC; KOrderP(NB, 4) = imag(ZTemp(3))/NumNC
				KOrderP(NB, 5) = real(ZTemp(4))/NumNC; KOrderP(NB, 6) = imag(ZTemp(4))/NumNC
				KOrderP(NB, 7) = real(ZTemp(5))/NumNC; KOrderP(NB, 8) = imag(ZTemp(5))/NumNC
			end if
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& 	  
!____________________ Counting calling times and time consumed in post process __________________
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&       
		call system_clock(time2)
		TimeDatPr = TimeDatPr + TimeIntrvl(time1, time2)

	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$



!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine FourierTrans(NB) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  FourierTrans(NB) 
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to Perform Fourier transformation for SpinZZ, SpinPM, GreenT, DenDen into reciprocal space.
! KEYWORDS: Fourier transformation for equal-time quantities.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-30
! DESCRIPTION:
!
!     Fourier Transformation.
!
!     Input: (none).    Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt, Observable
! SUBROUTINES CALLED: GrFStatics, ExpectEngy, CorreFunct
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use Observable
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		integer NB        ! Number of BIN in the simulation
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer Nk        ! Loop integer for different k points
		integer No1       ! Loop integer for orbital
		integer No2       ! Loop integer for orbital
		integer imj       ! Loop integer for distances between two unit cells
		
		integer I1        ! Index integer for distance vector between two unit cells in a1 direction
		integer I2        ! Index integer for distance vector between two unit cells in a2 direction
		
		integer Itp1         ! Temporary integer 
		
		real(rp) VecK(2)     ! The k point in BZ region during calculations
		real(rp) VecR(2)     ! Distance vector for two unit cells
		
		real(rp) Rtp1        ! Real temporary number
!______________________________________________________________________________________________________________	  
!_____________________________ Main calculations for all the static quantities ________________________________
!______________________________________________________________________________________________________________
!_______________________________________________________________________________________________	  
!________________ 0. Fourier Transformations for these correlation functions ___________________
!_______________________________________________________________________________________________
		do Nk = 1, NumNC+NumL1+NumL2+1
			VecK = KpList(Nk, 1) * X1Vec + KpList(Nk, 2) * X2Vec
			Itp1 = 0
			do No1 = 1, NOrbt
				do No2 = 1, NOrbt
					Itp1 = Itp1 + 1
					do imj = 1, NumNC
						I1 = UCList(imj, 1)
						I2 = UCList(imj, 2)
						I1 = I1 - NumL1/2 - mod(NumL1, 2)
						I2 = I2 - NumL2/2 - mod(NumL2, 2)
						VecR = I1 * A1Vec + I2 * A2Vec
						Rtp1 = dot_product(VecK, VecR)
						KGreenT(NB, Nk, Itp1) = KGreenT(NB, Nk, Itp1) + exp(cmplx(0.0_rp, Rtp1, rp)) * GreenT(imj, No1, No2)
						KSpinZZ(NB, Nk, Itp1) = KSpinZZ(NB, Nk, Itp1) + exp(cmplx(0.0_rp, Rtp1, rp)) * SpinZZ(imj, No1, No2)
						KSpinPM(NB, Nk, Itp1) = KSpinPM(NB, Nk, Itp1) + exp(cmplx(0.0_rp, Rtp1, rp)) * SpinPM(imj, No1, No2)
						KDenDen(NB, Nk, Itp1) = KDenDen(NB, Nk, Itp1) + exp(cmplx(0.0_rp, Rtp1, rp)) * DenDen(imj, No1, No2)
					enddo
					KSpnSpn(NB, Nk, Itp1) = KSpinZZ(NB, Nk, Itp1) + KSpinPM(NB, Nk, Itp1)
				enddo
			enddo
		enddo
!_______________________________________________________________________________________________	  
!________________ 1. Normalize correlation function values by unit cell number _________________
!_______________________________________________________________________________________________
		KGreenT(NB, :, :) = KGreenT(NB, :, :) / cmplx(NumNC*1.0_rp, 0.0_rp, rp)
		KSpinZZ(NB, :, :) = KSpinZZ(NB, :, :) / cmplx(NumNC*1.0_rp, 0.0_rp, rp)
		KSpinPM(NB, :, :) = KSpinPM(NB, :, :) / cmplx(NumNC*1.0_rp, 0.0_rp, rp)
		KSpnSpn(NB, :, :) = KSpnSpn(NB, :, :) / cmplx(NumNC*1.0_rp, 0.0_rp, rp)
		KDenDen(NB, :, :) = KDenDen(NB, :, :) / cmplx(NumNC*1.0_rp, 0.0_rp, rp)
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$