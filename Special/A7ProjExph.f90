!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: Several subroutines used to perform Initialization of the projector matrix before the whole PQMC simulations.
! COMMENT: PQMC Initialization process.
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-27
! PURPOSE: Different subroutines are introduced as following:
!             
!   InitProjExph --> Subroutine to perform Initialization of the projector matrix in PQMC Simulations;
!   MagFdPhase   --> Function used to calculate the hopping phase factor from the twisted boundary or the external magnetic field.
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine InitProjExph()
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  InitProjExph()
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to calculate the input slater determinant and calculate the exponential hopping matrices.
!               namely, the three matrices: ProjM, ExpK0, ExpKI.
! KEYWORDS: Initialization of constants in PQMC.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-10
! DESCRIPTION:
!
!     Initialization of input slater determinant ProjM and exponential hopping matrices ExpK0, ExpKI, including the following processes:
!             (0) Allocate all these three matrices and initialization;
!             (1) Construct the noninteracting Hamiltonian matrix for the system;
!             (2) Diagonalize the Hamiltonian matrix for the noninteracting system;
!             (3) Calculate ProjM(NumNS, NumNS) matrix;
!             (4) Calculate ExpoK(NumNS, NumNS) matrix.
!
!     Input:  (none)   Output: (none)
!
! MODULES USED: RealPrecsn, CoreParamt, StdInOutSt
! SUBROUTINES CALLED: MagFdPhase, HermDiag2
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use StdInOutSt
		implicit none
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer IERR0  ! Integer used in Hermitian matrix diagonalization      

		integer I0    ! Loop integer
		integer I1    ! Loop integer
		integer I2    ! Loop integer
		integer I3    ! Loop integer
		integer Id    ! Loop integer
		
		integer I0_A  ! Integer for 3NN hopping terms, A sublattice
		integer I1_A  ! Integer for 3NN hopping terms, A sublattice
		integer I2_A  ! Integer for 3NN hopping terms, A sublattice
		integer I3_A  ! Integer for 3NN hopping terms, A sublattice
		integer I0_B  ! Integer for 3NN hopping terms, B sublattice
		integer I1_B  ! Integer for 3NN hopping terms, B sublattice
		integer I2_B  ! Integer for 3NN hopping terms, B sublattice
		integer I3_B  ! Integer for 3NN hopping terms, B sublattice
		
		integer Itp1  ! temporary integer 
		integer Itp2  ! temporary integer 
		integer Itp3  ! temporary integer 
		
		real(rp) Rtp0  ! temporary integer 
		real(rp) Rtp1  ! temporary integer 
		real(rp) Rtp2  ! temporary integer 
		real(rp) Rtp3  ! temporary integer 
		
		complex(rp) Ztp1    ! Temporary complex value
		complex(rp) Ztp2    ! Temporary complex value
		
		real(rp), allocatable :: HmltR(:, :)    ! Real part of Hamiltonian matrix
		real(rp), allocatable :: HmltI(:, :)    ! Imaginary part of Hamiltonian matrix
		real(rp), allocatable :: EgVal(:)       ! The eigenvalues
		real(rp), allocatable :: EgVcR(:, :)    ! Real part of eigenvector matrix
		real(rp), allocatable :: EgVcI(:, :)    ! Imaginary part of eigemvector matrix
		
		complex(rp), external :: MagFdPhase          ! External function to calculate the complex phase factor for NN hopping
!______________________________________________________________________________________________________________	  
!__________________________________ Allocate Array and Initializations ________________________________________
!______________________________________________________________________________________________________________	
		allocate(HmltR(NumNS, NumNS))
		allocate(HmltI(NumNS, NumNS))
		allocate(EgVal(NumNS))
		allocate(EgVcR(NumNS, NumNS))
		allocate(EgVcI(NumNS, NumNS))
		HmltR = 0.0_rp
		HmltI = 0.0_rp
		EgVal = 0.0_rp
		EgVcR = 0.0_rp
		EgVcI = 0.0_rp
!______________________________________________________________________________________________________________	  
!_____________________ Main calculations of projector matrix and exponential hopping matrices _________________
!______________________________________________________________________________________________________________
!**************************************************************************************************	  
!___________________ 0. Allocate all these three matrices and initialization ______________________
!**************************************************************************************************
		allocate(ProjM(NumNS, NumNS))     ! The projector matrix, P(NumNS, NumNS) --> But we use part
		allocate(ExpK0(NumNS, NumNS))     ! The exponential of Kinetic matrix, exp(-\Delta\tau * K)
		allocate(ExpKI(NumNS, NumNS))     ! The inverse of exponential of Kinetic matrix, [exp(-\Delta\tau * K)]^{-1}
		ProjM = cmplx(0.0_rp, 0.0_rp, rp)  ! Initialization
		ExpK0 = cmplx(0.0_rp, 0.0_rp, rp)  ! Initialization
		ExpKI = cmplx(0.0_rp, 0.0_rp, rp)  ! Initialization
!**************************************************************************************************	  
!___________________ 1. Construct the noninteracting Hamiltonian matrix ___________________________
!**************************************************************************************************     
!__________________________________________________________________________________________	  
!____________________________ (0) The NN hopping terms ____________________________________
!__________________________________________________________________________________________
			do I1 = 1, NumNC                
				Itp1 = NNBond(I1, 0)
				do I2 = 1, 3
					Itp2 = NNBond(I1, I2)
					Rtp0 = NNStBnd(I1, I2) * 1.0_rp
					if(I2 == 1) then
						Ztp1 = cmplx(Hopt1*Dimer, 0.0_rp, rp) * MagFdPhase(NNBond(I1, 0), I2) * Rtp0
					else
						Ztp1 = cmplx(Hopt1, 0.0_rp, rp) * MagFdPhase(NNBond(I1, 0), I2) * Rtp0
					end if
					HmltR(Itp1, Itp2) = real(Ztp1)
					HmltI(Itp1, Itp2) = imag(Ztp1)
					HmltR(Itp2, Itp1) = real(conjg(Ztp1))
					HmltI(Itp2, Itp1) = imag(conjg(Ztp1))
				enddo
			enddo
!__________________________________________________________________________________________	  
!____________________________ (1) The SOC hopping terms ___________________________________
!__________________________________________________________________________________________
		if( abs(HopSC) > 1.0E-10_rp ) then
				do Id = 1, NumNC
					I0 = NNUCList(Id,  0,  0)
					I1 = NNUCList(Id, +1,  0)
					I2 = NNUCList(Id, -1, +1)
					I3 = NNUCList(Id,  0, -1)
			
					I0_A = InvStList(I0, 1)
					I1_A = InvStList(I1, 1)
					I2_A = InvStList(I2, 1)
					I3_A = InvStList(I3, 1)
					
					I0_B = InvStList(I0, 2)
					I1_B = InvStList(I1, 2)
					I2_B = InvStList(I2, 2)
					I3_B = InvStList(I3, 2)
					
					Rtp1 = NNUCBnd(Id, +1,  0) * 1.0_rp
					Rtp2 = NNUCBnd(Id, -1, +1) * 1.0_rp
					Rtp3 = NNUCBnd(Id,  0, -1) * 1.0_rp
				
					HmltI(I0_A, I1_A) = HmltI(I0_A, I1_A) - HopSC * Rtp1
					HmltI(I1_A, I0_A) = HmltI(I1_A, I0_A) + HopSC * Rtp1
					HmltI(I0_A, I2_A) = HmltI(I0_A, I2_A) - HopSC * Rtp2
					HmltI(I2_A, I0_A) = HmltI(I2_A, I0_A) + HopSC * Rtp2
					HmltI(I0_A, I3_A) = HmltI(I0_A, I3_A) - HopSC * Rtp3
					HmltI(I3_A, I0_A) = HmltI(I3_A, I0_A) + HopSC * Rtp3
				
					HmltI(I0_B, I1_B) = HmltI(I0_B, I1_B) + HopSC * Rtp1
					HmltI(I1_B, I0_B) = HmltI(I1_B, I0_B) - HopSC * Rtp1
					HmltI(I0_B, I2_B) = HmltI(I0_B, I2_B) + HopSC * Rtp2
					HmltI(I2_B, I0_B) = HmltI(I2_B, I0_B) - HopSC * Rtp2
					HmltI(I0_B, I3_B) = HmltI(I0_B, I3_B) + HopSC * Rtp3
					HmltI(I3_B, I0_B) = HmltI(I3_B, I0_B) - HopSC * Rtp3
				enddo
		end if
!__________________________________________________________________________________________	  
!____________________________ (2) The 3NN hopping terms ___________________________________
!__________________________________________________________________________________________
		if( abs(Hopt3) > 1.0E-10_rp ) then
				do I1 = 1, NumNC
					Itp1 = TNNBond(I1, 0)
					do I2 = 1, 3
						Rtp0 = TNNsBnd(I1, I2) * 1.0_rp
						Itp2 = TNNBond(I1, I2)
						HmltR(Itp1, Itp2) = HmltR(Itp1, Itp2) + Hopt3 * Rtp0
						HmltR(Itp2, Itp1) = HmltR(Itp2, Itp1) + Hopt3 * Rtp0
					enddo
				enddo
		end if
!**************************************************************************************************	  
!___________________ 2. Diagonalize the Hamiltonian matrix for noninteracting system ______________
!************************************************************************************************** 		
		call HermDiag2(NumNS, NumNS, HmltR, HmltI, EgVal, EgVcR, EgVcI, IERR0)
		if(IERR0 .ne. 0) then          
			write(ErrMsg, "('Subroutine InitProjExph: In HermDiag2, IERR0 /= 0; IERR0 = ', I10)") IERR0
			call ErrOut(ErrMsg)
		end if
!**************************************************************************************************	  
!___________________ 3. Output the energy eigenvalues from the diagonalization ____________________
!**************************************************************************************************
			write(        *, "('InitProjMt: ')")
			write(MonitorOt, "('InitProjMt: ')")
			do I1 = 1, NumNS
				write( OutPF(2), "(I6, A, es25.16)") I1, char(9), EgVal(I1)
				write(        *, "(I6, A, es25.16)") I1, char(9), EgVal(I1)
				write(MonitorOt, "(I6, A, es25.16)") I1, char(9), EgVal(I1)
			enddo
			write(        *, "()")
			write(MonitorOt, "()")
			write(OutPF(2) , "()")
			write(OutPF(2) , "()")
			write(OutPF(2) , "('     Total Energy = ', es25.16)") sum(EgVal(1:NumEl))
			write(OutPF(2) , "('Energy Difference = ', es25.16)") EgVal(NumEl+1)-EgVal(NumEl)
!**************************************************************************************************	  
!___________________ 4. Calculate ProjM(NumNS, NumNS) matrix ______________________________________
!************************************************************************************************** 
		do I1 = 1, NumNS
			do I2 = 1, NumNS
				ProjM(I1, I2) = cmplx(EgVcR(I1, I2), EgVcI(I1, I2), rp)
			enddo
		enddo
!**************************************************************************************************	  
!___________________ 5. Calculate ExpK0, ExpKI matrix _____________________________________________
!**************************************************************************************************
		do I1 = 1, NumNS
			do I2 = 1, NumNS
				Ztp1 = cmplx(0.0_rp, 0.0_rp, rp)
				Ztp2 = cmplx(0.0_rp, 0.0_rp, rp)
				do I3 = 1, NumNS
					Ztp1 = Ztp1 + ProjM(I1, I3) * exp(-Dltau*EgVal(I3)) * conjg(ProjM(I2, I3))
					Ztp2 = Ztp2 + ProjM(I1, I3) * exp(+Dltau*EgVal(I3)) * conjg(ProjM(I2, I3))
				enddo
				ExpK0(I1, I2) = Ztp1       ! Exp(-\Delta\tau * K)
				ExpKI(I1, I2) = Ztp2       ! [Exp(-\Delta\tau * K)]^{-1} = Exp(+\Delta\tau * K)
			enddo
		enddo
!______________________________________________________________________________________________________________	  
!___________________________________________ Deallocate the arrays ____________________________________________
!______________________________________________________________________________________________________________
		deallocate(HmltR)
		deallocate(HmltI)
		deallocate(EgVal)
		deallocate(EgVcR)
		deallocate(EgVcI)
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$




!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	complex(rp) function MagFdPhase(Id, nf) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  MagFdPhase(Id, nf) 
! TYPE:     function
! PURPOSE:  This Subroutine calculate the hoppin phase factor due to the external magnetic field and the twisted boundary phase. 
! KEYWORDS: Calculate the switch integers.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-11-29
! DESCRIPTION:
!
!     In this subroutine, we calculate the phase factor for the NN hopping term.
!
!     Input:  (none)   Output: (none)
!
! MODULES USED: RealPrecsn, CoreParamt
! SUBROUTINES CALLED:  (none)
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		integer Id    ! Integer index for the lattice site
		integer nf    ! The integer index 1,2,3 of NN lattice site for Id site
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer I_u        ! The Integer index of the Unit cell 
		integer n_i        ! n_i=1, 2 indicating the A or B sublattice
		
		integer Itp1       ! Temporary integer quantity
		
		real(rp) Rtp1      ! Temporary real quantity
		real(rp) Rtp2      ! Temporary real quantity
		
		complex(rp) Ztp1   ! Temporary complex quantity
		
		real(rp) Vec1(2)   ! Temporary vector used in calculations
		real(rp) Vec2(2)   ! Temporary vector used in calculations
!______________________________________________________________________________________________________________	  
!_______________________________ Main calculations of the List matrices _______________________________________
!______________________________________________________________________________________________________________
!_________________________________________________________________________________________________	  
!________________________ 1. Position of this Id lattice site  ___________________________________
!_________________________________________________________________________________________________
		Itp1 = mod(Id-1, NumNS) + 1
		I_u = StList(Id, 1)    ! The Integer index of the Unit cell 
		n_i = StList(Id, 2)    ! n_i=1, 2 indicating the A or B sublattice --> Here we have n_i=1
		Vec1 = UCList(I_u, 1) * A1Vec + UCList(I_u, 2) * A2Vec  ! Position vector of I_u unit cell and its A sublattice site
!_________________________________________________________________________________________________	  
!________________________ 2. Vector difference between Id site and its NN sites __________________
!_________________________________________________________________________________________________
		Rtp1 = (2.0_rp/3.0_rp) * A2Vec(2)
		if(nf == 1) then
			Vec2(1) = 0.0_rp
			Vec2(2) = Rtp1
		else if(nf == 2) then
			Vec2    = A1Vec - A2Vec
			Vec2(2) = Vec2(2) + Rtp1
		else if(nf == 3) then
			Vec2    = - A2Vec
			Vec2(2) = Vec2(2) + Rtp1
		end if
!_________________________________________________________________________________________________	  
!________________________ 3. Calculate the phase factor from the external magnetic field _________
!_________________________________________________________________________________________________
		Rtp1 = (Magnt*1.0_rp) / abs(L1Vec(1)*L2Vec(2) - L1Vec(2)*L2Vec(1))      ! Strength of external magnetic field B
		Rtp2 = 2.0_rp * rp_pi * (Rtp1/2) * (Vec1(1)*Vec2(2) - Vec1(2)*Vec2(1))  ! The phase factor
		Ztp1 = cmplx(0.0_rp, Rtp2, rp)
		MagFdPhase = exp(Ztp1)
!_________________________________________________________________________________________________	  
!________________________ 4. Calculate the phase factor from the twisted Bounary phase ___________
!_________________________________________________________________________________________________
		Rtp1  = 2.0_rp * rp_pi * TwstX * Vec2(1) / NumL1 / sqrt(dot_product(A1Vec, A1Vec))
		Ztp1  = cmplx(0.0_rp, Rtp1, rp)
		MagFdPhase = MagFdPhase * exp(Ztp1)
		
	end function
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$