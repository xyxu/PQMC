!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: Several subroutines used to perform Initialization of all Ising fields before the whole PQMC simulations.
! COMMENT: PQMC Initialization process.
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-27
! PURPOSE: Different subroutines are introduced as following:
!             
!   InitIsingFld --> Subroutine to perform Initialization of all Ising fields used in PQMC Simulations;
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine InitIsingFld()
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  InitIsingFld()
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to perform the initialization for all the Ising fields for U, V, J interactions.
! KEYWORDS: Initialization of constants in PQMC.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-10
! DESCRIPTION:
!
!     Initialization of Ising fields for U, V, J interactions, including:
!             (0) Allocate the Ising fields matrices for these three interactions;
!             (1) Initialization for Ising fields of U interaction;
!
!     Input:  (none)   Output: (none)
!
! MODULES USED: RealPrecsn, CoreParamt
! SUBROUTINES CALLED: RANDOM_NUMBER
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use StdInOutSt
		implicit none
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer I1         ! Loop integer 
		integer I2         ! Loop integer 
		integer I3         ! Loop integer 
		
		real(rp) Rtp1      ! Temporary real quantity
		real(rp) Rtp2      ! Temporary real quantity	
		
		real(rp), external :: Ranf
!______________________________________________________________________________________________________________	  
!_______________________________ Main calculations of Ising fields Initialization _____________________________
!______________________________________________________________________________________________________________
!VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV	  
!________________ 0. Allocate all the Ising fields ________________________________________________
!VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV	
		if( abs(HubbU) > 1.0E-10_rp ) allocate(IsngbU(NumNS, LTrot))     ! Ising fields for the intra-layer U  interaction term
!VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV	  
!________________ 1. Initialization for Ising fields of intra-layer U interaction _________________
!VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
		if( abs(HubbU) > 1.0E-10_rp ) then
!____________________________________________________________________________	  
!_________________ Open the file and read for the main core _________________
!____________________________________________________________________________
				open(ExtPF(50), err = 191, file = "Output/AA_UIntrIsingField.txt", status = "old")
				do I2 = 1, NumNS
					read(ExtPF(50), *) IsngbU(I2, 1:LTrot)
				enddo
				close(ExtPF(50))
				write(        *, "('InitIsingFld: Read Ising fields for U interaction from input file!')")
				write(MonitorOt, "('InitIsingFld: Read Ising fields for U interaction from input file!')")
				go to 193
!____________________________________________________________________________	  
!________________________ No input file, Random generator ___________________
!____________________________________________________________________________				
191         continue
				write(        *, "('InitIsingFld: Ising fields for U interaction are generated Randomly!')")
				write(MonitorOt, "('InitIsingFld: Ising fields for U interaction are generated Randomly!')")
				IsngbU = +1
				do I2 = 1, NumNS
					do I1 = 1, LTrot
						Rtp1 = Ranf(ISeed)
						if(Rtp1 .le. 0.25_rp) then
							IsngbU(I2, I1) = -2
						else if( (Rtp1 .gt. 0.25_rp) .and. (Rtp1 .le. 0.50_rp) ) then
							IsngbU(I2, I1) = -1
						else if( (Rtp1 .gt. 0.50_rp) .and. (Rtp1 .le. 0.75_rp) ) then
							IsngbU(I2, I1) = +1
						else
							IsngbU(I2, I1) = +2
						end if
					enddo
				enddo
!____________________________________________________________________________	  
!_____________________ If read successfully, go to 193 ______________________
!____________________________________________________________________________
193         continue
		end if
!**************************************************************************************************	  
!________________ 2. Output the core number, Iseed integer for the simulation _____________________
!**************************************************************************************************		
		write(*, "('InitIsngFd: ', I12)") ISeed

      write(MonitorOt, "('InitIsngFd: ', I12)") ISeed
      write(MonitorOt, "()")
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$