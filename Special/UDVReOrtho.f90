!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine UDVReOrtho(ND1, ND2, A, NCon) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  UDVReOrtho(ND1, ND2, A, NCon)
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to perform the UDV decomposition for input A matrix as: A=UDV and give U to A: A=U.
!                 The UDV decomposition is actually a QR decomposition as reorthogonal process.
! KEYWORDS: UDV decomposition for A matrix.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-11-29
! DESCRIPTION:
!
!     UDV decomposition for A matrix as A=UDV and finally A=U.
!         This decomposition is done by calling subroutines defined in UDVDecomps module.
!
!     Input:  (none)   Output: (none)
!
! MODULES USED: RealPrecsn
! SUBROUTINES CALLED: UDVdcmps_C
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use TimeRecord
		use QMCTimeRec
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		integer NCon              ! Integer indicating whether to perform the test calculations in UDV decomposition
		integer ND1               ! dimension of A(ND1, ND2)
		integer ND2               ! dimension of A(ND1, ND2)
		complex(rp) A(ND1, ND2)   ! Input complex matrix
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer(8) time1    ! Starting time point in this UDV decomposition process
		integer(8) time2    ! Ending   time point in this UDV decomposition process

		integer I1     ! Loop integer
		integer I2     ! Loop integer
		
		complex(rp), allocatable ::  D(:)      ! The D vector in UDV decomposition
		complex(rp), allocatable ::  U(:, :)   ! The U matrix in UDV decomposition
		complex(rp), allocatable ::  V(:, :)   ! The V matrix in UDV decomposition
		complex(rp), allocatable :: AT(:, :)   ! The transpose matrix of A matrix
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& 	  
!________________ Counting calling times and time consumed in UDV decomposition _________________
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		TimsUDVOt = TimsUDVOt + 1
		call system_clock(time1)
!______________________________________________________________________________________________________________	  
!_______________________________ Main calculations of UDV decomposition _______________________________________
!______________________________________________________________________________________________________________
!______________________________________________________________________________________________	  
!_____________________ 0. For the ND1 > ND2 case ______________________________________________
!______________________________________________________________________________________________
		if(ND1 .gt. ND2) then		
			allocate(U(ND1, ND2))
			allocate(D(ND2))
			allocate(V(ND2, ND2))
			call UDVdcmps_C(ND1, ND2, A, U, D, V, NCon)
			A = U
			deallocate(D)
			deallocate(U)
			deallocate(V)
!______________________________________________________________________________________________	  
!_____________________ 1. For the ND1 < ND2 case ______________________________________________
!______________________________________________________________________________________________
		else
			allocate(U(ND2, ND1))
			allocate(D(ND1))
			allocate(V(ND1, ND1))
			allocate(AT(ND2, ND1))
			do I1 = 1, ND1
				do I2 = 1, ND2
					AT(I2, I1) = A(I1, I2)
				enddo
			enddo
			call UDVdcmps_C(ND2, ND1, AT, U, D, V, NCon)
			do I1 = 1, ND1
				do I2 = 1, ND2
					A(I1, I2) = U(I2, I1)
				enddo
			enddo
			deallocate(D)
			deallocate(U)
			deallocate(V)
			deallocate(AT)
		end if
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& 	  
!________________ Counting calling times and time consumed in UDV decomposition _________________
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		call system_clock(time2)
		TimeUDVOt = TimeUDVOt + TimeIntrvl(time1, time2)
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$