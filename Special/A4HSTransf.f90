!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: Several subroutines used to perform Initialization of HS Tranformation related quantities before the whole PQMC simulations.
! COMMENT: PQMC Initialization process.
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-27
! PURPOSE: Different subroutines are introduced as following:
!             
!   InitHSTransf --> Subroutine to perform initialization of HS Tranformation related quantities used in PQMC;
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	

	
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine InitHSTransf()
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  InitHSTransf()
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to perform the initialization for quantities in Hubbard-Stratonovich Transformation, such as the 
!                 GamL, EtaL and constants for U, V, J interactions.
! KEYWORDS: Initialization of constants in PQMC.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-10
! DESCRIPTION:
!
!     Initialization of Hubbard-Stratonovich Transformation quantities, including:
!             (0) Initializations of all the constants for U, V, J interactions;
!             (1) GamL(-2:2), EtaL(-2:2), FlipL(-2:2, 3) for 4-component Ising fields;
!             (2) HS tranformation parameters for U interaction;
!             (3) HS tranformation parameters for V interaction;
!             (4) HS tranformation parameters for J interaction.
!
!     Input:  (none)   Output: (none)
!
! MODULES USED: RealPrecsn, CoreParamt
! SUBROUTINES CALLED:  (none)
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		implicit none
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer I1          ! Loop integer
		integer I2          ! Loop integer
		integer Id          ! Temporary integer
!______________________________________________________________________________________________________________	  
!__________________________ Main calculations of quantities in HS Transformation ______________________________
!______________________________________________________________________________________________________________
!************************************************************************************************** 
!_________________ 0. Initializations for all these quantities ____________________________________
!**************************************************************************************************
!____________________________________________________________________________________	  
!_________________ GamL(-2:2), EtaL(-2:2), FlipL(-2:2, 3) ___________________________
!____________________________________________________________________________________
		FlipL = 0
		EtaL  = 0.0_rp
		GamL  = 0.0_rp
		DGamL = 0.0_rp
!____________________________________________________________________________________	  
!________________________________ U interaction _____________________________________
!____________________________________________________________________________________		
		LmdaU = cmplx(0.0_rp, 0.0_rp, rp)
		ExpoU = cmplx(0.0_rp, 0.0_rp, rp)
		DltU1 = cmplx(0.0_rp, 0.0_rp, rp)
		DltU2 = cmplx(0.0_rp, 0.0_rp, rp)
!**************************************************************************************************	  
!_________________ 1. Calculate GamL(-2:2), EtaL(-2:2), FlipL(-2:2, 3) matrices ___________________
!**************************************************************************************************    
		EtaL(-2) = - sqrt( 2.0_rp * ( 3.0_rp + sqrt(6.0_rp) ) )
		EtaL(-1) = - sqrt( 2.0_rp * ( 3.0_rp - sqrt(6.0_rp) ) )
		EtaL(+1) = + sqrt( 2.0_rp * ( 3.0_rp - sqrt(6.0_rp) ) )
		EtaL(+2) = + sqrt( 2.0_rp * ( 3.0_rp + sqrt(6.0_rp) ) )
		
		GamL(-2) = 1.0_rp - sqrt(6.0_rp)/3.0_rp
		GamL(-1) = 1.0_rp + sqrt(6.0_rp)/3.0_rp
		GamL(+1) = 1.0_rp + sqrt(6.0_rp)/3.0_rp
		GamL(+2) = 1.0_rp - sqrt(6.0_rp)/3.0_rp
		
		FlipL(-2, 1) = -1
		FlipL(-2, 2) = +1
		FlipL(-2, 3) = +2
		
		FlipL(-1, 1) = -2
		FlipL(-1, 2) = +1
		FlipL(-1, 3) = +2
		
		FlipL(+1, 1) = -2
		FlipL(+1, 2) = -1
		FlipL(+1, 3) = +2
		
		FlipL(+2, 1) = -2
		FlipL(+2, 2) = -1
		FlipL(+2, 3) = +1
		
		do I1 = -2, +2
			if(I1 .ne. 0) then
				do I2 = 1, 3
					Id = FlipL(I1, I2)
					DGamL(I1, I2) = GamL(Id) / GamL(I1)
				enddo
			end if
		enddo
!**************************************************************************************************	  
!_________________ 2. HS tranformation parameters for U interaction _______________________________
!**************************************************************************************************
		LmdaU = cmplx(0.0_rp, 0.0_rp, rp)
		if( abs(HubbU) > 1.0E-10_rp ) then
			if(HubbU .ge. 0.0_rp) then
				LmdaU = cmplx(0.0_rp, sqrt(Dltau*HubbU/2.0_rp), rp)
			else
				LmdaU = cmplx(sqrt(Dltau*abs(HubbU)/2.0_rp), 0.0_rp, rp)
			end if
			
			ExpoU(-2) = exp(LmdaU * cmplx(EtaL(-2), 0.0_rp, rp))
			ExpoU(-1) = exp(LmdaU * cmplx(EtaL(-1), 0.0_rp, rp))
			ExpoU(+1) = exp(LmdaU * cmplx(EtaL(+1), 0.0_rp, rp))
			ExpoU(+2) = exp(LmdaU * cmplx(EtaL(+2), 0.0_rp, rp))
			
			do I1 = -2, 2
				if(I1 .ne. 0) then
					do I2 = 1, 3
						Id = FlipL(I1, I2)
						DltU1(I1, I2) = ExpoU(Id)/ExpoU(I1) - cmplx(1.0_rp, 0.0_rp, rp)
						DltU2(I1, I2) = exp(-LmdaU*cmplx(EtaL(Id), 0.0_rp, rp)/2.0_rp) / exp(-LmdaU*cmplx(EtaL(I1), 0.0_rp, rp)/2.0_rp)
					enddo
				end if
			enddo
		end if
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$