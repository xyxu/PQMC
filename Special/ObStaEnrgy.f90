!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! PROGRAM: A subroutine used to perform the equal-time meansurement of the expectation energies for all the terms in the model Hamiltonian
!            including eight energy terms and Double Occupancy, total number of electrons and Sign for the partition function:
!                 (1) The expectation energy for all the NN hopping terms;        --> EHopt1
!                 (2) The expectation energy for the dimerized NN hopping term;   --> EHoptd
!                 (3) The expectation energy for Intrinsic SOC term;              --> EHopSC
!                 (4) The expectation energy for the 3NN hopping terms;           --> EHopt3
!                 (5) The expectation energy for the inter-layer hopping terms;   --> EHopt3
!                 (6) The expectation energy for intra-layer U  interaction term; --> EUIntr = U*\sum(n_iu * n_id) - U/2*\sum(n_i)
!                 (7) The expectation energy for intra-layer V  interaction term; --> EVIntr = V*\sum(n_i * n_j) - z*V*\sum(n_i)
!                 (8) The expectation energy for inter-layer Vz interaction term; --> EVLayr
!                 (9) The expectation energy for intra-layer J  interaction term; --> EJIntr = H_K + H_C
!                (10) The expectation energy for inter-layer Jz interaction term; --> EJLayr = H_K + H_C
!                (11) The expectation energy for the whole model Hamiltonian;     --> ETotal = EHopt1+EHopSC+EHopt3+EHoptz+EUIntr+EVIntr+EJIntr+EJLayr
!                (12) The expectation energy for the Double Occupancy;            --> DouO1A
!                (13) The expectation energy for total number of electrons;       --> TotOcc
!                (14) The sign integer from the phase setting in simulation;      --> Rtp1
! COMMENT: PQMC initialization process.
! AUTHOR:  Yuan-Yao He
! DATE:    2014-12-27
! PURPOSE: Different subroutines are introduced as following:
!             
!   ExpectEngy --> Subroutine to calculate the expextation energies for all the terms in model Hamiltonian;
!             
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	

	

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!____________________________________ Begin subroutine ____________________________________________________________________________________
!__________________________________________________________________________________________________________________________________________
	subroutine ObStaEnrgy(GUp, GUpC, GDw, GDwC) 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
! PROGRAM:  ObStaEnrgy(GUp, GUpC, GDw, GDwC) 
! TYPE:     subroutine
! PURPOSE:  This Subroutine is used to calculate the expectation energies for all the terms in the model Hamiltonian.
!                 including: NN, 2NN, 3NN hopping terms, U, V, J interaction terms and Total energy, and the anisotropic td term.
!                 Totally eight energy terms.
! KEYWORDS: Calculate expectation energy.
! AUTHOR:   Yuan-Yao He
! TIME:     2014-12-01
! DESCRIPTION:
!
!     Calculate the expectation energies for all the terms in the model Hamiltonian.
!
!     Input: GUp  --> The spin-up Green Function calculated from PQMC;   --> <c_i c_j^+>
!            GUpC --> The spin-up Green Function calculated from PQMC;   --> <c_i^+ c_j>
!            GDw  --> The spin-down Green Function calculated from PQMC; --> <c_i c_j^+>
!            GDwC --> The spin-down Green Function calculated from PQMC; --> <c_i^+ c_j>
! 
!     Outpt: (none).
!
! MODULES USED: RealPrecsn, CoreParamt, Observable
! SUBROUTINES CALLED: MagFdPhase
! END PROGRAM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
!______________________________________________________________________________________________________________	  
!_________________________________________ Modules used in this subroutine ____________________________________
!______________________________________________________________________________________________________________ 
		use RealPrecsn
		use CoreParamt
		use Observable
		implicit none
!______________________________________________________________________________________________________________	  
!_________________________________________ All Input and Output Quantities ____________________________________
!______________________________________________________________________________________________________________
		complex(rp) GUp (NumNS, NumNS) 
		complex(rp) GUpC(NumNS, NumNS)
		complex(rp) GDw (NumNS, NumNS) 
		complex(rp) GDwC(NumNS, NumNS)
!______________________________________________________________________________________________________________	  
!______________________________ Temperory Quantities used in the calculations _________________________________
!______________________________________________________________________________________________________________
		integer Id            ! Loop integer
		integer Jd            ! Loop integer
		
		integer I0            ! Integer index for Lattice site
		integer I1            ! Integer index for Lattice site
		integer I2            ! Integer index for Lattice site 
		integer I3            ! Integer index for Lattice site
		
		integer I0_A          ! Integer index for Lattice site
		integer I1_A          ! Integer index for Lattice site
		integer I2_A          ! Integer index for Lattice site 
		integer I3_A          ! Integer index for Lattice site
		
		integer I0_B          ! Integer index for Lattice site
		integer I1_B          ! Integer index for Lattice site
		integer I2_B          ! Integer index for Lattice site 
		integer I3_B          ! Integer index for Lattice site
		
		real(rp) Rtp1         ! Temporary real number
		real(rp) Rtp2         ! Temporary real number
		real(rp) Rtp3         ! Temporary real number

		complex(rp) Ztp1      ! Complex temporary number
		complex(rp) Ztp2      ! Complex temporary number
		complex(rp) Ztp3      ! Complex temporary number
		
		complex(rp) EHopt1    ! Expectation energy for NN hopping term
		complex(rp) EHoptd    ! Expectation energy for dimerized NN hopping term
		complex(rp) EHopSC    ! Expectation energy for Intrinsic SOC term
		complex(rp) EHopt3    ! Expectation energy for 3NN hopping term
		complex(rp) EUIntr    ! Expectation energy for intra-layer U  interaction term     --> With chemical potential
		complex(rp) EJIntr    ! Expectation energy for intra-layer J  interaction term
		complex(rp) ETotal    ! Expectation energy for total model Hamiltonian             --> With chemical potential
		complex(rp) TotOcc    ! Total occupation as: (GUpC(Id, Id)+GDwC(Id, Id)) summation for all Id sites
		complex(rp) DouOcp    ! Double Occupancy results for PQMC calculations, average for A and B sublattices
		
		complex(rp), external :: MagFdPhase          ! External function to calculate the complex phase factor for NN hopping
!______________________________________________________________________________________________________________	  
!__________________________________ Allocate Array and Initializations ________________________________________
!______________________________________________________________________________________________________________
		ETotal = cmplx(0.0_rp, 0.0_rp, rp)
!______________________________________________________________________________________________________________	  
!________________________________________ Main calculations ___________________________________________________
!______________________________________________________________________________________________________________
!***************************************************************************************************
!___________ (1) Expectation energy for the NN hopping term: EHopt1 and EHoptd _____________________
!***************************************************************************************************
		EHopt1 = cmplx(0.0_rp, 0.0_rp, rp)
		EHoptd = cmplx(0.0_rp, 0.0_rp, rp)
		do Id = 1, NumNC
			I0 = NNBond(Id, 0)
			I1 = NNBond(Id, 1)
			I2 = NNBond(Id, 2)
			I3 = NNBond(Id, 3)
			
			Ztp1 = cmplx(Hopt1*Dimer, 0.0_rp, rp) * MagFdPhase(NNBond(Id, 0), 1) * NNStBnd(Id, 1)
			Ztp2 = cmplx(      Hopt1, 0.0_rp, rp) * MagFdPhase(NNBond(Id, 0), 2) * NNStBnd(Id, 2)
			Ztp3 = cmplx(      Hopt1, 0.0_rp, rp) * MagFdPhase(NNBond(Id, 0), 3) * NNStBnd(Id, 3)
			
			EHopt1 = EHopt1 + GUpC(I0, I1) * Ztp1 + GUpC(I1, I0) * conjg(Ztp1)  ! 1-NN Spin-up two terms
			EHopt1 = EHopt1 + GUpC(I0, I2) * Ztp2 + GUpC(I2, I0) * conjg(Ztp2)  ! 2-NN Spin-up two terms
			EHopt1 = EHopt1 + GUpC(I0, I3) * Ztp3 + GUpC(I3, I0) * conjg(Ztp3)  ! 3-NN Spin-up two terms
			EHopt1 = EHopt1 + GDwC(I0, I1) * Ztp1 + GDwC(I1, I0) * conjg(Ztp1)  ! 1-NN Spin-down two terms
			EHopt1 = EHopt1 + GDwC(I0, I2) * Ztp2 + GDwC(I2, I0) * conjg(Ztp2)  ! 2-NN Spin-down two terms
			EHopt1 = EHopt1 + GDwC(I0, I3) * Ztp3 + GDwC(I3, I0) * conjg(Ztp3)  ! 3-NN Spin-down two terms
			
			EHoptd = EHoptd + GUpC(I0, I1) * Ztp1 + GUpC(I1, I0) * conjg(Ztp1)  ! dimerized 1-NN Spin-up two terms
			EHoptd = EHoptd + GDwC(I0, I1) * Ztp1 + GDwC(I1, I0) * conjg(Ztp1)  ! dimerized 1-NN Spin-down two terms
		enddo
!***************************************************************************************************
!___________ (2) Expectation energy for the SOC hopping term _______________________________________
!***************************************************************************************************
		EHopSC = cmplx(0.0_rp, 0.0_rp, rp)
		if( abs(HopSC) > 1.0E-10_rp ) then
				do Id = 1, NumNC
					I0 = NNUCList(Id,  0,  0)
					I1 = NNUCList(Id, +1,  0)
					I2 = NNUCList(Id, -1, +1)
					I3 = NNUCList(Id,  0, -1)
			
					I0_A = InvStList(I0, 1)
					I1_A = InvStList(I1, 1)
					I2_A = InvStList(I2, 1)
					I3_A = InvStList(I3, 1)
				
					I0_B = InvStList(I0, 2)
					I1_B = InvStList(I1, 2)
					I2_B = InvStList(I2, 2)
					I3_B = InvStList(I3, 2)
					
					Ztp1 = NNUCBnd(Id, +1,  0) * cmplx(0.0_rp, HopSC, rp)
					Ztp2 = NNUCBnd(Id, -1, +1) * cmplx(0.0_rp, HopSC, rp)
					Ztp3 = NNUCBnd(Id,  0, -1) * cmplx(0.0_rp, HopSC, rp)
					
					EHopSC = EHopSC - GUpC(I0_A, I1_A) * Ztp1 - GUpC(I1_A, I0_A) * conjg(Ztp1)
					EHopSC = EHopSC + GDwC(I0_A, I1_A) * Ztp1 + GDwC(I1_A, I0_A) * conjg(Ztp1)
					EHopSC = EHopSC - GUpC(I0_A, I2_A) * Ztp2 - GUpC(I2_A, I0_A) * conjg(Ztp2)
					EHopSC = EHopSC + GDwC(I0_A, I2_A) * Ztp2 + GDwC(I2_A, I0_A) * conjg(Ztp2)
					EHopSC = EHopSC - GUpC(I0_A, I3_A) * Ztp3 - GUpC(I3_A, I0_A) * conjg(Ztp3)
					EHopSC = EHopSC + GDwC(I0_A, I3_A) * Ztp3 + GDwC(I3_A, I0_A) * conjg(Ztp3)
					
					EHopSC = EHopSC + GUpC(I0_B, I1_B) * Ztp1 + GUpC(I1_B, I0_B) * conjg(Ztp1)
					EHopSC = EHopSC - GDwC(I0_B, I1_B) * Ztp1 - GDwC(I1_B, I0_B) * conjg(Ztp1)
					EHopSC = EHopSC + GUpC(I0_B, I2_B) * Ztp2 + GUpC(I2_B, I0_B) * conjg(Ztp2)
					EHopSC = EHopSC - GDwC(I0_B, I2_B) * Ztp2 - GDwC(I2_B, I0_B) * conjg(Ztp2)
					EHopSC = EHopSC + GUpC(I0_B, I3_B) * Ztp3 + GUpC(I3_B, I0_B) * conjg(Ztp3)
					EHopSC = EHopSC - GDwC(I0_B, I3_B) * Ztp3 - GDwC(I3_B, I0_B) * conjg(Ztp3)
				enddo
		end if
!***************************************************************************************************
!___________ (3) Expectation energy for the 3NN hopping term _______________________________________
!***************************************************************************************************
		EHopt3 = cmplx(0.0_rp, 0.0_rp, rp)
		if( abs(Hopt3) > 1.0E-10_rp ) then
				do Id = 1, NumNC
					I0 = TNNBond(Id, 0)
					I1 = TNNBond(Id, 1)
					I2 = TNNBond(Id, 2)
					I3 = TNNBond(Id, 3)
					
					Ztp1 = NNUCBnd(Id, +1,  0) * cmplx(Hopt3, 0.0_rp, rp)
					Ztp2 = NNUCBnd(Id, -1, +1) * cmplx(Hopt3, 0.0_rp, rp)
					Ztp3 = NNUCBnd(Id,  0, -1) * cmplx(Hopt3, 0.0_rp, rp)
				
					EHopt3 = EHopt3 + GUpC(I0, I1) * Ztp1 + GUpC(I1, I0) * conjg(Ztp1)  ! Spin-up terms
					EHopt3 = EHopt3 + GUpC(I0, I2) * Ztp2 + GUpC(I2, I0) * conjg(Ztp2)  ! Spin-up terms
					EHopt3 = EHopt3 + GUpC(I0, I3) * Ztp3 + GUpC(I3, I0) * conjg(Ztp3)  ! Spin-up terms
					EHopt3 = EHopt3 + GDwC(I0, I1) * Ztp1 + GDwC(I1, I0) * conjg(Ztp1)  ! Spin-down terms
					EHopt3 = EHopt3 + GDwC(I0, I2) * Ztp2 + GDwC(I2, I0) * conjg(Ztp2)  ! Spin-down terms
					EHopt3 = EHopt3 + GDwC(I0, I3) * Ztp3 + GDwC(I3, I0) * conjg(Ztp3)  ! Spin-down terms
				enddo
		end if
!***************************************************************************************************
!___________ (5) Expectation energy for the U interaction term _____________________________________
!***************************************************************************************************
		EUIntr = cmplx(0.0_rp, 0.0_rp, rp)                      ! Initialization of total energy of U interaction term --> With    chemical potential
		TotOcc = cmplx(0.0_rp, 0.0_rp, rp)                      ! Initialization of Total Occupation (number of electrons)
		DouOcp = cmplx(0.0_rp, 0.0_rp, rp)                      ! Initialization of Double Occupancy
		do Id = 1, NumNS
			DouOcp = DouOcp + GUpC(Id, Id) * GDwC(Id, Id)                   ! Count total Double Occupancy
			TotOcc = TotOcc + GUpC(Id, Id) + GDwC(Id, Id)                   ! Count number of electrons
		enddo
		EUIntr = (DouOcp - TotOcc/2) * cmplx(HubbU, 0.0_rp, rp) ! Total energy of this U interaction term --> With    chemical potential
		DouOcp = DouOcp / NumNS                                 ! Average Double Occupancy for this measurement
!***************************************************************************************************
!___________ (6) Expectation energy for the whole model Hamiltonian ________________________________
!***************************************************************************************************
		ETotal = EHopt1 + EHopSC + EHopt3 + EJIntr
!***************************************************************************************************
!___________ (7) Check if there is minus sign problem during PQMC simulation _______________________
!***************************************************************************************************
		Rtp1 = 1.0_rp
		Rtp2 = dble(Phase)
		if(Rtp2 .lt. 0.0_rp) Rtp1 = -1.0_rp
		Ztp1 = Phase / cmplx(dble(Phase), 0.0_rp, rp)
!***************************************************************************************************
!___________ (11) Add the above measurement value in this step to the Obser vector _________________
!***************************************************************************************************
		Obser(1) = Obser(1) + EHopt1 * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)   ! All the NN hopping terms
		Obser(2) = Obser(2) + EHoptd * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)   ! The Dimerized NN bond term
		Obser(3) = Obser(3) + EHopSC * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)   ! The Intrinsic SOC terms
		Obser(4) = Obser(4) + EHopt3 * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)   ! The 3NN terms
		Obser(5) = Obser(5) + EUIntr * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)   ! The intra-layer U interaction term
		Obser(6) = Obser(6) + ETotal * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)   ! The Total energy
		Obser(7) = Obser(7) + DouOcp * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)   ! The double occupancy
		Obser(8) = Obser(8) + TotOcc * Ztp1 * cmplx(Rtp1, 0.0_rp, rp)   ! The Total occupation
		Obser(9) = Obser(9) +                 cmplx(Rtp1, 0.0_rp, rp)   ! The Sign for the simulation
		
	end subroutine
!__________________________________________________________________________________________________________________________________________  
!____________________________________ End subroutine ______________________________________________________________________________________
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$